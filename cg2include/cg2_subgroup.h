#pragma once

#include <stdint.h>

uint32_t __subgroupShuffleXor(uint32_t value, uint32_t mask);
