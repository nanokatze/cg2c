import typing

# TODO: use enums for Comp and Dim values

class Comp(typing.NamedTuple):
    prefix: str
    c_type_scalar: str
    c_type_vec4: str

comp_types = [
    Comp(prefix="", c_type_scalar="float", c_type_vec4="float4"),
    Comp(prefix="i", c_type_scalar="int32_t", c_type_vec4="int32x4_t"),
    Comp(prefix="u", c_type_scalar="uint32_t", c_type_vec4="uint32x4_t"),
    Comp(prefix="i64", c_type_scalar="int64_t", c_type_vec4="int64x4_t"),
    Comp(prefix="u64", c_type_scalar="uint64_t", c_type_vec4="uint64x4_t"),
]

class Dim(typing.NamedTuple):
    name: str # TODO: rename
    dim: int
    ms: bool
    arrayed: bool

dims = [
    Dim(name="1D", dim=1, ms=False, arrayed=False),
    Dim(name="1DArray", dim=1, ms=False, arrayed=True),
    Dim(name="2D", dim=2, ms=False, arrayed=False),
    Dim(name="2DArray", dim=2, ms=False, arrayed=True),
    Dim(name="3D", dim=3, ms=False, arrayed=False),
    # Dim(name="Cube", dim=2, ms=False, arrayed=True),
    # Dim(name="CubeArray", dim=3, ms=False, arrayed=True),
    Dim(name="2DMS", dim=2, ms=True, arrayed=False),
    Dim(name="2DMSArray", dim=2, ms=True, arrayed=True),
]

class Op(typing.NamedTuple):
    comp_constraints: list[str]
    dim_constraints: list[str]
    name: str
    fpcoords: bool = False

ops = [
    Op(comp_constraints=[0, 1, 2, 3, 4], dim_constraints=[0, 1, 2, 3, 4, 5, 6], name="{0.c_type_vec4} __image{1.name}Load(uint32_t image, {2});"),
    # Op(comp_constraints=[0], dim_constraints=[2], name="{0.c_type_vec4} __tex{1.name}(uint32_t texture, uint32_t sampler, {2});", fpcoords=True),
]

for op in ops:
    for comp_type in op.comp_constraints:
        for dim in op.dim_constraints:
            coords = "int32_t P"
            if dims[dim].dim > 1:
                coords = "int32x{}_t P".format(dims[dim].dim)
            if dims[dim].arrayed:
                coords += ", int32_t layer"
            if dims[dim].ms:
                coords += ", int32_t sample"
            print(op.name.format(comp_types[comp_type], dims[dim], coords))
