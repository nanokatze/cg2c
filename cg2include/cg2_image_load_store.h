#pragma once

#include <stdint.h>

void __image2DStore(uint32_t image, __int32x2 P, __float4 color);

void __uimage2DStore(uint32_t image, __int32x2 P, __uint32x4 color);
