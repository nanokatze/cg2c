#pragma once

typedef long long ptrdiff_t;

// Result of sizeof, _Alignof, same as uintptr_t.
typedef unsigned long long size_t;
