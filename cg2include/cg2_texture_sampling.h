#pragma once

#include <stdint.h>

__float4 __tex2D(uint32_t image, uint32_t sampler, __float2 P);
// __float4 __txb2D(uint32_t image, uint32_t sampler, __float2 P, float bias);
// __float4 __txf2D(uint32_t image, uint32_t sampler, __int2 P, int lod);

__float4 __texCube(uint32_t image, uint32_t sampler, __float3 P);
