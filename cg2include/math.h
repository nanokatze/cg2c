#pragma once

float fmaf(float x, float y, float z);

float sqrtf(float x);

float fminf(float x, float y);

float fmaxf(float x, float y);

float fabsf(float x);

float floorf(float x);

float ceilf(float x);

float truncf(float x);

float roundevenf(float x);

float ldexpf(float x, int exp);

float nanf(char *tag);

float sinf(float x);

float cosf(float x);

float __sinf(float x);

float __cosf(float x);

float __tanhf(float x);

float __exp2f(float x);

float __log2f(float x);

float __powrf(float x, float y);
