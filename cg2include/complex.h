#pragma once

#define complex _Complex

#define I 1.0if16

float crealf(float complex z);

float cimagf(float complex z);

float cabsf(float complex z);

float complex cexpf(float complex z);
