#include "all.h"

int64_t
cg2c_gcd(int64_t a, int64_t b)
{
	while (b != 0) {
		if (a > b) {
			int64_t tmp = a;
			a = b;
			b = tmp;
		}
		b = b % a;
	}
	return a;
}
