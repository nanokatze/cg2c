#include "all.h"

Value* cg2c_inline(Builder b, Region *r, Value *a);

static Value*
inlineCalls(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	bool *changed = etc;

	switch (op) {
	case OpCall: {
		Value *target = cg2c_slindex(Value*, args, 0);
		Value *arg = cg2c_slindex(Value*, args, 1);

		if (target->op != OpSymbol)
			break;

		Symbol *s = cg2c_valueAuxSymbol(target);
		if (s->binding == SymbolBindingUndef)
			break;

		Value *inlined = cg2c_inline(b, s->def, arg);

		*changed = true;
		return inlined;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

Region* cg2c_inlineCalls(Builder b, Region *r, bool *changed) { return cg2c_rewrite(b, inlineCalls, changed, r); }
