#include "all.h"

Symbol*
cg2c_findSymbol(Slice symbols, String name)
{
	for (size_t i = 0; i < cg2c_sllen(symbols); i++) {
		Symbol *s = cg2c_slindex(Symbol*, symbols, i);
		if (s->binding != SymbolBindingLocal && cg2c_strcmp(s->name, name) == 0)
			return s;
	}
	return NULL;
}

Map*
cg2c_symbolMap(Compile *ctxt, Slice symbols)
{
	GC *gc = ctxt->gc;

	Map *m = cg2c_mkmap(cg2c_str_ptr_map, gc);
	for (size_t i = 0; i < cg2c_sllen(symbols); i++) {
		Symbol *s = cg2c_slindex(Symbol*, symbols, i);
		String name = s->name;

		// Symbol list must consist only of symbols with definitions.
		assert(s->binding != SymbolBindingUndef);

		if (s->binding == SymbolBindingLocal)
			continue;

		Symbol *existing = *(Symbol* const*)cg2c_mapaccess1(cg2c_str_ptr_map, m, &name);
		if (existing != NULL) {
			if (s->binding == SymbolBindingWeak)
				continue;

			if (existing->binding != SymbolBindingWeak) {
				cg2c_errorfAt(ctxt, s->pos, "duplicate symbol %.*s\n"
					"\t:0:0: previous definition",
					cg2c_strlen2(name), cg2c_strdata(name));
				continue;
			}
		}

		*(Symbol**)cg2c_mapassign(cg2c_str_ptr_map, m, &s->name, gc) = s;
	}

	return m;
}

typedef struct LiveSymbols LiveSymbols;
struct LiveSymbols {
	Slice list;
	Map   *set;
};

static void
addLive(LiveSymbols *live, Symbol *s, GC *gc)
{
	if (cg2c_mapcontains(cg2c_ptr_set, live->set, &s))
		return;
	live->list = cg2c_slappend(sizeof(Symbol*), live->list, &s, gc);
	cg2c_mapassign(cg2c_ptr_set, live->set, &s, gc);
}

typedef struct Link Link;
struct Link {
	Symbol *(*lookup)(Compile *ctxt, String name, void *etc);
	void *lookupEtc;

	LiveSymbols live;
};

static Value* link(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc);

Slice
cg2c_link(Compile *ctxt, const LinkOptions *opts, Symbol* (*lookup)(Compile *ctxt, String name, void *etc), void *etc, Symbol *root)
{
	GC *gc = ctxt->gc;

	Link *l = &(Link) {
		.lookup = lookup,
		.lookupEtc = etc,
		.live = {
			.list = NULL_SLICE,
			.set = cg2c_mkmap(cg2c_ptr_set, gc),
		},
	};
	addLive(&l->live, root, gc);
	for (size_t i = 0; i < cg2c_sllen(l->live.list); i++) {
		Symbol *s = cg2c_slindex(Symbol*, l->live.list, i);

		Builder b = {
			.ctxt = ctxt,
			.rewriter = cg2c_newRewriter(NULL_SLICE, gc),
			.cse = s->cse,
		};
		s->def = cg2c_rewrite(b, link, l, s->def);
	}

	return l->live.list;
}

static Symbol*
resolve(Compile *ctxt, Symbol *s, Link *l)
{
	if (s->binding != SymbolBindingUndef && s->binding != SymbolBindingWeak) {
		// Do not redefine strong symbols.
		return s;
	}

	Symbol *r = l->lookup(ctxt, s->name, l->lookupEtc);
	if (s->binding == SymbolBindingWeak) {
		// We're redefining a weak symbol. A symbol with the
		// same name must exist in the symbol table as well.
		assert(r != NULL);
	}
	return r;
}

Value*
link(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	Link *l = etc;
	Compile *ctxt = b.ctxt;
	GC *gc = ctxt->gc;

	switch (op) {
	case OpSymbol: {
		Symbol *s = aux;
		assert(s->type == type);

		Symbol *r = resolve(ctxt, s, l);
		if (r == NULL) {
			if (true)
				cg2c_errorfAt(ctxt, b.pos, "unresolved symbol: %.*s", cg2c_strlen2(s->name), cg2c_strdata(s->name));
			break;
		}
		assert(r->type == s->type);

		addLive(&l->live, r, gc);

		return cg2c_buildSymbol(b, r);
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

static void liveSymbols(Compile *ctxt, Value *v, void *etc);

Slice
cg2c_liveSymbols(Compile *ctxt, Symbol *root)
{
	GC *gc = ctxt->gc;

	LiveSymbols *live = &(LiveSymbols) {
		.list = NULL_SLICE,
		.set  = cg2c_mkmap(cg2c_ptr_set, gc),
	};
	addLive(live, root, gc);
	for (size_t i = 0; i < cg2c_sllen(live->list); i++) {
		Symbol *s = cg2c_slindex(Symbol*, live->list, i);

		cg2c_foreach(ctxt, liveSymbols, live, s->def);
	}

	return live->list;
}

void
liveSymbols(Compile *ctxt, Value *v, void *etc)
{
	LiveSymbols *l = etc;
	GC *gc = ctxt->gc;

	switch (v->op) {
	case OpSymbol: {
		Symbol *s = cg2c_valueAuxSymbol(v);

		if (s->binding != SymbolBindingUndef)
			addLive(l, s, gc);

		break;
	}

	default:
		break;
	}
}
