#include "all.h"

// TODO: this could be made public in the future as well
static Slice
rewriteValueArgs(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Value *v)
{
	GC *gc = b.ctxt->gc;

	Slice args = v->args;
	bool copied = false;
	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);
		Value *aRewritten = cg2c_rewriteValue(b, re, rewriter, etc, a);
		if (a != aRewritten) {
			// TODO: maybe provide a 4-element on-stack storage to
			// clone args into. This will avoid an extra allocation
			// (albeit small -- at most 32 bytes) here.
			if (!copied) {
				args = cg2c_slclone(sizeof(Value*), args, gc);
				copied = true;
			}
			cg2c_slindex(Value*, args, i) = aRewritten;
		}
	}
	return args;
}

typedef struct SimpleRewriter SimpleRewriter;
struct SimpleRewriter {
	RewriterFunc rewriter;
	void *etc;
};

static Value*
simpleRewriter(Builder b, Map *re, Value *v, void *etc_)
{
	SimpleRewriter *etc = etc_;
	GC *gc = b.ctxt->gc;

	switch (v->op) {
	case OpRegion: {
		Region *r = v->aux;
		Region *rr = cg2c_rewriteRegion(b, re, simpleRewriter, etc, r);
		return cg2c_buildRegionValue(b, rr);
	}

	default: {
		Slice args = rewriteValueArgs(b, re, simpleRewriter, etc, v);
		return etc->rewriter(cg2c_builderWithPos(b, v->pos), v, v->op, v->type, v->aux, args, etc->etc);
	}
	}
}

Region*
cg2c_rewrite(Builder b, RewriterFunc rewriter, void *etc, Region *r)
{
	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);
	return cg2c_rewriteRegion(b, re, simpleRewriter, &(SimpleRewriter) {.rewriter = rewriter, .etc = etc}, r);
}

static Value*
plainRewriter(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	return cg2c_buildValue(b, op, type, aux, args);
}

Region*
cg2c_rewritePlain(Builder b, Region *r)
{
	return cg2c_rewrite(b, plainRewriter, NULL, r);
}

Region*
cg2c_rewriteRegion(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Region *r)
{
	GC *gc = b.ctxt->gc;

	Region *rr = *(Region* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &r);
	if (rr == NULL) {
		rr = cg2c_newRegion(b.ctxt, r->type);
		*(Region**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &r, gc) = rr;
		cg2c_regionTerminate(rr, cg2c_rewriteValue(b, re, rewriter, etc, cg2c_regionTerminator(r)));
	}
	return rr;
}

Value*
cg2c_rewriteValue(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Value *v)
{
	GC *gc = b.ctxt->gc;

	Value *w = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &v);
	if (w != NULL)
		return w;
	w = rewriter(b, re, v, etc);
	*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &v, gc) = w;
	return w;
}

Value*
cg2c_rewriteValueDefault(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Value *v)
{
	Slice args = rewriteValueArgs(b, re, rewriter, etc, v);
	return cg2c_buildValue(cg2c_builderWithPos(b, v->pos), v->op, v->type, v->aux, args);
}
