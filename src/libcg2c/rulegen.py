import typing

import mako
from mako.template import Template

import ops

# TODO: handle commutative ops


class Pattern(typing.NamedTuple):
    var: str = None
    op: str = None
    args: list = []
    ddd: bool = False


class RewriteRule(typing.NamedTuple):
    pattern: Pattern
    replace: str
    name: str = None  # TODO: should first and non-optional
    guard: str = None

    def c_name(self, fallback_name=None):
        name = self.name or fallback_name
        return 'rule_' + ''.join(w[:1].upper() + w[1:] for w in name.split())


RULES_C = Template(r"""
<%def name="indented(stops)">
% for line in capture(caller.body).split('\n'):
${'    ' * int(stops) + line}
% endfor
</%def>

% for i, rule in enumerate(rules):

static Value*
${rule.c_name(str(i))}(Builder b, Value *_v)
{\
<%def name="vars(pat, v)">\
% if pat.var:
    Value *${pat.var} = ${v};
% endif
% for i, a in enumerate(pat.args):
    Value *${v + f'_{i}'} = cg2c_valueArg(${v}, ${i});
${vars(a, v + f'_{i}')}\
% endfor
</%def>

${vars(rule.pattern, '_v')}
%if rule.guard:
    if (!(${rule.guard}))
        return _v;
%endif

${rule.replace}
}
% endfor

static RewriteRule rules[] = {
<%def name="pattern_tree(pat, tab_stops)">\
% if pat.op:
&(Pattern) {\
<%self:indented stops="${tab_stops}">\
    .op = Op${pat.op},
% if len(pat.args) > 0:
    .args = (Pattern*[]) {
% for a in pat.args:
        ${pattern_tree(a, tab_stops=2)}\
% endfor
    },
    .narg = ${len(pat.args)},
% endif
% if pat.ddd:
    .ddd = true,
% endif
},\
</%self:indented>\
% else:
NULL,
% endif
</%def>

% for i, rule in enumerate(rules):
    {
        .pattern = ${pattern_tree(rule.pattern, tab_stops=2)}\
        .replace = ${rule.c_name(str(i))},
    },
% endfor
};
Slice ${label} = {rules, ARRAY_SIZE(rules), ARRAY_SIZE(rules), sizeof rules[0]};
""")
