#include "all.h"
#include "cg2.h"

// TODO: think of how to reconcile gen with typechecker

// TODO: provide (Pos) {} as position information for internal constants

static bool
needsMem(Node *n)
{
	return true;
}

typedef struct State State;
struct State {
	Compile *ctxt;

	Region *cc;   // region that is being constructed
	Map    *vars; // CSym* → Value*
	Value  *mem;  // current mem definition
	Value  *result; // ugh

	struct {
		Type *cmplx[KindLast];
		Type *vec[KindLast][5];

		Type *bb; // (mem) noret; TODO: rename into something else
		Type *loop;
	} types;

	Rewriter *rewriter;
	CSE      *cse;
};

static Builder
builder(State *s, Pos pos)
{
	assert(pos.base != NULL);

	return (Builder) {
		.ctxt = s->ctxt,
		.rewriter = s->rewriter,
		.cse = s->cse,
		.pos = cg2c_memdup(&pos, sizeof pos, s->ctxt->gc),
	};
}

static Value*
newValue0A(State *s, Op op, Type *type, void *aux, Pos pos)
{
	return cg2c_buildValue(builder(s, pos), op, type, aux, NULL_SLICE);
}

static Value*
newValue1(State *s, Op op, Type *type, Value *x, Pos pos)
{
	Value *args[] = {x};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue2(State *s, Op op, Type *type, Value *x, Value *y, Pos pos)
{
	Value *args[] = {x, y};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *x, Value *y, Value *z)
{
	Value *args[] = {x, y, z};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

// TODO: maybe Region should just have Pos?
static void
beginBlock(State *s, Region *r, Pos pos)
{
	Builder b = builder(s, pos);

	s->cc = r;
	s->mem = cg2c_buildExtract(b, cg2c_buildParam(b, r), 0);
	assert(cg2c_typeIs(s->mem->type, KindMem));
}

static void
endBlock(State *s, Region *r, Value *t)
{
	assert(s->cc == r);
	cg2c_regionTerminate(s->cc, t);
}

// TODO: better name for this and the following

static Kind ckind2kind[NTYPE] = {
	[TBOOL]    = KindBool,
	[TINT8]    = KindInt8,
	[TUINT8]   = KindInt8,
	[TINT16]   = KindInt16,
	[TUINT16]  = KindInt16,
	[TINT32]   = KindInt32,
	[TUINT32]  = KindInt32,
	[TINT64]   = KindInt64,
	[TUINT64]  = KindInt64,
	[TFLOAT16] = KindInt16,
	[TFLOAT32] = KindInt32,
	[TFLOAT64] = KindInt64,
	[TPTR]     = KindPtr,
};

static Type*
ctype2type(State *s, CType *type)
{
	switch (type->kind) {
	case TBOOL:
	case TINT8: case TUINT8:
	case TINT16: case TUINT16:
	case TINT32: case TUINT32:
	case TINT64: case TUINT64:
	case TFLOAT16:
	case TFLOAT32:
	case TFLOAT64:
		return cg2c_types[ckind2kind[type->kind]];

	case TCOMPLEX: {
		Kind kind = ckind2kind[type->elem->kind];
		if (s->types.cmplx[kind] == NULL)
			s->types.cmplx[kind] = cg2c_tupleN(s->ctxt, cg2c_types[kind], 2);
		return s->types.cmplx[kind];
	}

	case TVEC2: {
		Kind kind = ckind2kind[type->elem->kind];
		if (s->types.vec[kind][2] == NULL)
			s->types.vec[kind][2] = cg2c_tupleN(s->ctxt, cg2c_types[kind], 2);
		return s->types.vec[kind][2];
	}

	case TVEC3: {
		Kind kind = ckind2kind[type->elem->kind];
		if (s->types.vec[kind][3] == NULL)
			s->types.vec[kind][3] = cg2c_tupleN(s->ctxt, cg2c_types[kind], 3);
		return s->types.vec[kind][3];
	}

	case TVEC4: {
		Kind kind = ckind2kind[type->elem->kind];
		if (s->types.vec[kind][4] == NULL)
			s->types.vec[kind][4] = cg2c_tupleN(s->ctxt, cg2c_types[kind], 4);
		return s->types.vec[kind][4];
	}

	case TPTR:
		// TODO: handle pointers to functions in same way we handle
		// TFUNC case
		return cg2c_types[ckind2kind[type->kind]];

	case TFUNC: {
		// TODO: clean this mess up

		Slice param = NULL_SLICE;
		param = cg2c_slappend(sizeof(Type*), param, &cg2c_types[KindMem], s->ctxt->gc);
		for (size_t i = 0; i < cg2c_sllen(type->fields); i++) {
			CField f = cg2c_slindex(CField, type->fields, i);
			Type *t = ctype2type(s, f.type);
			param = cg2c_slappend(sizeof(Type*), param, &t, s->ctxt->gc);
		}
		Type *par = cg2c_tuple(s->ctxt, param);

		Type *tmp = ctype2type(s, type->elem);

		Slice result = NULL_SLICE;
		result = cg2c_slappend(sizeof(Type*), result, &cg2c_types[KindMem], s->ctxt->gc);
		result = cg2c_slappend(sizeof(Type*), result, &tmp, s->ctxt->gc);
		Type *res = cg2c_tuple(s->ctxt, result);

		return cg2c_func(s->ctxt, par, res);
	}

	case TSTRUCT:
	case TUNION: {
		Slice meh = NULL_SLICE;
		// TODO: use min(type->align, 8) or something, or always use the maximum possible element
		assert(type->align >= 4);
		for (int64_t i = 0; i < type->size; i += 4)
			meh = cg2c_slappend(sizeof(Type*), meh, &cg2c_types[KindInt32], s->ctxt->gc);
		return cg2c_tuple(s->ctxt, meh);
	}

	case TVOID:
		return cg2c_tuple(s->ctxt, NULL_SLICE); // TODO: do this only once

	default:
		assert(!"shouldn't get here");
		return NULL;
	}
}

static Value* genLoad(State *s, Type *t, Value *p, int64_t align, Pos pos);
static void   genStore(State *s, Value *p, Value *v, int64_t align, Pos pos);

static int64_t
sizeof_(CType *t)
{
	assert(t->align > 0);
	return t->size;
}

static int64_t
alignof_(CType *t)
{
	assert(t->align > 0);
	return t->align;
}

static Value*
genVar(State *s, int64_t size, int64_t align, Pos pos)
{
	return cg2c_buildVar(builder(s, pos), size, align);
}

Value*
genLoad(State *s, Type *pointee, Value *p, int64_t align, Pos pos)
{
	return cg2c_buildLoad(builder(s, pos), pointee, align, s->mem, p);
}

void
genStore(State *s, Value *p, Value *v, int64_t align, Pos pos)
{
	s->mem = cg2c_buildStore(builder(s, pos), align, s->mem, p, v);
}

static Node* getarg(Node *n, size_t i) { return cg2c_slindex(Node*, n->args, i); }
static Node* getarg0(Node *n) { return getarg(n, 0); }
static Node* getarg1(Node *n) { return getarg(n, 1); }

// TODO: get rid of this
static void
vars(State *b, Node *n)
{
	switch (n->op) {
	case ODCLFUNC:
	case ODCLLIST: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = cg2c_slindex(Node*, n->stmts, i);
			vars(b, s);
		}
		break;
	}

	case ODCL: {
		Value *p = genVar(b, sizeof_(n->sym->type), alignof_(n->sym->type), n->pos);
		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, b->vars, &(void*) {n->sym}, b->ctxt->gc) = p;
		break;
	}

	case OBLOCK: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = cg2c_slindex(Node*, n->stmts, i);
			vars(b, s);
		}
		break;
	}

	case OIF: {
		vars(b, n->x);
		if (n->y != NULL)
			vars(b, n->y);
		break;
	}

	case OFOR: {
		if (n->init != NULL)
			vars(b, n->init);
		vars(b, n->body);
		break;
	}

	case ODOWHILE: {
		vars(b, n->body);
		break;
	}

	default:
		break;
	}
}

static bool
integer(CType *t)
{
	switch (t->kind) {
	case TINT8: case TUINT8:
	case TINT16: case TUINT16:
	case TINT32: case TUINT32:
	case TINT64: case TUINT64:
		return true;

	default:
		return false;
	}
}

static bool
unsigned_(CType *t)
{
	switch (t->kind) {
	case TUINT8: case TUINT16: case TUINT32: case TUINT64:
		return true;

	default:
		return false;
	}
}

static bool
floating(CType *t)
{
	switch (t->kind) {
	case TFLOAT16: case TFLOAT32: case TFLOAT64:
		return true;

	default:
		return false;
	}
}

static Value* genExpr(State *s, Node *n);

static Value*
genIndex(State *s, Node *x, Node *y, Pos pos)
{
	Type *intptrt = cg2c_intptr(s->ctxt);

	Builder b = builder(s, pos);

	Value *base = genExpr(s, x);
	Value *index = genExpr(s, y);
	// TODO: symmetry
	assert(x->type->kind == TPTR);
	Value *index2 = newValue1(s, OpSignExt, intptrt, index, pos);
	Value *size = cg2c_buildConst(b, intptrt, (uint64_t)sizeof_(x->type->elem));
	Value *off = cg2c_buildArith2(b, OpMul, index2, size);
	return cg2c_buildAddPtr(b, base, off);
}

static Value*
genAddr(State *s, Node *n)
{
	switch (n->op) {
	case ONAME: {
		Value *v = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, s->vars, &n->sym);
		assert(v);
		return v;
	}

	case OINDEX:
		return genIndex(s, getarg0(n), getarg1(n), n->pos);

	case ODOT: {
		CType *stype = n->x->type;

		assert(stype->kind == TSTRUCT || stype->kind == TUNION);
		CField f = cg2c_slindex(CField, stype->fields, n->field);

		Value *base = genAddr(s, n->x);
		return cg2c_buildAddPtrConst(builder(s, n->pos), base, f.off);
	}

	case OARROW: {
		CType *stype = n->x->type->elem;

		// TODO: handle vectors

		assert(stype->kind == TSTRUCT || stype->kind == TUNION);
		CField f = cg2c_slindex(CField, stype->fields, n->field);

		// TODO: handle bit-fields

		Value *base = genExpr(s, n->x);
		return cg2c_buildAddPtrConst(builder(s, n->pos), base, f.off);
	}

	case ODEREF:
		return genExpr(s, n->x);

	default:
		assert(0);
		return NULL;
	}
}

static Value*
buildBoolToInt(Builder b, Value *x)
{
	assert(cg2c_typeIs(x->type, KindBool));
	Value *zero = cg2c_buildConst(b, cg2c_types[KindInt32], 0);
	Value *one = cg2c_buildConst(b, cg2c_types[KindInt32], 1);
	return newValue3(b, OpCondSelect, cg2c_types[KindInt32], x, one, zero);
}

static Value*
buildAnyToBool(Builder b, Value *x)
{
	Value *zero = cg2c_buildConst(b, x->type, 0);
	return cg2c_buildCmp(b, OpNotEqual, x, zero);
}

static Value*
genExpr(State *s, Node *n)
{
	Builder b = builder(s, n->pos);

	switch (n->op) {
	// TODO: should be same as OAS
	case ODCL: {
		Value *loc = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, s->vars, &n->sym);
		assert(loc);
		Value *v = genExpr(s, getarg0(n));
		genStore(s, loc, v, alignof_(getarg0(n)->type), n->pos);
		return v;
	}

	case ONAME: {
		if (n->sym->class == CGLOBL || n->sym->class == CEXTERN) {
			// TODO: clone n->sym->name so that we don't keep the
			// whole tree live
			//
			// TODO: hash-cons these objects so that CSE kicks in
			// before linking
			Symbol *tmp = cg2c_malloc(sizeof *tmp, s->ctxt->gc);
			tmp->name = n->sym->name;
			tmp->type = ctype2type(s, n->sym->type);
			return cg2c_buildSymbol(b, tmp);
		}
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->sym->type), loc, alignof_(n->sym->type), n->pos);
	}

	case OAS: {
		if (n->x->op == ODOTSHUF) {
			// TODO: load the entire vector, insert element and
			// store back, rather than just storing at a particular
			// location. Let the compiler optimize redundant stores
			// out, if necessary
			Value *base = genAddr(s, n->x->x);
			Value *p = cg2c_buildAddPtrConst(builder(s, n->pos), base, (int64_t)n->x->shuf[0] * 4);
			Value *v = genExpr(s, n->y);
			genStore(s, p, v, alignof_(n->y->type), n->pos);
			return v;
		}
		Value *p = genAddr(s, n->x);
		Value *v = genExpr(s, n->y);
		genStore(s, p, v, alignof_(n->y->type), n->pos);
		return v;
	}

	case OCALL: {
		assert(getarg0(n)->type->kind == TFUNC);

		assert(cg2c_typeIs(s->mem->type, KindMem));

		Value *x = genExpr(s, getarg0(n));

		Slice args = NULL_SLICE;
		args = cg2c_slappend(sizeof(Value*), args, &s->mem, s->ctxt->gc);
		for (size_t i = 1; i < cg2c_sllen(n->args); i++) {
			Value *a = genExpr(s, cg2c_slindex(Node*, n->args, i));
			// TODO: flatten tuples
			args = cg2c_slappend(sizeof(Value*), args, &a, s->ctxt->gc);
		}
		Value *call = cg2c_buildCall(b, x, cg2c_buildMakeTuple(b, args));

		// This doesn't seem correct, but it is what we assume for now.
		assert(cg2c_tupleNElem(call->type) == 2);

		s->mem = cg2c_buildExtract(b, call, 0);
		assert(cg2c_typeIs(s->mem->type, KindMem));

		return cg2c_buildExtract(b, call, 1);
	}

	case OLT: {
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpLess;
			if (unsigned_(n->x->type))
				op = OpULess;
		} else if (floating(n->x->type))
			op = OpFLess;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, x, y));
	}

	case OLE: {
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpGreaterEqual;
			if (unsigned_(n->x->type))
				op = OpUGreaterEqual;
		} else if (floating(n->x->type))
			op = OpFGreaterEqual;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, y, x));
	}

	case OGT: {
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpLess;
			if (unsigned_(n->x->type))
				op = OpULess;
		} else if (floating(n->x->type))
			op = OpFLess;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, y, x));
	}

	case OGE: {
		// This is very compelling to replace integer and float types
		// with TINT, TUINT, TFLOAT and have them be parametrizable by
		// bitwidth.
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpGreaterEqual;
			if (unsigned_(n->x->type))
				op = OpUGreaterEqual;
		} else if (floating(n->x->type))
			op = OpFGreaterEqual;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, x, y));
	}

	case OEQ: {
		Op op = OpXxx;
		if (integer(n->x->type))
			op = OpEqual;
		else if (floating(n->x->type))
			op = OpFEqual;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, x, y));
	}

	case ONE: {
		Op op = OpXxx;
		if (integer(n->x->type))
			op = OpNotEqual;
		else if (floating(n->x->type))
			op = OpFNotEqual;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		return buildBoolToInt(b, cg2c_buildCmp(b, op, x, y));
	}

	case OANDAND: {
		// a && b => a ? b : false

		Value *x = buildAnyToBool(b, genExpr(s, getarg0(n)));

		Region *r = s->cc;
		Value *m = s->mem;

		Type *fun = cg2c_func(s->ctxt,
			cg2c_tuple1(s->ctxt, cg2c_types[KindMem]),
			cg2c_tuple2(s->ctxt, cg2c_types[KindMem], cg2c_types[KindBool]));

		Region *then = cg2c_newRegion(s->ctxt, fun);
		{
			beginBlock(s, then, n->pos);
			Value *y = buildAnyToBool(b, genExpr(s, getarg1(n)));
			endBlock(s, then, cg2c_buildMakeTuple2(b, s->mem, y));
		}

		Region *else_ = cg2c_newRegion(s->ctxt, fun);
		{
			beginBlock(s, else_, n->pos);
			endBlock(s, else_, cg2c_buildMakeTuple2(b, s->mem, cg2c_buildConst(b, cg2c_types[KindBool], false)));
		}

		s->cc = r;
		s->mem = m;

		Value *andAnd = cg2c_buildIf(b, x, then, else_, cg2c_buildMakeTuple1(b, s->mem));
		s->mem = cg2c_buildExtract(b, andAnd, 0);
		return buildBoolToInt(b, cg2c_buildExtract(b, andAnd, 1));
	}

	case OOROR: {
		// a || b => a ? true : b

		Value *x = buildAnyToBool(b, genExpr(s, getarg0(n)));

		Region *r = s->cc;
		Value *m = s->mem;

		Type *fun = cg2c_func(s->ctxt,
			cg2c_tuple1(s->ctxt, cg2c_types[KindMem]),
			cg2c_tuple2(s->ctxt, cg2c_types[KindMem], cg2c_types[KindBool]));

		Region *then = cg2c_newRegion(s->ctxt, fun);
		{
			beginBlock(s, then, n->pos);
			endBlock(s, then, cg2c_buildMakeTuple2(b, s->mem, cg2c_buildConst(b, cg2c_types[KindBool], true)));
		}

		Region *else_ = cg2c_newRegion(s->ctxt, fun);
		{
			beginBlock(s, else_, n->pos);
			Value *y = buildAnyToBool(b, genExpr(s, getarg1(n)));
			endBlock(s, else_, cg2c_buildMakeTuple2(b, s->mem, y));
		}

		s->cc = r;
		s->mem = m;

		Value *orOr = cg2c_buildIf(b, x, then, else_, cg2c_buildMakeTuple1(b, s->mem));
		s->mem = cg2c_buildExtract(b, orOr, 0);
		return buildBoolToInt(b, cg2c_buildExtract(b, orOr, 1));
	}

	case OCONV: {
		if (getarg0(n)->type->kind == TARRAY) {
			assert(n->type->kind == TPTR);
			return genAddr(s, getarg0(n));
		}

		Value *x = genExpr(s, getarg0(n));
		Type *yT = ctype2type(s, n->type);

		if (integer(getarg0(n)->type) && integer(n->type)) {
			if (!unsigned_(getarg0(n)->type))
				return newValue1(s, OpSignExt, yT, x, n->pos);
			else
				return newValue1(s, OpZeroExt, yT, x, n->pos);
		}

		if (integer(getarg0(n)->type) && floating(n->type)) {
			if (!unsigned_(getarg0(n)->type))
				return newValue1(s, OpIToF, yT, x, n->pos);
			else
				return newValue1(s, OpUToF, yT, x, n->pos);
		}

		if (integer(getarg0(n)->type) && n->type->kind == TPTR)
			return newValue1(s, OpIntToPtr, yT, x, n->pos);

		if (floating(getarg0(n)->type) && integer(n->type)) {
			if (!unsigned_(n->type))
				return newValue1(s, OpFToI, yT, x, n->pos);
			else
				return newValue1(s, OpFToU, yT, x, n->pos);
		}

		if (floating(getarg0(n)->type) && floating(n->type))
			return newValue1(s, OpFToF, yT, x, n->pos);

		if (n->type->kind == TCOMPLEX) {
			assert(n->type->elem->kind == TFLOAT32);
			assert(n->type->elem->kind == getarg0(n)->type->kind);
			Value *imag = cg2c_buildConst(b, ctype2type(s, n->type->elem), 0);
			return cg2c_buildMakeTuple2(b, x, imag);
		}

		if (n->type->kind != TCOMPLEX && getarg0(n)->type->kind == TCOMPLEX) {
			assert(n->type->kind == getarg0(n)->type->elem->kind);
			return cg2c_buildExtract(b, x, 0);
		}

		if (n->type->kind == TPTR && getarg0(n)->type->kind == TPTR)
			return x;

		assert(0);
		return NULL;
	}

	case ODOT: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type) /* TODO: get alignment from field */, n->pos);
	}

	case OARROW: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type) /* TODO: get alignment from field */, n->pos);
	}

	case OADD: {
		if (n->type->kind == TPTR)
			return genIndex(s, getarg0(n), getarg1(n), n->pos);

		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (n->type->kind == TCOMPLEX) {
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, x, 0);
			Value *x_im = cg2c_buildExtract(b, x, 1);
			Value *y_re = cg2c_buildExtract(b, y, 0);
			Value *y_im = cg2c_buildExtract(b, y, 1);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_re, y_re);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_im, y_im);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpAdd : OpFAdd, x, y);
	}

	case OSUB: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		// TODO: handle pointer - integer and pointer - pointer
		if (n->type->kind == TCOMPLEX) {
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, x, 0);
			Value *x_im = cg2c_buildExtract(b, x, 1);
			Value *y_re = cg2c_buildExtract(b, y, 0);
			Value *y_im = cg2c_buildExtract(b, y, 1);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_re, y_re);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_im, y_im);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpSub : OpFSub, x, y);
	}

	case OMUL: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (n->type->kind == TCOMPLEX) {
			// BUG: ±inf and nan are not handled correctly
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, x, 0);
			Value *x_im = cg2c_buildExtract(b, x, 1);
			Value *y_re = cg2c_buildExtract(b, y, 0);
			Value *y_im = cg2c_buildExtract(b, y, 1);
			// complex(x_re, x_im) * complex(y_re, y_im)
			// = (x_re + x_im*i) * (y_re + y_im*i)
			// = x_re*(y_re + y_im*i) + x_im*i*(y_re + y_im*i)
			// = x_re*y_re + x_re*y_im*i + x_im*i*y_re + x_im*i*y_im*i
			// = x_re*y_re + x_re*y_im*i + x_im*i*y_re - x_im*y_im
			// = complex(x_re*y_re - x_im*y_im, x_re*y_im + x_im*i*y_re)
			Value *x_re_y_re = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_re, y_re);
			Value *x_im_y_im = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_im, y_im);
			Value *x_re_y_im = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_re, y_im);
			Value *x_im_y_re = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_im, y_re);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_re_y_re, x_im_y_im);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_re_y_im, x_im_y_re);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpMul : OpFMul, x, y);
	}

	case ODIV: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (integer(n->type)) {
			// An anchor so that the scheduler doesn't schedule too
			// early
			// Value *anchor = cg2c_buildParam(b, s->cc);
			return newValue2(s, !unsigned_(n->type) ? OpDiv : OpUDiv, ctype2type(s, n->type), x, y, /* anchor, */ n->pos);
		}
		if (floating(n->type)) {
			return cg2c_buildArith2(b, OpFDiv, x, y);
		}
		assert(0);
		break;
	}

	case OMOD: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (integer(n->type)) {
			// An anchor so that the scheduler doesn't schedule too
			// early
			// Value *anchor = cg2c_buildParam(b, s->cc);
			return newValue2(s, !unsigned_(n->type) ? OpRem : OpURem, ctype2type(s, n->type), x, y, /* anchor, */ n->pos);
		}
		assert(0);
		break;
	}

	case OOR: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildArith2(b, OpOr, x, y);
	}

	case OAND: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildArith2(b, OpAnd, x, y);
	}

	case OLSH: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildShift(b, OpLsh, x, y);
	}

	case ORSH: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		assert(integer(n->type));
		if (!unsigned_(n->type))
			return cg2c_buildShift(b, OpRsh, x, y);
		else
			return cg2c_buildShift(b, OpURsh, x, y);
	}

	case ONEG: {
		Value *x = genExpr(s, getarg0(n));
		if (integer(n->type)) {
			Value *zero = cg2c_buildConst(b, ctype2type(s, n->type), 0);
			return cg2c_buildArith2(b, OpSub, zero, x);
		}
		if (floating(n->type)) {
			return newValue1(s, OpFNeg, ctype2type(s, n->type), x, n->pos);
		}
		assert(0);
		break;
	}

	case OCOM: {
		Value *x = genExpr(s, getarg0(n));
		return newValue1(s, OpNot, ctype2type(s, n->type), x, n->pos);
	}

	case OADDR:
		return genAddr(s, getarg0(n));

	case OINDEX: {
		// TODO: handle vectors

		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type), n->pos);
	}

	case ODEREF: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type), n->pos);
	}

	case ODOTSHUF: {
		// TODO: load the entire vector and extract
		Value *base = genAddr(s, n->x);
		Value *p = cg2c_buildAddPtrConst(b, base, (int64_t)n->shuf[0] * 4);
		return genLoad(s, ctype2type(s, n->type), p, alignof_(n->type), n->pos);
	}

	case OCONST:
		return cg2c_buildConst(b, ctype2type(s, n->type), n->asd);

	default:
		fprintf(stderr, "%d\n", n->op);
		assert(0);
		return NULL;
	}

	return NULL;
}

static void
genStmt(State *b, Node *n)
{
	switch (n->op) {
	case ODCLFUNC:
	case ODCLLIST:
	case OBLOCK: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++)
			genStmt(b, cg2c_slindex(Node*, n->stmts, i));
		break;
	}

	case ODCL:
		if (cg2c_sllen(n->args) == 0)
			break;
	case OAS:
	case OCONV:
	case OCALL:
	case OADD:
	case OMUL: {
		genExpr(b, n);
		break;
	}

	case ONOP:
		break;

	case OIF: {
		Builder bld = builder(b, n->pos);

		Value *cond = buildAnyToBool(bld, genExpr(b, n->cond));

		Region *r = b->cc;
		Value *m = b->mem;

		Region *then = cg2c_newRegion(b->ctxt, b->types.bb);
		beginBlock(b, then, n->pos);
		genStmt(b, n->x);
		endBlock(b, then, cg2c_buildMakeTuple1(bld, b->mem));

		Region *else_ = cg2c_newRegion(b->ctxt, b->types.bb);
		beginBlock(b, else_, n->pos);
		if (n->y != NULL)
			genStmt(b, n->y);
		endBlock(b, else_, cg2c_buildMakeTuple1(bld, b->mem));

		b->cc = r;
		b->mem = m;

		b->mem = cg2c_buildExtract(bld, cg2c_buildIf(bld, cond, then, else_, cg2c_buildMakeTuple1(bld, b->mem)), 0);
		assert(cg2c_typeIs(b->mem->type, KindMem));

		break;
	}

	case OFOR: {
		Builder bld = builder(b, n->pos);

		if (n->init != NULL)
			genStmt(b, n->init);

		Region *r = b->cc;
		Value *m = b->mem;

		Region *body = cg2c_newRegion(b->ctxt, b->types.loop);
		{
			beginBlock(b, body, n->pos);

			Value *cond = buildAnyToBool(bld, genExpr(b, n->cond));

			Region *r = b->cc;
			Value *m = b->mem;

			Region *then = cg2c_newRegion(b->ctxt, b->types.bb);
			{
				beginBlock(b, then, n->pos);
				genStmt(b, n->body);
				if (n->step != NULL)
					genExpr(b, n->step);
				endBlock(b, then, cg2c_buildMakeTuple1(bld, b->mem));
			}

			Region *else_ = cg2c_newRegion(b->ctxt, b->types.bb);
			{
				beginBlock(b, else_, n->pos);
				endBlock(b, else_, cg2c_buildMakeTuple1(bld, b->mem));
			}

			b->cc = r;
			b->mem = m;

			Value *realBody = cg2c_buildIf(bld, cond, then, else_, cg2c_buildMakeTuple1(bld, b->mem));

			endBlock(b, body, cg2c_buildMakeTuple2(bld, cond, realBody));
		}

		b->cc = r;
		b->mem = m;

		b->mem = cg2c_buildExtract(bld, cg2c_buildLoop(bld, cg2c_buildMakeTuple1(bld, b->mem), body), 0);

		break;
	}

	case ODOWHILE: {
		Builder bld = builder(b, n->pos);

		Region *r = b->cc;
		Value *m = b->mem;

		Region *body = cg2c_newRegion(b->ctxt, b->types.loop);
		{
			beginBlock(b, body, n->pos);
			genStmt(b, n->body);
			Value *cond = buildAnyToBool(bld, genExpr(b, n->cond));
			endBlock(b, body, cg2c_buildMakeTuple2(bld, cond, cg2c_buildMakeTuple1(bld, b->mem)));
		}

		b->cc = r;
		b->mem = m;

		b->mem = cg2c_buildExtract(bld, cg2c_buildLoop(bld, cg2c_buildMakeTuple1(bld, b->mem), body), 0);

		break;
	}

	case ORETURN: {
		if (n->x != NULL) {
			Value *x = genExpr(b, n->x);
			b->result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
		} else {
			Value *x = cg2c_buildMakeTuple(builder(b, n->pos), NULL_SLICE);
			b->result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
		}

		break;
	}

	case OCONST:
		break;

	default:
		assert(0);
		break;
	}
}

Symbol*
cg2c_gen(Compile *ctxt, CSym *cs)
{
	Node *n = cs->def;

	assert(n->op == ODCLFUNC);

	State *b = &(State) {.ctxt = ctxt};

	b->rewriter = cg2c_newRewriter(cg2c_commonRules, ctxt->gc);
	b->cse = cg2c_newCSE(ctxt->gc);

	Type *fun = ctype2type(b, cs->type);

	b->types.bb = cg2c_func(b->ctxt,
		cg2c_tuple1(b->ctxt, cg2c_types[KindMem]),
		cg2c_tuple1(b->ctxt, cg2c_types[KindMem]));
	b->types.loop = cg2c_func(b->ctxt,
		cg2c_tuple1(b->ctxt, cg2c_types[KindMem]),
		cg2c_tuple2(b->ctxt, cg2c_types[KindBool], cg2c_tuple1(b->ctxt, cg2c_types[KindMem])));

	Region *r = cg2c_newRegion(ctxt, fun);

	b->vars = cg2c_mkmap(cg2c_ptr_ptr_map, ctxt->gc);

	beginBlock(b, r, n->pos);

	Value *arg_f = cg2c_buildParam(builder(b, n->pos), r);

	for (size_t i = 0; i < cg2c_sllen(cs->args); i++) {
		CSym *a = cg2c_slindex(CSym*, cs->args, i);

		// TODO: the way we set pos here is incorrect, but good enough

		Value *p = genVar(b, sizeof_(a->type), alignof_(a->type), n->pos);
		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, b->vars, &a, b->ctxt->gc) = p;

		Value *x = cg2c_buildExtract(builder(b, n->pos), arg_f, i+1);
		genStore(b, p, x, alignof_(a->type), n->pos);
	}
	vars(b, n);
	genStmt(b, n);

	// BUG: make ORETURN actually short-circuit the function
	if (b->result == NULL) {
		Value *x = cg2c_buildMakeTuple(builder(b, n->pos), NULL_SLICE);
		b->result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
	}
	endBlock(b, r, b->result);

	Symbol *s = cg2c_malloc(sizeof *s, ctxt->gc);
	s->name = cs->name;
	s->type = r->type;
	// No qualifiers = SymbolBindingGlobal, static and static inline = SymbolBindingLocal,
	// inline = SymbolBindingWeak
	s->binding = SymbolBindingGlobal;
	s->def = r;
	s->cse = b->cse;
	return s;
}

int64_t
cg2c_ceval(Compile *ctxt, Node *n)
{
	// We shouldn't be generating any instructions that need vars, mem or
	// the current continuation, so don't initialize those.
	State *b = &(State) {
		.ctxt = ctxt,

		.rewriter = cg2c_newRewriter(cg2c_commonRules, ctxt->gc),
		.cse = cg2c_newCSE(ctxt->gc),
	};

	Value *v = genExpr(b, n);
	return cg2c_valueAuxInt(v);
}
