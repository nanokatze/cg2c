#include "all.h"
#include "cg2.h"
#include <cg2c/cg2c.h>

#include <string.h>

// TODO: source.c should become preprocessor

// TODO: better errors
// * add a way to compute "starting position" of a node for use in diagnostics
// * make advance useful

// TODO: rewrite parser again

static CType* types[NTYPE] = {
	[TBOOL]    = &(CType) {.kind = TBOOL, .size = 1, .align = 1},
	[TINT8]    = &(CType) {.kind = TINT8, .size = 1, .align = 1},
	[TUINT8]   = &(CType) {.kind = TUINT8, .size = 1, .align = 1},
	[TINT16]   = &(CType) {.kind = TINT16, .size = 2, .align = 2},
	[TUINT16]  = &(CType) {.kind = TUINT16, .size = 2, .align = 2},
	[TINT32]   = &(CType) {.kind = TINT32, .size = 4, .align = 4},
	[TUINT32]  = &(CType) {.kind = TUINT32, .size = 4, .align = 4},
	[TINT64]   = &(CType) {.kind = TINT64, .size = 8, .align = 8},
	[TUINT64]  = &(CType) {.kind = TUINT64, .size = 8, .align = 8},
	[TFLOAT16] = &(CType) {.kind = TFLOAT16, .size = 2, .align = 2},
	[TFLOAT32] = &(CType) {.kind = TFLOAT32, .size = 4, .align = 4},
	[TFLOAT64] = &(CType) {.kind = TFLOAT64, .size = 8, .align = 8},
	[TVOID]    = &(CType) {.kind = TVOID},
};

typedef struct DeclSpec DeclSpec;
struct DeclSpec {
	CType *type;
	int   class;
	bool  inline_;
};

typedef struct Decl Decl;
struct Decl {
	String name;
	CType  *type;
};

typedef struct Targets Targets;
struct Targets {
	Node  *break_;
	Node  *continue_;
	CType *return_; // type for return value
};

static Node* getarg(Node *n, size_t i) { return cg2c_slindex(Node*, n->args, i); }
static Node* getarg0(Node *n) { return getarg(n, 0); }
static Node* getarg1(Node *n) { return getarg(n, 1); }

static Node*    parseXOrADecl(Noder*, int);
static DeclSpec parseDeclSpec(Noder*, bool);
static CType*   parseStruct(Noder*, CKind);
static CType*   parseEnum(Noder*);
static Decl     parseDecl(Noder*, CType*, bool, bool);
static CType*   parsePtrTo(Noder*, CType*);
static CType*   parseDeclSuffix(Noder*, CType*);
static Slice    parseParamList(Noder*);
static CType*   parseType(Noder*);
static bool     haveDecl(Noder*);
static Node*    parseStmt(Noder*, Targets*);
static Slice    parseStmtList(Noder*, Targets*);
static Node*    parseCommaExpr(Noder*);
static Node*    parseExpr(Noder*);
static Node*    parseBinaryExpr(Noder*, int);
static Node*    parseUnaryExpr(Noder*);
static CType*   parseUnaryExprOrType(Noder*);
static Node*    parsePrimaryExpr(Noder*);
static Node*    parsePostfixExpr(Noder*);
static String   parseName(Noder*);
static Node*    parseNumber(Noder*);
static String   parseString(Noder*);
static void     want(Noder*, int);
static bool     got(Noder*, int);
static void     advance(Noder*);

static void syntaxErrorfAt(Noder*, Pos, const char*, ...) __attribute__((no_split_stack));

// TODO: possibly get rid of constructors in the future

static Node* newAddExpr(Compile*, COp, Node*, Node*, Pos);
static Node* newSubExpr(Compile*, COp, Node*, Node*, Pos);
static Node* newShiftExpr(Compile*, COp, Node*, Node*, Pos);
static Node* newCmpExpr(Compile*, COp, Node*, Node*, Pos);
static Node* newLogicalExpr(Compile*, COp, Node*, Node*, Pos);
static Node* newCondExpr(Compile*, Node*, Node*, Node*, Pos);
static Node* newCommaExpr(Compile*, Node*, Node*, Pos);

static Node* newAssignExpr(Compile*, Node*, Node*, Pos);
static Node* newAssignOpExpr(Compile*, COp, Node*, Node*, Pos);

static Node* newDerefExpr(Compile*, Node*, Pos);
static Node* newAddrExpr(Compile*, Node*, Pos);
static Node* newNotExpr(Compile*, Node*, Pos);
static Node* newComExpr(Compile*, Node*, Pos);
static Node* newNegExpr(Compile*, Node*, Pos);

static Node* newIncDecExpr(Compile*, COp, COp, Node*, Pos);

static Node* newBadExpr(Compile*, CType*, Pos);

static Node* newNopStmt(Compile*, Pos);
static Node* newBlockStmt(Compile*, Slice, Pos);
static Node* newIfStmt(Compile*, Node*, Node*, Node*, Pos);
static Node* newForStmt(Compile*, Node*, Node*, Node*, Node*, Pos);
static Node* newDoWhileStmt(Compile*, Node*, Node*, Pos);
static Node* newReturnStmt(Compile*, Node*, Pos);

static bool checkConstExpr(Compile*, Node*);
static void checkDiscarded(Compile*, Node*);

enum {
	BCHAR     = 1 << 0,
	BSHORT    = 1 << 2,
	BINT      = 1 << 4,
	BLONG     = 1 << 6,
	BSIGNED   = 1 << 10,
	BUNSIGNED = 1 << 12,
	BFLOAT    = 1 << 14,
	BDOUBLE   = 1 << 16,
	BBOOL     = 1 << 20,
	BVOID     = 1 << 24,
	BETC      = 1 << 30,
};

static CKind combine(Compile*, int);

// TODO: dump types too

static const char *onames[OEND] = {
	[OBAD] = "BAD",
	[ONAME] = "NAME",
	[OCONST] = "CONST",
	[ODOT] = "DOT",
	[OARROW] = "ARROW",
	[ODOTSHUF] = "DOTSHUF",
	[OARROWSHUF] = "ARROWSHUF",
	[OAS] = "AS",
	[OASOP] = "ASOP",
	[OPOSTINCDEC] = "POSTINCDEC",
	[OPREINCDEC] = "PREINCDEC",
	[OCALL] = "CALL",
	[OCONV] = "CONV",
	[OADD] = "ADD",
	[OSUB] = "SUB",
	[OMUL] = "MUL",
	[ODIV] = "DIV",
	[OMOD] = "MOD",
	[OOR] = "OR",
	[OXOR] = "XOR",
	[OAND] = "AND",
	[OLSH] = "LSH",
	[ORSH] = "RSH",
	[OEQ] = "EQ",
	[ONE] = "NE",
	[OLT] = "LT",
	[OLE] = "LE",
	[OGT] = "GT",
	[OGE] = "GE",
	[OOROR] = "OROR",
	[OANDAND] = "ANDAND",
	[ONOT] = "NOT",
	[OCOM] = "COM",
	[OPLUS] = "PLUS",
	[ONEG] = "NEG",
	[OCOND] = "COND",
	[OINDEX] = "INDEX",
	[ODEREF] = "DEREF",
	[OADDR] = "ADDR",
	[OCOMMA] = "COMMA",
	[ODCLFUNC] = "DCLFUNC",
	[ODCLLIST] = "DCLLIST",
	[ODCL] = "DCL",
	[ONOP] = "NOP",
	[OBLOCK] = "BLOCK",
	[OIF] = "IF",
	[OSWITCH] = "SWITCH",
	[OFOR] = "FOR",
	[OBREAK] = "BREAK",
	[OCONTINUE] = "CONTINUE",
	[OGOTO] = "GOTO",
	[ORETURN] = "RETURN",
};

static void
dumpAST(Node *n, int L)
{
	for (int i = 0; i < L; i++)
		fprintf(stderr, "  ");
	fprintf(stderr, "%s", onames[n->op]);

	// TODO: make a table of these

	switch (n->op) {
	case ONAME:
		fprintf(stderr, " %.*s", cg2c_strlen2(n->sym->name), cg2c_strdata(n->sym->name));
		break;

	case OCONST:
		fprintf(stderr, " %lld", (long long)n->asd);
		break;

	case ODOT: {
		CField f = cg2c_slindex(CField, n->x->type->fields, n->field);
		fprintf(stderr, " %zu (%.*s)", n->field, cg2c_strlen2(f.name), cg2c_strdata(f.name));
		break;
	}

	case OARROW: {
		CField f = cg2c_slindex(CField, n->x->type->elem->fields, n->field);
		fprintf(stderr, " %zu (%.*s)", n->field, cg2c_strlen2(f.name), cg2c_strdata(f.name));
		break;
	}

	default:
		break;
	}

	if (n->implicit)
		fprintf(stderr, " (implicit)");

	fprintf(stderr, " // :%d:%d\n", n->pos.line, n->pos.col);

	if (n->x != NULL)
		dumpAST(n->x, L+1);
	if (n->y != NULL)
		dumpAST(n->y, L+1);

	for (size_t i = 0; i < cg2c_sllen(n->args); i++) {
		Node *s = cg2c_slindex(Node*, n->args, i);
		if (s == NULL)
			continue;
		dumpAST(s, L+1);
	}

	switch (n->op) {
	case ODCLFUNC:
	case ODCLLIST:
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = cg2c_slindex(Node*, n->stmts, i);
			if (s == NULL)
				continue;
			dumpAST(s, L+1);
		}
		break;

	case ODCL:
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = cg2c_slindex(Node*, n->stmts, i);
			if (s == NULL)
				continue;
			dumpAST(s, L+1);
		}
		break;

	case OBLOCK:
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = cg2c_slindex(Node*, n->stmts, i);
			if (s == NULL)
				continue;
			dumpAST(s, L+1);
		}
		break;

	default:
		break;
	}
}

// TODO: move some functions out of the way

static void
cg2c_openScope(Noder *p)
{
	p->scopes = cg2c_slappend(sizeof(CScope), p->scopes, &(CScope) {
		.names = cg2c_mkmap(cg2c_str_ptr_map, p->ctxt->gc),
		.tags  = cg2c_mkmap(cg2c_str_ptr_map, p->ctxt->gc),
	}, p->ctxt->gc);
}

static void
cg2c_closeScope(Noder *p)
{
	CScope *scope = &cg2c_slindex(CScope, p->scopes, cg2c_sllen(p->scopes)-1);
	scope->names = NULL;
	scope->tags = NULL;

	p->scopes = cg2c_slice(sizeof(CScope), p->scopes, 0, cg2c_sllen(p->scopes)-1);
}

static CSym*
cg2c_lookup(Noder *p, String name)
{
	for (size_t i = cg2c_sllen(p->scopes); i-- > 0; ) {
		CScope *scope = &cg2c_slindex(CScope, p->scopes, i);
		CSym *s = *(void* const*)cg2c_mapaccess1(cg2c_str_ptr_map, scope->names, &name);
		if (s != NULL)
			return s;
	}
	return NULL;
}

static char* classNames[] = {
	[CTYPEDEF] = "typedef",
	[CGLOBL]   = "globl",
	[CAUTO]    = "auto",
	[CPARAM]   = "param",
};

static CSym*
declare(Noder *p, String name, CSym *s, int ctxt) // declname or something
{
	assert(ctxt == CGLOBL || ctxt == CAUTO || ctxt == CPARAM);

	// TODO: handle tentative definitions

	CScope *scope = &cg2c_slindex(CScope, p->scopes, cg2c_sllen(p->scopes)-1);
	CSym *s1 = *(void* const*)cg2c_mapaccess1(cg2c_str_ptr_map, scope->names, &name);
	if (s1 != NULL) {
		// TODO: introduce a position to string function for use here

		if (s1->class == CEXTERN) {
			s1->class = CGLOBL;
			return s1;
		}

		cg2c_errorfAt(p->ctxt, &s->def->pos, "%.*s redeclared\n"
			"\t:%d:%d: previous declaration",
			cg2c_strlen2(name), cg2c_strdata(name),
			s1->def->pos.line, s1->def->pos.col);
	}
	*(void**)cg2c_mapassign(cg2c_str_ptr_map, scope->names, &name, p->ctxt->gc) = s;
	return s;
}

static int64_t
nextPowerOfTwo(int64_t x)
{
	int64_t y = 1;
	while (y < x)
		y <<= 1;
	return y;
}

Slice
cg2c_parseC(Compile *ctxt, String filename, String src)
{
	PosBase *posBase = cg2c_malloc(sizeof *posBase, ctxt->gc);

	posBase->filename = filename;

	Map *keywords = cg2c_mkmap(cg2c_str_32_map, ctxt->gc);
	for (Kwtab *kw = cg2c_kwtab; kw != NULL && kw < cg2c_kwtab+cg2c_nkwtab; kw++) {
		String name = cg2c_stringnocopy(kw->name);
		*(int*)cg2c_mapassign(cg2c_str_32_map, keywords, &name, ctxt->gc) = kw->token;
	}

	Noder *p = &(Noder) {
		.ctxt     = ctxt,
		.posBase  = posBase,
		.keywords = keywords,
		.in       = src,
		.line     = 1,
		.col      = 0,
	};

	cg2c_openScope(p);

	// Prime the parser

	cg2c_getc(p);
	cg2c_scan(p);

	// TODO: just implement __attribute__ ext_vector_type syntax

	struct {
		const char *name;
		CKind comp;
		int dim;
	} vecs[] = {
		{"__int32x2", TINT32, 2},
		{"__int32x4", TINT32, 4},
		{"__uint32x4", TUINT32, 4},
		{"__float2", TFLOAT32, 2},
		{"__float3", TFLOAT32, 3},
		{"__float4", TFLOAT32, 4},
	};

	for (size_t i = 0; i < ARRAY_SIZE(vecs); i++) {
		String name = cg2c_stringnocopy(vecs[i].name);

		CType *fp = cg2c_malloc(sizeof *fp, ctxt->gc);
		fp->kind = vecs[i].comp;
		fp->size = 4;
		fp->align = 4;

		CType *type = cg2c_malloc(sizeof *type, ctxt->gc);
		type->kind =
			vecs[i].dim == 4 ? TVEC4 :
			vecs[i].dim == 3 ? TVEC3 :
			vecs[i].dim == 2 ? TVEC2 :
			0;
		type->size = fp->size * nextPowerOfTwo(vecs[i].dim);
		type->align = fp->align;
		type->elem = fp;

		CSym *typeFloat4 = cg2c_malloc(sizeof *typeFloat4, ctxt->gc);
		typeFloat4->class = CTYPEDEF;
		typeFloat4->name = name;
		typeFloat4->type = type;
		declare(p, name, typeFloat4, CGLOBL);
	}

	// Collect CSyms

	while (p->tok != LEOF)
		parseXOrADecl(p, CGLOBL);

	// Don't closeScope because we don't want use checks.
	assert(cg2c_sllen(p->scopes) == 1);

	if (ctxt->errored)
		return NULL_SLICE;

	// Turn CSyms into symbols and generate IR

	Slice o = NULL_SLICE;
	for (size_t i = 0; i < cg2c_sllen(p->syms); i++) {
		CSym *s = cg2c_slindex(CSym*, p->syms, i);

		if (s->def == NULL)
			continue;

		// dumpAST(s->def, 0);

		if (s->def->op != ODCLFUNC)
			continue;

		Symbol *s2 = cg2c_gen(p->ctxt, s);
		o = cg2c_slappend(sizeof(Symbol*), o, &s2, p->ctxt->gc);
	}

	return o;
}

// TODO: remove
static int64_t
sizeof_(Compile *ctxt, CType *t, Pos pos)
{
	assert(t->align > 0); // ensure this type is complete and size and align were filled-in
	return t->size;
}

static int64_t
alignof_(Compile *ctxt, CType *t, Pos pos)
{
	assert(t->align > 0);
	return t->align;
}

static CType*
arithconv(Compile *ctxt, CType *x, CType *y)
{
	if (y != NULL && x->kind < y->kind) {
		CType *tmp = x;
		x = y;
		y = tmp;
	}
	if (x->kind < TINT32)
		x = types[TINT32];
	return x;
}

static bool
typesEqual(CType *x, CType *y)
{
	if (x->kind != y->kind)
		return false;

	switch (x->kind) {
	case TBOOL:
	case TINT8:
	case TUINT8:
	case TINT16:
	case TUINT16:
	case TINT32:
	case TUINT32:
	case TINT64:
	case TUINT64:
	case TFLOAT16:
	case TFLOAT32:
	case TFLOAT64:
		return true;

	case TCOMPLEX:
		return typesEqual(x->elem, y->elem);

	case TVEC2:
	case TVEC3:
	case TVEC4:
		return typesEqual(x->elem, y->elem);

	case TPTR:
		return typesEqual(x->elem, y->elem);

	case TSTRUCT:
		return x == y; // meeeh

	case TVOID:
		return true;

	default:
		assert(0);
		return false;
	}
}

static Node*
implconv(Compile *ctxt, CType *type, Node *x)
{
	if (typesEqual(type, x->type))
		return x;

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OCONV;
	n->type = type;
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, ctxt->gc);
	n->pos = x->pos; // implicit cast has same position as its argument
	n->implicit = true;
	return n;
}

static Node*
newArithExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	if (x->type->kind > TCOMPLEX || y->type->kind > TCOMPLEX) {
		cg2c_errorfAt(ctxt, &pos, "invalid operation");
		return newBadExpr(ctxt, types[TINT32], pos);
	}

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = arithconv(ctxt, x->type, y->type);
	x = implconv(ctxt, n->type, x);
	y = implconv(ctxt, n->type, y);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

// TODO: split parseXOrADecl into parseXDecl and parseADecl. This is a bit
// tricky because these functions would share a lot in common, e.g. disallow
// initializer for extern and __shared variables, etc.

// ctxt can be CGLOBL or CAUTO, also doesn't need to be returning anything
Node*
parseXOrADecl(Noder *p, int ctxt)
{
	Pos pos = p->pos;

	Slice declList = NULL_SLICE; // of Node*

	if (got(p, LAuto)) {
		String name = parseName(p);

		want(p, LAssign);

		Node *x = parseExpr(p);

		CSym *s = cg2c_malloc(sizeof *s, p->ctxt->gc);
		s->name = name;
		s->type = x->type;
		s->class = ctxt;

		Node *dcl = cg2c_malloc(sizeof *dcl, p->ctxt->gc);
		dcl->op = ODCL;
		dcl->sym = s;
		dcl->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, p->ctxt->gc);
		dcl->pos = pos;

		s->def = dcl;

		declare(p, name, s, ctxt);

		declList = cg2c_slappend(sizeof(Node*), declList, &dcl, p->ctxt->gc);

		goto out;
	}

	DeclSpec ds = parseDeclSpec(p, true);

	int class = ds.class;
	if (class == CXXX)
		class = ctxt;

	do {
		Pos pos = p->pos;

		// TODO: fix handling of abdecl
		Decl d = parseDecl(p, ds.type, true, true);

		if (ds.class == CXXX)
			ds.class = ctxt;

		// Declare Name now because it can be referenced in the initializer
		// and/or function body.

		CSym *s = cg2c_malloc(sizeof *s, p->ctxt->gc);
		s->name = d.name;
		s->type = d.type;
		s->class = ds.class;

		// Collect global (includes static) symbols
		if (ds.class == CGLOBL && cg2c_strlen(d.name) > 0) // BUG: we shouldn't need to check name!!!
			p->syms = cg2c_slappend(sizeof(CSym*), p->syms, &s, p->ctxt->gc);

		// ODCL should just contain ONAME in x so it's more similar to OAS

		Node *dcl = cg2c_malloc(sizeof *dcl, p->ctxt->gc);
		dcl->op = ODCL;
		dcl->sym = s;
		dcl->pos = pos;

		bool nonTypedefedFunc = d.type->kind == TFUNC && ds.type->kind != TFUNC;
		if (cg2c_sllen(declList) == 0 && nonTypedefedFunc && ctxt != CAUTO && got(p, LLBrace)) {
			assert(d.type != NULL);

			s = declare(p, d.name, s, ctxt);
			dcl->sym = s;
			s->def = dcl;

			cg2c_openScope(p);

			for (size_t i = 0; i < cg2c_sllen(d.type->fields); i++) {
				CField f = cg2c_slindex(CField, d.type->fields, i);

				CSym *arg = cg2c_malloc(sizeof *arg, p->ctxt->gc);
				arg->name = f.name;
				arg->type = f.type;
				arg->class = CPARAM;
				declare(p, arg->name, arg, CPARAM);

				s->args = cg2c_slappend(sizeof(CSym*), s->args, &arg, p->ctxt->gc);
			}

			dcl->op = ODCLFUNC;
			dcl->stmts = parseStmtList(p, &(Targets) {.return_ = d.type->elem});

			cg2c_closeScope(p);

			want(p, LRBrace);
			return dcl;
		}

		if (d.type->kind == TFUNC)
			s->class = CEXTERN;

		s->def = dcl;

		declare(p, d.name, s, ctxt);

		if (got(p, LAssign)) {
			// TODO: depending on storage class,

			// if (got(p, LLBrace)) // designated initializer

			Node *x = parseExpr(p);
			x = implconv(p->ctxt, d.type, x);
			dcl->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, p->ctxt->gc);
		}

		declList = cg2c_slappend(sizeof(Node*), declList, &dcl, p->ctxt->gc);
	} while (got(p, LComma));

out:
	want(p, LSemi);

	Node *n = cg2c_malloc(sizeof *n, p->ctxt->gc);
	n->op = ODCLLIST;
	n->stmts = declList;
	return n;
}

static int tokenToTypebit[NToken] = {
	[LChar]     = BCHAR,
	[LShort]    = BSHORT,
	[LInt]      = BINT,
	[LLong]     = BLONG,
	[LSigned]   = BSIGNED,
	[LUnsigned] = BUNSIGNED,
	[LFloat]    = BFLOAT,
	[LDouble]   = BDOUBLE,
	[LBool]     = BBOOL,
	[LVoid]     = BVOID,
};

static int tokenToGarb[NToken] = {
	[LConst]    = GCONST,
	[LVolatile] = GVOLATILE,
	[LRestrict] = GRESTRICT,
	[LAtomic]   = GATOMIC,
};

static int tokenToClass[NToken] = {
	[LTypedef] = CTYPEDEF,
	[LStatic]  = CGLOBL, // BUG
};

DeclSpec
parseDeclSpec(Noder *p, bool classOk)
{
	int garb = 0;
	(void)garb;

	int a = 0;

	CType *type = cg2c_malloc(sizeof *type, p->ctxt->gc);

	type->kind = combine(p->ctxt, a);

	int class = CXXX;

	bool cmplx = false;

loop:;
	int b = tokenToTypebit[p->tok];
	if (b != 0) {
		int aplusb = a + b;
		if (combine(p->ctxt, aplusb) == TXXX) {
			cg2c_errorfAt(p->ctxt, &p->pos, "unexpected type specifier");
		} else {
			type->kind = combine(p->ctxt, a = aplusb); // TODO: separate CType for typebits
			type->size = types[type->kind]->size;
			type->align = types[type->kind]->align;
		}
		cg2c_scan(p);
		goto loop;
	}
	switch (p->tok) {
	case LComplex: {
		cmplx = true;
		cg2c_scan(p);
		goto loop;
	}

	case LStruct: {
		cg2c_scan(p);
		type = parseStruct(p, TSTRUCT);
		break;
	}

	case LUnion: {
		cg2c_scan(p);
		type = parseStruct(p, TUNION);
		break;
	}

	case LEnum: {
		cg2c_scan(p);
		type = parseEnum(p);
		break;
	}

	case LName: { // TODO: this type may need to be modified (e.g. garb bits reset)
		if (a || garb)
			break;
		CSym *s = cg2c_lookup(p, p->lit);
		if (s == NULL || s->class != CTYPEDEF)
			break;
		type = s->type;
		cg2c_scan(p);
		break;
	}
	}

	int g = tokenToGarb[p->tok];
	if (g != GXXX) {
		garb |= g;
		cg2c_scan(p);
		goto loop;
	}

	int c = tokenToClass[p->tok];
	if (c != CXXX) {
		if (!classOk)
			syntaxErrorfAt(p, p->pos, "unexpected storage class specifier");
		else if (class != CXXX)
			syntaxErrorfAt(p, p->pos, "multiple storage class specifiers");
		else
			class = c;
		cg2c_scan(p);
		goto loop;
	}

	if (cmplx) {
		CType *cmplxType = cg2c_malloc(sizeof *cmplxType, p->ctxt->gc);
		cmplxType->kind = TCOMPLEX;
		cmplxType->size = 2 * type->size;
		cmplxType->align = type->align;
		cmplxType->elem = type;
		type = cmplxType;
	}

	return (DeclSpec) {.type = type, .class = class};
}

CType*
parseStruct(Noder *p, CKind kind)
{
	CType *stype = cg2c_malloc(sizeof *stype, p->ctxt->gc);
	stype->kind = kind;

	if (p->tok == LName) {
		String tag = parseName(p);
		(void)tag;

		if (p->tok != LLBrace)
			return stype;

		// TODO: more
	}

	want(p, LLBrace);

	int64_t size = 0;
	int64_t align = 0;
	while (p->tok != LEOF && p->tok != LRBrace) {
		if (!haveDecl(p))
			syntaxErrorfAt(p, p->pos, "expecting declaration");

		DeclSpec ds = parseDeclSpec(p, false);

		do {
			Decl d = parseDecl(p, ds.type, true, false); // TODO: embedding

			if (got(p, LColon))
				parseExpr(p); // TODO: bit-fields

			// TODO: make this less ugly

			int64_t fsize = d.type->size;
			int64_t falign = d.type->align;

			assert(falign > 0 && (falign&(falign-1)) == 0);
			int64_t off = 0;
			if (kind == TSTRUCT)
				off = (size + falign - 1) &~ (falign - 1);
			else if (kind == TUNION)
				off = 0;
			else
				assert(0);

			stype->fields = cg2c_slappend(sizeof(CField), stype->fields, &(CField) {
				.name = d.name,
				.type = d.type,
				.off  = off,
			}, p->ctxt->gc);

			if (size < off+fsize)
				size = off+fsize;
			if (align < falign)
				align = falign;
		} while (got(p, LComma));

		if (!got(p, LSemi))
			break;
	}

	// Round struct's size to its alignment
	size = ((size + align - 1) &~ (align - 1));

	stype->size = size;
	stype->align = align;

	want(p, LRBrace);

	return stype;
}

CType*
parseEnum(Noder *p)
{
	CType *type = types[TINT32];

	if (p->tok == LName) {
		String tag = parseName(p);
		(void)tag;

		if (p->tok != LLBrace)
			return type; // incomplete

		// TODO: declare tag
	}

	want(p, LLBrace);

	int64_t iota = -1;
	while (p->tok != LEOF && p->tok != LRBrace) {
		Pos pos = p->pos;

		String name = parseName(p);

		if (got(p, LAssign)) {
			Node *x = parseExpr(p);
			if (checkConstExpr(p->ctxt, x))
				iota = cg2c_ceval(p->ctxt, x);
			else
				iota = 0;
		} else {
			iota++;
		}

		CSym *s = cg2c_malloc(sizeof *s, p->ctxt->gc);
		s->name = name;
		s->type = types[TINT32];
		s->class = CCONST;
		s->defInt = (uint64_t)iota;
		declare(p, s->name, s, CAUTO /* doesn't matter for now */);

		if (!got(p, LComma))
			break;
	}

	want(p, LRBrace);

	return type;
}

Decl
parseDecl(Noder *p, CType *type, bool declOk, bool abdeclOk)
{
	type = parsePtrTo(p, type);
	if (got(p, LLParen)) {
		bool haveParamList = (!declOk || p->tok != LName) && haveDecl(p) || p->tok == LRParen;
		if (abdeclOk && haveParamList) {
			// type "(" param-list ")" ...
			CType *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
			t->kind = TFUNC;
			t->fields = parseParamList(p);
			want(p, LRParen);
			t->elem = parseDeclSuffix(p, type);
			return (Decl) {.type = t};
		}
		// BUG: this fails on int ((x))
		/*
			// type "(" name ")" ...
			CType *tmp = cg2c_malloc(sizeof *tmp, p->ctxt->gc);
			Decl d = parseDecl(p, tmp, declOk, abdeclOk);
			want(p, LRParen);
			*tmp = *parseDeclSuffix(p, type);
			return d;
		*/
		// BUG: this is incorrect, but works for now
		Decl d = parseDecl(p, type, declOk, abdeclOk);
		want(p, LRParen);
		return d;
	}

	String name = {0};
	if (declOk) {
		if (!abdeclOk || p->tok == LName)
			name = parseName(p);
	} else {
		assert(abdeclOk);
	}

	type = parseDeclSuffix(p, type);

	return (Decl) {.name = name, .type = type};
}

// TODO: should take Noder *p instead
static CType*
newPtrTo(Compile *ctxt, CType *elem, int garb, Pos pos)
{
	CType *t = cg2c_malloc(sizeof *t, ctxt->gc);
	t->kind = TPTR;
	t->garb = garb;
	t->size = ctxt->ptrSize;
	t->align = ctxt->ptrSize;
	t->elem = elem;
	t->pos = pos;
	return t;
}

CType*
parsePtrTo(Noder *p, CType *elem)
{
	Pos pos = p->pos;

	if (got(p, LStar)) {
		int garb = GXXX;
		for (;;) {
			int g = tokenToGarb[p->tok];
			if (g == GXXX)
				break;
			garb |= g;
			cg2c_scan(p);
		}
		return newPtrTo(p->ctxt, elem, garb, pos);
	}
	return elem;
}

CType*
parseDeclSuffix(Noder *p, CType *elem)
{
	switch (p->tok) {
	case LLParen: {
		cg2c_scan(p);
		CType *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
		t->kind = TFUNC;
		// BUG: this is incorrect but should be enough to be able to
		// store functions into variables
		t->size = 8;
		t->align = 8;
		t->fields = parseParamList(p);
		want(p, LRParen);
		t->elem = parseDeclSuffix(p, elem);
		//t->elem->garb = 0;
		return t;
	}

	case LLBrack: {
		cg2c_scan(p);
		CType *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
		t->kind = TARRAY;
		Node *lenExpr = NULL;
		if (p->tok != LRBrack)
			lenExpr = parseExpr(p);
		want(p, LRBrack);
		t->elem = parseDeclSuffix(p, elem);
		if (lenExpr != NULL) {
			int64_t len = cg2c_ceval(p->ctxt, lenExpr);
			t->size = len * t->elem->size;
			t->align = t->elem->align;
		}
		//t->elem->garb = 0; ?
		return t;
	}

	default:
		return elem;
	}
}

Slice
parseParamList(Noder *p)
{
	// TODO: continue instead of breaking early for better error recovery

	Slice params = NULL_SLICE;

	cg2c_openScope(p);

	do {
		if (got(p, LDotDotDot))
			break;

		Decl d = parseDecl(p, parseDeclSpec(p, false).type, true, true); // allow unnamed parameters

		// Check for (void) the same way GCC and Clang do.
		if (cg2c_sllen(params) == 0 && cg2c_strlen(d.name) == 0 && d.type->kind == TVOID && d.type->garb == 0)
			break;

		params = cg2c_slappend(sizeof(CField), params, &(CField) {
			.name = d.name,
			.type = /* paramconv */ d.type,
		}, p->ctxt->gc);
	} while (got(p, LComma));

	// TODO: tags declared in param list should be visible in function body
	cg2c_closeScope(p);

	return params;
}

// parseType parses an abstract declarator.
CType* parseType(Noder *p) { return parseDecl(p, parseDeclSpec(p, false).type, false, true).type; }

bool
haveDecl(Noder *p)
{
	switch (p->tok) {
	case LName: {
		CSym *s = cg2c_lookup(p, p->lit);
		return s != NULL && s->class == CTYPEDEF;
	}
	case LAuto:
	case LAlignas:
	case LComplex:
	case LStruct:
	case LUnion:
	case LEnum:
		return true;
	default:
		return tokenToTypebit[p->tok] || tokenToGarb[p->tok] || tokenToClass[p->tok];
	}
}

Node*
parseStmt(Noder *p, Targets *ctxt)
{
	Pos pos = p->pos;
	switch (p->tok) {
	default: {
		if (got(p, LSemi))
			return newNopStmt(p->ctxt, pos);
		Node *x = parseCommaExpr(p);
		want(p, LSemi);
		checkDiscarded(p->ctxt, x);
		return x;
	}

	case LLBrace: {
		cg2c_scan(p);
		cg2c_openScope(p);
		Slice stmts = parseStmtList(p, ctxt);
		cg2c_closeScope(p);
		want(p, LRBrace);
		return newBlockStmt(p->ctxt, stmts, pos);
	}

	case LIf: {
		cg2c_scan(p);
		want(p, LLParen);
		Node *cond = parseCommaExpr(p);
		want(p, LRParen);
		Node *then = parseStmt(p, ctxt);
		Node *else_ = NULL;
		if (got(p, LElse))
			else_ = parseStmt(p, ctxt);
		return newIfStmt(p->ctxt, cond, then, else_, pos);
	}

	case LFor: {
		cg2c_scan(p);
		cg2c_openScope(p);
		want(p, LLParen);
		Node *init = NULL;
		if (haveDecl(p)) {
			init = parseXOrADecl(p, CAUTO);
		} else if (!got(p, LSemi)) {
			init = parseCommaExpr(p);
			want(p, LSemi);
		}
		Node *cond = NULL;
		if (p->tok != LSemi)
			cond = parseCommaExpr(p);
		want(p, LSemi);
		Node *step = NULL;
		if (p->tok != LSemi)
			step = parseCommaExpr(p);
		want(p, LRParen);
		Node *body = parseStmt(p, ctxt);
		cg2c_closeScope(p);
		return newForStmt(p->ctxt, init, cond, step, body, pos);
	}

	case LWhile: {
		cg2c_scan(p);
		want(p, LLParen);
		Node *cond = parseCommaExpr(p);
		want(p, LRParen);
		Node *body = parseStmt(p, ctxt);
		return newForStmt(p->ctxt, NULL, cond, NULL, body, pos);
	}

	case LDo: {
		cg2c_scan(p);
		Node *body = parseStmt(p, ctxt);
		want(p, LWhile);
		want(p, LLParen);
		Node *cond = parseCommaExpr(p);
		want(p, LRParen);
		return newDoWhileStmt(p->ctxt, body, cond, pos);
	}

	/*
		case LBreak: {
		}

		case LContinue: {
		}
	*/

	case LReturn: {
		cg2c_scan(p);
		Node *result = NULL;
		if (p->tok != LSemi)
			result = parseCommaExpr(p);
		want(p, LSemi);
		return newReturnStmt(p->ctxt, result, pos);
	}
	}
}

Slice
parseStmtList(Noder *p, Targets *ctxt)
{
	Slice l = NULL_SLICE;
	while (p->tok != LEOF && p->tok != LRBrace) {
		Node *s;
		if (haveDecl(p))
			s = parseXOrADecl(p, CAUTO);
		else
			s = parseStmt(p, ctxt);
		l = cg2c_slappend(sizeof(Node*), l, &s, p->ctxt->gc);
	}
	return l;
}

enum {
	precXxx,
	precComma,
	precAssign,
	precCond,
	precEtc,
};

Node*
parseCommaExpr(Noder *p)
{
	Node *x = parseExpr(p);
	while (p->tok == LComma) {
		Pos pos = p->pos;
		cg2c_scan(p);
		Node *y = parseExpr(p);
		x = newCommaExpr(p->ctxt, x, y, pos);
	}
	return x;
}

Node* parseExpr(Noder *p) { return parseBinaryExpr(p, 0); }

typedef struct BinOpInfo BinOpInfo;
struct BinOpInfo {
	int prec;

	Node* (*newExpr)(Compile*, COp, Node*, Node*, Pos);
	COp op;
};

static Node*
newBitwiseExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	if (x->type->kind > TUINT64 || y->type->kind > TUINT64) {
		cg2c_errorfAt(ctxt, &pos, "invalid operation");
		return newBadExpr(ctxt, types[TINT32], pos);
	}

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = arithconv(ctxt, x->type, y->type);
	x = implconv(ctxt, n->type, x);
	y = implconv(ctxt, n->type, y);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

static BinOpInfo binOps[NOperator] = {
	[KOrOr]   = {precEtc + 0, newLogicalExpr, OOROR},
	[KAndAnd] = {precEtc + 1, newLogicalExpr, OANDAND},
	[KOr]     = {precEtc + 2, newBitwiseExpr, OOR},
	[KXor]    = {precEtc + 3, newBitwiseExpr, OXOR},
	[KAnd]    = {precEtc + 4, newBitwiseExpr, OAND},
	[KEq]     = {precEtc + 5, newCmpExpr, OEQ},
	[KNe]     = {precEtc + 5, newCmpExpr, ONE},
	[KLt]     = {precEtc + 6, newCmpExpr, OLT},
	[KLe]     = {precEtc + 6, newCmpExpr, OLE},
	[KGt]     = {precEtc + 6, newCmpExpr, OGT},
	[KGe]     = {precEtc + 6, newCmpExpr, OGE},
	[KShl]    = {precEtc + 7, newShiftExpr, OLSH},
	[KShr]    = {precEtc + 7, newShiftExpr, ORSH},
	[KAdd]    = {precEtc + 8, newAddExpr, OADD},
	[KSub]    = {precEtc + 8, newSubExpr, OSUB},
	[KMul]    = {precEtc + 9, newArithExpr, OMUL},
	[KDiv]    = {precEtc + 9, newArithExpr, ODIV},
	[KRem]    = {precEtc + 9, newArithExpr, OMOD}, // special
};

Node*
parseBinaryExpr(Noder *p, int minprec)
{
	Node *x = parseUnaryExpr(p); // TODO: allow x to be provided for label parsing
	for (;;) {
		Pos pos = p->pos;
		switch (p->tok) {
		case LOp:
		case LStar: {
			BinOpInfo *o = &binOps[p->op];
			int tprec = o->prec;
			if (tprec <= minprec)
				goto done;
			cg2c_scan(p);
			Node *y = parseBinaryExpr(p, tprec);
			x = o->newExpr(p->ctxt, o->op, x, y, pos);
			break;
		}

		case LAssign: {
			int tprec = precAssign;
			if (tprec < minprec)
				goto done;
			cg2c_scan(p);
			Node *y = parseBinaryExpr(p, tprec);
			x = newAssignExpr(p->ctxt, x, y, pos);
			break;
		}

		case LAssignOp: {
			int tprec = precAssign;
			if (tprec < minprec)
				goto done;
			COp op = binOps[p->op].op;
			cg2c_scan(p);
			Node *y = parseBinaryExpr(p, tprec);
			x = newAssignOpExpr(p->ctxt, op, x, y, pos);
			break;
		}

		case LQuestion: {
			int tprec = precCond;
			if (tprec < minprec)
				goto done;
			cg2c_scan(p);
			Node *then = parseCommaExpr(p);
			want(p, LColon);
			Node *else_ = parseBinaryExpr(p, tprec);
			x = newCondExpr(p->ctxt, x, then, else_, pos);
			break;
		}

		default:
			goto done;
		}
	}
done:
	return x;
}

static Node* (*unOps[NOperator])(Compile*, Node*, Pos) = {
	[KMul] = newDerefExpr,
	[KAnd] = newAddrExpr,

	//[KNot] = {newUnaryExpr, ONOT},
	[KCom] = newComExpr, // should only work for integer types
	//[KAdd] = {newUnaryExpr, OPLUS},
	[KSub] = newNegExpr,
};

// parseUnaryExpr parses unary expressions. Casts, despite being syntactically
// unary expressions, are handled in parsePrimaryExpr together with composite
// literals (syntactically postfix expressions) and parenthesized expressions.
Node*
parseUnaryExpr(Noder *p)
{
	Pos pos = p->pos;
	switch (p->tok) {
	case LOp:
	case LStar: {
		if (!unOps[p->op])
			break;
		Node* (*newExpr)(Compile*, Node*, Pos) = unOps[p->op];
		cg2c_scan(p);
		Node *x = parseUnaryExpr(p);
		return newExpr(p->ctxt, x, pos);
	}

	case LOpOp: {
		COp op = binOps[p->op].op;
		cg2c_scan(p);
		Node *x = parseUnaryExpr(p);
		return newIncDecExpr(p->ctxt, OPREINCDEC, op, x, pos);
	}

	/*
		case LSizeof: {
			cg2c_scan(p);

			Pos pos2 = p->pos;

			CType *type = parseUnaryExprOrType(p);

			uint64_t size = sizeof_(p->ctxt, type, pos2);

			cg2c_errorfAt(p->ctxt, pos, "%llu", (unsigned long long)size);
			return newBadExpr(p->ctxt, sizeType(p->ctxt), pos);
		}
	*/
	}

	return parsePostfixExpr(p);
}

CType*
parseUnaryExprOrType(Noder *p)
{
	if (got(p, LLParen)) {
		CType *t;
		if (haveDecl(p))
			t = parseType(p);
		else
			t = parseCommaExpr(p)->type;
		want(p, LRParen);
		return t;
	}
	return parseUnaryExpr(p)->type;
}

Node*
parsePrimaryExpr(Noder *p)
{
	Pos pos = p->pos;
	switch (p->tok) {
	case LName: {
		CSym *s = cg2c_lookup(p, p->lit);
		if (s == NULL) {
			cg2c_errorfAt(p->ctxt, &pos, "undefined: %.*s", cg2c_strlen2(p->lit), cg2c_strdata(p->lit));
			cg2c_scan(p);
			return newBadExpr(p->ctxt, types[TINT32], pos);
		}
		if (s->class == CTYPEDEF) {
			cg2c_errorfAt(p->ctxt, &pos, "type %.*s is not an expression", cg2c_strlen2(p->lit), cg2c_strdata(p->lit));
			cg2c_scan(p);
			return newBadExpr(p->ctxt, types[TINT32], pos);
		}
		if (s->class == CCONST) {
			cg2c_scan(p);
			Node *n = cg2c_malloc(sizeof *n, p->ctxt->gc);
			n->op = OCONST;
			n->type = s->type;
			n->asd = s->defInt;
			n->pos = pos;
			return n;
		}
		Node *n = cg2c_malloc(sizeof *n, p->ctxt->gc);
		n->op = ONAME;
		n->type = s->type;
		n->sym = s;
		n->pos = pos;
		cg2c_scan(p);
		return n;
	}

	case LNumber:
	case LChar_:
		return parseNumber(p);

	/*
		case LString: {
			Node *x = cg2c_malloc(sizeof *x, p->ctxt->gc);
			x->pos = p->pos;
			x->op = OCONST;
			x->lit = parseString(p);
			//x->type =
			return x;
		}
	*/

	case LLParen: {
		cg2c_scan(p);
		if (!haveDecl(p)) {
			Node *x = parseCommaExpr(p);
			want(p, LRParen);
			return x;
		}
		Node *x = cg2c_malloc(sizeof *x, p->ctxt->gc);
		x->op = OCONV;
		x->type = parseType(p);
		want(p, LRParen);
		/*
			if (p->tok == LLBrace) {
				if (type->kind == TFUNC)
					assert(!"anonymous functions not implemented");
				assert(!"composite literals not implemented");
			}
		*/
		Node *y = parseUnaryExpr(p);
		x->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {y}, 1, p->ctxt->gc);
		x->pos = pos;
		return x;
	}

	default: {
		cg2c_errorfAt(p->ctxt, &pos, "expecting expression");
		advance(p);
		return newBadExpr(p->ctxt, types[TINT32], pos);
	}
	}
}

// This should really be a universal thing, every time anything is used as a
// value. We should also introduce a CType*-only variant of this as function
// parameters undergo the same decay.
static Node*
usecast(Compile *ctxt, Node *x)
{
	if (x->type->kind == TARRAY) {
		return implconv(ctxt, newPtrTo(ctxt, x->type->elem, 0, x->pos), x);
	}
	// TODO: also do the same treatment for TFUNC
	return x;
}

Node*
parsePostfixExpr(Noder *p)
{
	Node *x = parsePrimaryExpr(p);
	for (;;) {
		Pos pos = p->pos;
		switch (p->tok) {
		case LLBrack: {
			x = usecast(p->ctxt, x);
			Node *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
			t->op = OINDEX;
			t->type = x->type->elem;
			cg2c_scan(p);
			Node *y = parseCommaExpr(p);
			t->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, p->ctxt->gc);
			want(p, LRBrack);
			t->pos = pos;
			x = t;
			break;
		}

		case LLParen: {
			CType *ftype = x->type;
			assert(ftype->kind == TFUNC);

			Node *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
			t->op = OCALL;
			t->type = ftype->elem;
			// t->x = x;
			cg2c_scan(p);
			Slice args = NULL_SLICE;
			args = cg2c_slappend(sizeof(Node*), args, &x, p->ctxt->gc);
			// or while (p->tok != LEOF && p->tok != LRParen) if we want to allow trailing comma
			if (p->tok != LRParen) {
				size_t i = 0;
				do {
					CField f = cg2c_slindex(CField, ftype->fields, i);
					Node *a = parseExpr(p);
					a = implconv(p->ctxt, f.type, a);
					args = cg2c_slappend(sizeof(Node*), args, &a, p->ctxt->gc);
					i++;
				} while (got(p, LComma));
			}
			t->args = args;
			want(p, LRParen);
			t->pos = pos;
			x = t;
			break;
		}

		// LDot and LArrow could be struct or union field access, which
		// could also be a bit-field, vector extract or insert or
		// shuffle.

		case LDot: {
			Pos pos = p->pos;
			cg2c_scan(p);
			String sel = parseName(p);
			switch (x->type->kind) {
			case TSTRUCT:
			case TUNION: {
				bool ok = false;
				// TODO: implement struct embedding
				for (size_t i = 0; i < cg2c_sllen(x->type->fields); i++) {
					CField f = cg2c_slindex(CField, x->type->fields, i);
					if (cg2c_strcmp(f.name, sel) == 0) {
						Node *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
						t->op = ODOT;
						t->type = f.type;
						t->field = i;
						t->x = x;
						t->pos = pos;
						x = t;

						ok = true;
						break;
					}
				}
				if (!ok) {
					cg2c_errorfAt(p->ctxt, &pos, "struct has no field %.*s", cg2c_strlen2(sel), cg2c_strdata(sel));
				}
				break;
			}

			case TVEC2: case TVEC3: case TVEC4: {
				assert(cg2c_strlen(sel) == 1);
				Node *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
				t->op = ODOTSHUF;
				t->type = x->type->elem;
				for (size_t i = 0; i < cg2c_strlen(sel); i++) {
					switch (cg2c_strdata(sel)[i]) {
					case 'x':
						t->shuf[i] = 0;
						break;
					case 'y':
						t->shuf[i] = 1;
						break;
					case 'z':
						t->shuf[i] = 2;
						break;
					case 'w':
						t->shuf[i] = 3;
						break;
					default:
						assert(0);
					}
				}
				t->x = x;
				t->pos = pos;
				x = t;
				break;
			}

			default:
				cg2c_errorfAt(p->ctxt, &pos, "dot");
			}
			break;
		}

		case LArrow: {
			Pos pos = p->pos;
			cg2c_scan(p);
			String sel = parseName(p);
			switch (x->type->elem->kind) {
			case TSTRUCT:
			case TUNION: {
				bool ok = false;
				// TODO: implement struct embedding
				for (size_t i = 0; i < cg2c_sllen(x->type->elem->fields); i++) {
					CField f = cg2c_slindex(CField, x->type->elem->fields, i);
					if (cg2c_strcmp(f.name, sel) == 0) {
						Node *t = cg2c_malloc(sizeof *t, p->ctxt->gc);
						t->op = OARROW;
						t->type = f.type;
						t->field = i;
						t->x = x;
						t->pos = pos;
						x = t;

						ok = true;
						break;
					}
				}
				if (!ok) {
					cg2c_errorfAt(p->ctxt, &pos, "struct has no field %.*s", cg2c_strlen2(sel), cg2c_strdata(sel));
				}
				break;
			}

			//case TVECTOR:

			default:
				cg2c_errorfAt(p->ctxt, &pos, "arrow");
			}
			break;
		}

		case LOpOp: {
			COp op = binOps[p->op].op;
			cg2c_scan(p);
			x = newIncDecExpr(p->ctxt, OPOSTINCDEC, op, x, pos);
			break;
		}

		default:
			goto done;
		}
	}
done:
	return x;
}

String
parseName(Noder *p)
{
	String n = {0};
	if (p->tok == LName)
		n = p->lit;
	want(p, LName);
	return n;
}

// TODO: rename it so that it's clear that it's a helper for parsing C integer
// literals and report errors
uint64_t
cg2c_strtou64(String s, int base, GC *gc)
{
	char *c = cg2c_malloc(cg2c_strlen(s)+1, gc);
	memmove(c, cg2c_strdata(s), cg2c_strlen(s));
	return strtoull(c, NULL, base);
}

// TODO: rename it so that it's clear that it's a helper for parsing C float
// literals and report errors
static double
cg2c_strtod(String s, GC *gc)
{
	char *c = cg2c_malloc(cg2c_strlen(s)+1, gc);
	memmove(c, cg2c_strdata(s), cg2c_strlen(s));
	return strtod(c, NULL);
}

static bool
cg2c_strhasprefix(String s, String prefix)
{
	size_t n = cg2c_strlen(s);
	size_t m = cg2c_strlen(prefix);
	return n >= m && cg2c_strcmp(cg2c_strslice(s, 0, m), prefix) == 0;
}

static bool
cg2c_strhassuffix(String s, String suffix)
{
	size_t n = cg2c_strlen(s);
	size_t m = cg2c_strlen(suffix);
	return n >= m && cg2c_strcmp(cg2c_strslice(s, n-m, n), suffix) == 0;
}

static bool
cg2c_strcontains(String s, char what)
{
	const char *c = cg2c_strdata(s);
	for (size_t i = 0; i < cg2c_strlen(s); i++) {
		if (c[i] == what)
			return true;
	}
	return false;
}

static uint32_t
floatbits(float x)
{
	uint32_t y;
	memmove(&y, &x, sizeof y);
	return y;
}

static uint64_t
doublebits(double x)
{
	uint64_t y;
	memmove(&y, &x, sizeof y);
	return y;
}

static double
doublefrombits(uint64_t x)
{
	double y;
	memmove(&y, &x, sizeof y);
	return y;
}

Node*
parseNumber(Noder *p)
{
	Pos pos = p->pos;

	int a = TINT32;

	uint64_t val = 0;

	switch (p->tok) {
	case LNumber: {
		// BUG: properly parse numbers

		if (cg2c_strcontains(p->lit, '.')) {
			double d = cg2c_strtod(p->lit, p->ctxt->gc);

			if (cg2c_strcontains(p->lit, 'f') || cg2c_strcontains(p->lit, 'F')) {
				a = TFLOAT32;
				val = (uint64_t)floatbits((float)d);
			} else {
				a = TFLOAT64;
				val = doublebits(d);
			}
		} else {
			if (cg2c_strhasprefix(p->lit, cg2c_stringnocopy("0b"))) {
				val = cg2c_strtou64(cg2c_strslice(p->lit, 2, cg2c_strlen(p->lit)), 2, p->ctxt->gc);
			} else if (cg2c_strhasprefix(p->lit, cg2c_stringnocopy("0x"))) {
				val = cg2c_strtou64(cg2c_strslice(p->lit, 2, cg2c_strlen(p->lit)), 16, p->ctxt->gc);
			} else {
				val = cg2c_strtou64(p->lit, 10, p->ctxt->gc);
			}

			// BUG: this depends on signedness
			if (val > 0xffffffff)
				a = TINT64;
		}

		cg2c_scan(p);
		break;
	}

	case LChar_:
		cg2c_scan(p);

		// fallthrough for now

	default:
		cg2c_errorfAt(p->ctxt, &pos, "expecting number or character");
	}

	Node *n = cg2c_malloc(sizeof *n, p->ctxt->gc);
	n->op = OCONST;
	n->type = types[a];
	n->asd = val;
	n->pos = pos;
	return n;
}

String
parseString(Noder *p)
{
	String s = {0};
	if (p->tok == LString)
		s = p->lit;
	want(p, LString);
	return s;
}

static char* tokenNames[NToken] = {
	[LEOF] = "EOF",

	[LName]   = "name",
	[LNumber] = "integer or floating point constant",
	[LChar_]  = "character constant",
	[LString] = "string literal",

	[LOp]       = "op",
	[LStar]     = "*",
	[LAssign]   = "=",
	[LAssignOp] = "op=",
	[LOpOp]     = "++ or --",

	[LLParen]    = "(",
	[LLBrack]    = "[",
	[LLBrace]    = "{",
	[LRParen]    = ")",
	[LRBrack]    = "]",
	[LRBrace]    = "}",
	[LComma]     = ",",
	[LSemi]      = ";",
	[LArrow]     = "->",
	[LDot]       = ".",
	[LDotDotDot] = "...",
	[LColon]     = ":",
	[LQuestion]  = "?",
};

bool
got(Noder *p, int tok)
{
	if (p->tok == tok) {
		cg2c_scan(p);
		return true;
	}
	return false;
}

void
want(Noder *p, int tok)
{
	if (!got(p, tok)) {
		cg2c_errorfAt(p->ctxt, &p->pos, "expecting %s", tokenNames[tok]);
		advance(p);
	}
}

void
advance(Noder *p /*, followset */)
{
	for (;;) {
		switch (p->tok) {
		case LEOF:
		// TODO: more tokens
			goto done;
		}

		cg2c_scan(p);
		break;
	}
done:;
}

void
syntaxErrorfAt(Noder *p, Pos pos, const char *format, ...)
{
	if (p->syntaxErrored && p->tok == LEOF)
		return;

	char b[1024] = {0};
	va_list arg;
	va_start(arg, format);
	vsnprintf(b, (sizeof b)-1, format, arg);
	va_end(arg);

	cg2c_errorfAt(p->ctxt, &pos, "%s", b);

	p->syntaxErrored = true;
}

Node*
newAddExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	assert(op == OADD);

	if (x->type->kind == TPTR) {
		Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
		n->op = OADD;
		n->type = x->type;
		n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
		n->pos = pos;
		return n;
	}

	if (y->type->kind == TPTR) {
	}

	return newArithExpr(ctxt, OADD, x, y, pos);
}

Node*
newSubExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	assert(op == OSUB);

	if (x->type->kind == TPTR) {
	}

	return newArithExpr(ctxt, OSUB, x, y, pos);
}

Node*
newShiftExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	if (x->type->kind > TUINT64 || y->type->kind > TUINT64) {
		cg2c_errorfAt(ctxt, &pos, "invalid operation");
		return newBadExpr(ctxt, types[TINT32], pos);
	}

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = arithconv(ctxt, x->type, NULL);
	x = implconv(ctxt, n->type, x);
	y = implconv(ctxt, arithconv(ctxt, y->type, NULL), y);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newCmpExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	CType *type = arithconv(ctxt, x->type, y->type);

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = types[TINT32];
	n->x = implconv(ctxt, type, x);
	n->y = implconv(ctxt, type, y);
	n->pos = pos;
	return n;
}

Node*
newLogicalExpr(Compile *ctxt, COp op, Node *x, Node *y, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = types[TINT32];
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newCondExpr(Compile *ctxt, Node *cond, Node *x, Node *y, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OCOND;
	n->type = x->type; // TODO: arithconv
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {cond, x, y}, 3, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newCommaExpr(Compile *ctxt, Node *x, Node *y, Pos pos)
{
	checkDiscarded(ctxt, x);

	// TODO: pos
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OCOMMA;
	n->type = y->type;
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newAssignExpr(Compile *ctxt, Node *x, Node *y, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OAS;
	n->type = x->type;
	y = implconv(ctxt, x->type, y);
	n->x = x;
	n->y = y;
	n->pos = pos;
	return n;
}

Node*
newAssignOpExpr(Compile *ctxt, COp asOp, Node *x, Node *y, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OASOP;
	n->type = x->type;
	n->asOp = asOp;
	y = implconv(ctxt, x->type, y);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x, y}, 2, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newDerefExpr(Compile *ctxt, Node *x, Pos pos)
{
	// TODO: perform conversion so that TARRAY becomes TPTR, TFUNC becomes
	// pointer to TFUNC, etc

	if (x->type->kind != TPTR) {
		cg2c_errorfAt(ctxt, &x->pos, "cannot indirect");
		return newBadExpr(ctxt, types[TINT32], pos);
	}

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = ODEREF;
	n->type = x->type->elem;
	n->x = x;
	n->pos = pos;
	return n;
}

Node*
newAddrExpr(Compile *ctxt, Node *x, Pos pos)
{
	// TODO: handle taking address of a function

	CType *type = newPtrTo(ctxt, x->type, GXXX, pos);

	// TODO: move addressability check out of this function

	switch (x->op) {
	case ONAME:
		break;

	// TODO: composite literals

	case ODOT:
	case OARROW:
		break;

	case OINDEX: // depends on meaning of this OINDEX, can't take addr of x[y] if x is a vector
	case ODEREF:
		break;

	default:
		cg2c_errorfAt(ctxt, &x->pos, "cannot take the address of");
		return newBadExpr(ctxt, type, pos);
	}

	// TODO: handle functions specially

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OADDR;
	n->type = type;
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, ctxt->gc);
	n->pos = pos;
	return n;
}

// almost the same for OPLUS and ONEG, this just has stricter checks
Node*
newComExpr(Compile *ctxt, Node *x, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OCOM;
	n->type = arithconv(ctxt, x->type, NULL);
	x = implconv(ctxt, n->type, x);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newNegExpr(Compile *ctxt, Node *x, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = ONEG;
	n->type = arithconv(ctxt, x->type, NULL);
	x = implconv(ctxt, n->type, x);
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newIncDecExpr(Compile *ctxt, COp op, COp asOp, Node *x, Pos pos)
{
	assert(op == OPOSTINCDEC || op == OPREINCDEC);
	assert(asOp == OADD || asOp == OSUB);

	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = op;
	n->type = x->type;
	n->asOp = asOp;
	n->args = cg2c_mkslice(sizeof(Node*), (Node*[]) {x}, 1, ctxt->gc);
	n->pos = pos;
	return n;
}

Node*
newBadExpr(Compile *ctxt, CType *type, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OBAD;
	n->type = type;
	n->pos = pos;
	return n;
}

Node*
newNopStmt(Compile *ctxt, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = ONOP;
	n->pos = pos;
	return n;
}

Node*
newBlockStmt(Compile *ctxt, Slice stmts, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OBLOCK;
	n->stmts = stmts;
	n->pos = pos;
	return n;
}

Node*
newIfStmt(Compile *ctxt, Node *cond, Node *then, Node *else_, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OIF;
	n->cond = cond;
	n->x = then;
	n->y = else_;
	n->pos = pos;
	return n;
}

Node*
newForStmt(Compile *ctxt, Node *init, Node *cond, Node *step, Node *body, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = OFOR;
	n->init = init;
	n->cond = cond;
	n->step = step;
	n->body = body;
	n->pos = pos;
	return n;
}

Node*
newDoWhileStmt(Compile *ctxt, Node *body, Node *cond, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = ODOWHILE;
	n->cond = cond;
	n->body = body;
	n->pos = pos;
	return n;
}

Node*
newReturnStmt(Compile *ctxt, Node *result, Pos pos)
{
	Node *n = cg2c_malloc(sizeof *n, ctxt->gc);
	n->op = ORETURN;
	n->x = result;
	n->pos = pos;
	return n;
}

static bool
isconstexpr(Node *n)
{
	switch (n->op) {
	case OCONST:
		return n->type->kind <= TUINT64;

	case OADD:
	case OSUB:
	case OMUL:
	case ODIV:
	case OMOD:
	case OLSH:
	case ORSH: {
		Node *x = cg2c_slindex(Node*, n->args, 0);
		Node *y = cg2c_slindex(Node*, n->args, 1);
		return n->type->kind <= TUINT64 && isconstexpr(x) && isconstexpr(y);
	}

	case OCOND: {
		Node *cond = cg2c_slindex(Node*, n->args, 0);
		Node *x = cg2c_slindex(Node*, n->args, 1);
		Node *y = cg2c_slindex(Node*, n->args, 2);
		return isconstexpr(cond) && isconstexpr(x) && isconstexpr(y);
	}

	default:
		return false;
	}
}

bool
checkConstExpr(Compile *ctxt, Node *x)
{
	bool ok = isconstexpr(x);
	if (!ok)
		cg2c_errorfAt(ctxt, &x->pos, "not a constant expression");
	return ok;
}

void
checkDiscarded(Compile *ctxt, Node *x)
{
	switch (x->op) {
	case OAS:
	case OASOP:
		return;

	case OPOSTINCDEC:
	case OPREINCDEC:
		return;

	case OCALL:
		return;

	default:
		if (x->type->kind != TVOID)
			cg2c_warnfAt(ctxt, &x->pos, "expression evaluated but not used");
		break;
	}
}

CKind
combine(Compile *ctxt, int a)
{
	switch (a) {
	case BBOOL:
		return TBOOL;

	case BCHAR:
		if (ctxt->unsignedChar)
			return TUINT8;
		return TINT8;

	case BSIGNED + BCHAR:
		return TINT8;

	case BUNSIGNED + BCHAR:
		return TUINT8;

	case BSHORT + BINT:
	case BSHORT:
	case BSIGNED + BSHORT + BINT:
	case BSIGNED + BSHORT:
		return TINT16;

	case BUNSIGNED + BSHORT + BINT:
	case BUNSIGNED + BSHORT:
		return TUINT16;

	case 0: // ?
	case BINT:
	case BSIGNED + BINT:
	case BSIGNED:
		return TINT32;

	case BUNSIGNED + BINT:
	case BUNSIGNED:
		return TUINT32;

	case BLONG + BINT:
	case BLONG:
	case BSIGNED + BLONG + BINT:
	case BSIGNED + BLONG:
		switch (ctxt->longSize) {
		case 4:
			return TINT32;
		case 8:
			return TINT64;
		default:
			assert(0);
		}

	case BUNSIGNED + BLONG + BINT:
	case BUNSIGNED + BLONG:
		switch (ctxt->longSize) {
		case 4:
			return TUINT32;
		case 8:
			return TUINT64;
		default:
			assert(0);
		}

	case BLONG + BLONG + BINT:
	case BLONG + BLONG:
	case BSIGNED + BLONG + BLONG + BINT:
	case BSIGNED + BLONG + BLONG:
		return TINT64;

	case BUNSIGNED + BLONG + BLONG + BINT:
	case BUNSIGNED + BLONG + BLONG:
		return TUINT64;

	case BFLOAT:
		return TFLOAT32;

	case BDOUBLE:
		return TFLOAT64;

	case BVOID:
		return TVOID;
	}

	return TXXX;
}
