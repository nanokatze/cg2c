#include "all.h"

// Rewrite some Loads into uses of the previously stored values.
//
// Based on The Simple and Efficient Construction of Static Single Assignment
// Form by M. Braun, S. Buchwald, S. Hack, R. Leißa, C. Mallon, and A. Zwinkau
// https://pp.info.uni-karlsruhe.de/uploads/publikationen/braun13cc.pdf

// BUG: our current loads to SSA isn't correct. We need to chase stores on a mem
// backwards and accumulate them into a bitcast-like thing, and bail once the
// location can not be reconciled at all with the one we're storing into.

// TODO: teach loadsToSSA to work with loops

// TODO: teach loadsToSSA to prepare data for elimDeadStores

typedef struct PerBlock PerBlock;
struct PerBlock {
	// TODO: we might be able to remove this thing once the general case is
	// handled better and we have a separate pass to do alias analysis and
	// place stores/loads on separate mem objects.
	Map *defs;

	// The last store into a location that isn't a sequence of AddPtrs into
	// a Var. This could also be made a list which we could chase a little.
	//Value *???;
};

typedef struct LoadsToSSA LoadsToSSA;
struct LoadsToSSA {
	Map *preds; // TODO: change this into Value* -> Slice

	Map *vars; // basic block -> PerBlock*

	Map *outputs; // Value* -> Slice of Slice of Value*

	bool changed;
};

// TODO: remove this in favor of just chasing mem backwards
static Value*
basicBlock(LoadsToSSA *ssa, Value *v)
{
	for (;;) {
		switch (v->op) {
		case OpParam:
			return v;

		case OpMakeTuple: {
			for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
				Value *a = cg2c_valueArg(v, i);
				if (cg2c_typeIs(a->type, KindMem)) {
					v = a;
					goto ok;
				}
			}
			assert(0);

		ok:
			break;
		}

		case OpExtract:
			// TODO: handle stuff here. If this is a suitable Call,
			// jump to the argument, otherwise, this is the
			// boundary.
			v = cg2c_valueArg(v, 0);
			break;

		// TODO: handle destructive uses of mem in a more generalized manner
		case OpStore:
		case OpAtomicRMW:
		case OpImageStore:
		case OpStoreOutput:
		case OpStoreBuiltin:
			v = cg2c_valueArg(v, 0);
			break;

		case OpIf: case OpLoop:
			return v;

		case OpCall: {
			// If we allow multiple mems, actually handling the Call
			// will be more complex:
			//
			// * if Call takes single mem and returns single mem, we
			//   just follow the argument mem.
			// * if Call takes multiple mems or returns multiple
			//   mems, this Call is like a fork or a merge.
			// v = cg2c_valueArg(v, 1);
			return v; // break;
		}

		default:
			assert(0);
			break;
		}
	}
}

static Value*
findMemImpl(Builder b, Value *v)
{
	switch (cg2c_typeKind(v->type)) {
	case KindMem:
		return v;

	case KindTuple:
		for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
			Value *e = cg2c_buildExtract(b, v, i);
			if (findMemImpl(b, e) != NULL)
				return e;
		}
		break;

	default:
		break;
	}

	return NULL;
}

static Value*
findMem(Builder b, Value *v)
{
	Value *mem = findMemImpl(b, v);
	assert(mem != NULL);
	return mem;
}

static Value* load(Builder b, Type *type, int64_t align, Value *mem, Value *ptr, LoadsToSSA *ssa, bool *changed);
static void store(Compile *ctxt, Value *mem, Value *ptr, Value *val, LoadsToSSA *ssa);

static Value*
loadsToSSA(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	Compile *ctxt = b.ctxt;
	LoadsToSSA *ssa = etc;

	switch (op) {
	case OpLoad: {
		int64_t align = *(int64_t*)aux;

		Value *mem = cg2c_slindex(Value*, args, 0);
		Value *ptr = cg2c_slindex(Value*, args, 1);

		bool changed = false;
		Value *v = load(b, type, align, mem, ptr, ssa, &changed);
		if (changed) {
			ssa->changed = true;
			// TODO: gate this behind a ctxt->flag
			if (false) {
				Pos pos = v->pos != NULL ? *v->pos : (Pos) {0};

				cg2c_warnfAt(ctxt, b.pos, "replaced a load with a value stored at :%u:%u", pos.line, pos.col);
			}
		}
		return v;
	}

	case OpStore: {
		Value *mem = cg2c_slindex(Value*, args, 0);
		Value *ptr = cg2c_slindex(Value*, args, 1);
		Value *val = cg2c_slindex(Value*, args, 2);

		store(ctxt, mem, ptr, val, ssa);

		break;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

static Value*
buildPhi(Builder b, Type *type, Value *pred, size_t phiIndex)
{
	Value* args[] = {pred, cg2c_buildConst(b, cg2c_types[KindInt32], phiIndex)};
	return cg2c_buildValue(b, OpPhi, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

// load will set changed if it managed to replace a load with a previously
// stored value, an earlier load or other things.
Value*
load(Builder b, Type *type, int64_t align, Value *mem, Value *ptr, LoadsToSSA *ssa, bool *changed)
{
	GC *gc = b.ctxt->gc;

	assert(cg2c_typeIs(mem->type, KindMem));

	Value *bb = basicBlock(ssa, mem);

	Map *defs = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, ssa->vars, &bb);

	Value *v = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, defs, &ptr);
	if (v != NULL) {
		if (v->type == type) {
			*changed = true;
			return v;
		}
	} else {
		switch (bb->op) {
		case OpParam: {
			// TODO: for ifs, there is only one predecessor, so just
			// load from there. For loops the current idea is that
			// we'll emit loads for the entry, but defer emitting
			// loads for the loop body.

			break;
		}

		case OpIf: {
			Value *preds[2] = {0};
			bool changed2 = false;
			for (size_t i = 0; i < 2; i++) {
				Region *r = cg2c_valueArgRegion(bb, 1+i);
				// BUG: this is incorrect for multiple mem values
				Value *mem2 = findMem(b, cg2c_regionTerminator(r));
				preds[i] = load(b, type, align, mem2, ptr, ssa, &changed2);
			}
			if (!changed2)
				break;

			if (preds[0] == preds[1]) {
				*changed = true;
				return preds[0];
			}

			Slice outputs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, ssa->outputs, &bb);
			if (cg2c_sllen(outputs) == 0) {
				outputs = cg2c_mkslicezeroed(sizeof(Slice), 2, gc);
				*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, ssa->outputs, &bb, gc) = outputs;
			}

			// TODO: don't redundantly append same values. Or add a
			// pass to eliminate redundant outputs.

			size_t phiIndex = cg2c_sllen(cg2c_slindex(Slice, outputs, 0));

			for (size_t i = 0; i < 2; i++) {
				Slice *hmm = &cg2c_slindex(Slice, outputs, i);
				assert(phiIndex == cg2c_sllen(*hmm));
				*hmm = cg2c_slappend(sizeof(Value*), *hmm, &preds[i], gc);
			}

			*changed = true;
			return buildPhi(b, type, bb, phiIndex);
		}

		case OpLoop: {
			// unimplemented

			break;
		}

		case OpCall: {
			break;
		}

		default:
			assert(0); // unreachable
			break;
		}
	}

	return cg2c_buildLoad(b, type, align, mem, ptr);
}

void
store(Compile *ctxt, Value *mem, Value *ptr, Value *val, LoadsToSSA *ssa)
{
	GC *gc = ctxt->gc;

	Value *bb = basicBlock(ssa, mem);

	Map *defs = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, ssa->vars, &bb);
	if (defs == NULL) {
		defs = cg2c_mkmap(cg2c_ptr_ptr_map, gc);
		*(Map**)cg2c_mapassign(cg2c_ptr_ptr_map, ssa->vars, &bb, gc) = defs;
	}

	*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, defs, &ptr, gc) = val;
}

typedef struct RewritePhis RewritePhis;
struct RewritePhis {
	Map *outputs;

	Map *ifsAndLoops;
};

static Value*
rewritePhis(Builder b, Map *re, Value *v, void *etc)
{
	RewritePhis *ssa = etc;
	GC *gc = b.ctxt->gc;

	switch (v->op) {
	case OpPhi: {
		Value *old = cg2c_valueArg(v, 0);
		uint64_t index = cg2c_valueAuxUint(cg2c_valueArg(v, 1));

		// Ensure the predecessor of this phi is rewritten
		cg2c_rewriteValue(b, re, rewritePhis, ssa, old);

		Value *pred = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, ssa->ifsAndLoops, &old);
		return cg2c_buildExtract(b, pred, cg2c_tupleNElem(old->type) + (size_t)index);
	}

	case OpRegion: {
		Region *r = v->aux;
		Region *rr = *(Region* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &r);
		assert(rr != NULL);
		return cg2c_buildRegionValue(b, rr);
	}

	case OpIf: {
		Value *arg = cg2c_rewriteValue(b, re, rewritePhis, ssa, cg2c_valueArg(v, 3));
		Value *cond = cg2c_rewriteValue(b, re, rewritePhis, ssa, cg2c_valueArg(v, 0));

		Slice outputs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, ssa->outputs, &v);
		if (cg2c_sllen(outputs) == 0) {
			// Rewrite regions and then proceed to rewrite this op
			// like a normal one.
			for (size_t i = 0; i < 2; i++)
				cg2c_rewriteRegion(b, re, rewritePhis, ssa, cg2c_valueArgRegion(v, 1+i));
			break;
		}

		Slice result = NULL_SLICE;
		{
			for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
				Type *e = cg2c_tupleElem(v->type, i);
				result = cg2c_slappend(sizeof(Type*), result, &e, gc);
			}
			Slice u = cg2c_slindex(Slice, outputs, 0);
			for (size_t i = 0; i < cg2c_sllen(u); i++) {
				Type *e = cg2c_slindex(Value*, u, i)->type;
				result = cg2c_slappend(sizeof(Type*), result, &e, gc);
			}
		}

		Type *rt = cg2c_func(b.ctxt, arg->type, cg2c_tuple(b.ctxt, result));

		Region *targets[2] = {0};
		for (size_t i = 0; i < 2; i++) {
			Region *r = cg2c_valueArgRegion(v, 1+i);
			Region *rr = cg2c_newRegion(b.ctxt, rt);

			*(Region**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &r, gc) = rr;

			Value *t = cg2c_rewriteValue(b, re, rewritePhis, ssa, cg2c_regionTerminator(r));

			Slice t2 = NULL_SLICE;
			for (size_t j = 0; j < cg2c_tupleNElem(v->type); j++) {
				Value *e = cg2c_buildExtract(b, t, j);
				t2 = cg2c_slappend(sizeof(Value*), t2, &e, gc);
			}
			Slice output_i = cg2c_slindex(Slice, outputs, i);
			for (size_t j = 0; j < cg2c_sllen(output_i); j++) {
				Value *e = cg2c_rewriteValue(b, re, rewritePhis, ssa, cg2c_slindex(Value*, output_i, j));
				t2 = cg2c_slappend(sizeof(Value*), t2, &e, gc);
			}

			cg2c_regionTerminate(rr, cg2c_buildMakeTuple(b, t2));

			targets[i] = rr;
		}

		Value *if_ = cg2c_buildIf(b, cond, targets[0], targets[1], arg);

		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, ssa->ifsAndLoops, &v, gc) = if_;

		Slice elems = NULL_SLICE;
		for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
			Value *e = cg2c_buildExtract(b, if_, i);
			elems = cg2c_slappend(sizeof(Value*), elems, &e, gc);
		}
		return cg2c_buildMakeTuple(b, elems);
	}

	case OpLoop: {
		// Unimplemented for now. Just rewrite the body as normal.

		cg2c_rewriteRegion(b, re, rewritePhis, ssa, cg2c_valueArgRegion(v, 1));

		break;
	}

	default:
		break;
	}

	return cg2c_rewriteValueDefault(b, re, rewritePhis, ssa, v);
}

bool
cg2c_loadsToSSA(Builder b, Region **r)
{
	LoadsToSSA *ssa = &(LoadsToSSA) {
		.vars = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc),
		.outputs = cg2c_mkmap(cg2c_ptr_slice_map, b.ctxt->gc),
	};
	*r = cg2c_rewrite(b, loadsToSSA, ssa, *r);

	if (ssa->changed) {
		RewritePhis *phis = &(RewritePhis) {
			.outputs = ssa->outputs,
			.ifsAndLoops = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc),
		};
		*r = cg2c_rewriteRegion(b, cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc), rewritePhis, phis, *r);

		// If any loads were rewritten, there's a good chance that we
		// got some dead stores. Eliminate them.
		//
		// TODO: live stores can be computed during rewritePhis
		LiveStores live = cg2c_liveStores(b.ctxt, *r);
		for (;;) {
			bool dsed = false;
			*r = cg2c_elimDeadStores(b, *r, live, &live, &dsed);
			if (!dsed)
				break;
		}
	}

	return ssa->changed;
}
