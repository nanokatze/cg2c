#pragma once

#include <stdint.h>

#include "all.h"

typedef struct ScratchLayout ScratchLayout;
struct ScratchLayout {
	Map      *offsets; // Value* → uint64_t scratch offsets for each var
	uint64_t size;     // total no. of scratch bytes
};

ScratchLayout cg2c_scratchAlloc(Compile *ctxt, Symbol *s);
