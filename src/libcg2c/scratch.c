#include "scratch.h"

// TODO: be smarter about scratch allocation, take Var lifetimes into account, etc

// TODO: scratchAlloc should be done on scheduled code

static uint64_t roundUp64(uint64_t x, uint64_t y) { return (x + y - 1) & ~y; }

static void
scratchAlloc(Compile *ctxt, Value *v, void *etc)
{
	ScratchLayout *es = etc;

	switch (v->op) {
	case OpVar: {
		uint64_t size = cg2c_valueAuxUint(cg2c_valueArg(v, 0));
		uint64_t align = cg2c_valueAuxUint(cg2c_valueArg(v, 1));

		if (!cg2c_mapcontains(cg2c_ptr_64_map, es->offsets, &v)) {
			uint64_t off = roundUp64(es->size, align);
			*(uint64_t*)cg2c_mapassign(cg2c_ptr_64_map, es->offsets, &v, ctxt->gc) = off;
			es->size = off + size;
		}

		break;
	}

	// TODO: recurse into ifs and loops

	default:
		break;
	}
}

ScratchLayout
cg2c_scratchAlloc(Compile *ctxt, Symbol *s)
{
	ScratchLayout es = {
		.offsets = cg2c_mkmap(cg2c_ptr_64_map, ctxt->gc),
		.size    = 0,
	};

	cg2c_foreach(ctxt, scratchAlloc, &es, s->def);

	return es;
}
