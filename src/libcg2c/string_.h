#pragma once

#include <stddef.h>

typedef struct GC GC;

typedef struct String String;
struct String {
	const char *data;
	size_t     len;
};

// cg2c_stringnocopy constructs a String from a C string s.
String cg2c_stringnocopy(const char *s);

// See strcmp(3).
int cg2c_strcmp(String s, String t);

// cg2c_strdata returns a pointer either to underlying contents of s or some
// other valid pointer, suitable for use with standard C library routines such
// as memcmp and printf.
const char* cg2c_strdata(String s);

// To get int length for use with C library routines such as printf, use
// cg2c_strlen2.
size_t cg2c_strlen(String s);

// rename to something like strilen?
int cg2c_strlen2(String s);

String cg2c_strslice(String s, size_t i, size_t j);