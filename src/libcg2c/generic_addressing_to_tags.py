from rulegen import Pattern, RewriteRule, RULES_C


print("""
#include "all.h"

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
    Value *args[] = {a0};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue2A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue3A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

// deprecated, should be replaced with a pass that will optimize pointers used
// by address space testing functions.
static Value*
cg2c_ptrchase(Value *v)
{
    for (;;) {
        assert(cg2c_typeIs(v->type, KindPtr));

        if (v->op == OpAddPtr)
            v = cg2c_valueArg(v, 0);
        else
            break;
    }

    return v;
}
""")

rules = [
    RewriteRule(
        pattern=Pattern(var="load", op="Load", args=[Pattern(var="rmem"), Pattern(var="p")]),
        replace="""
            Value *pBase = cg2c_ptrchase(p);

            Value *isGlobal = newValue1(b, OpPtrIsGlobal, cg2c_types[KindBool], pBase);

            Type *fun = cg2c_func(b.ctxt,
                cg2c_tuple1(b.ctxt, cg2c_types[KindMem]),
                cg2c_tuple1(b.ctxt, load->type));

            Region *bbGlobal = cg2c_newRegion(b.ctxt, fun);
            {
                Value *rmem = cg2c_buildExtract(b, cg2c_buildParam(b, bbGlobal), 0);
                Value *res = newValue2A(b, OpLoadGlobal, load->type, load->aux, rmem, p);
                cg2c_regionTerminate(bbGlobal, cg2c_buildMakeTuple1(b, res));
            }

            Region *bbScratch = cg2c_newRegion(b.ctxt, fun);
            {
                Value *rmem = cg2c_buildExtract(b, cg2c_buildParam(b, bbScratch), 0);
                Value *res = newValue2A(b, OpLoadScratch, load->type, load->aux, rmem, p);
                cg2c_regionTerminate(bbScratch, cg2c_buildMakeTuple1(b, res));
            }

            return cg2c_buildExtract(b, cg2c_buildIf(b, isGlobal, bbGlobal, bbScratch, cg2c_buildMakeTuple1(b, rmem)), 0);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="store", op="Store", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        replace="""
            Value *pBase = cg2c_ptrchase(p);

            Value *isGlobal = newValue1(b, OpPtrIsGlobal, cg2c_types[KindBool], pBase);

            Type *bb = cg2c_func(b.ctxt,
                cg2c_tuple1(b.ctxt, cg2c_types[KindMem]),
                cg2c_tuple1(b.ctxt, cg2c_types[KindMem]));

            Region *bbGlobal = cg2c_newRegion(b.ctxt, bb);
            {
                Value *mem = cg2c_buildExtract(b, cg2c_buildParam(b, bbGlobal), 0);
                mem = newValue3A(b, OpStoreGlobal, cg2c_types[KindMem], store->aux, mem, p, v);
                cg2c_regionTerminate(bbGlobal, cg2c_buildMakeTuple1(b, mem));
            }

            Region *bbScratch = cg2c_newRegion(b.ctxt, bb);
            {
                Value *mem = cg2c_buildExtract(b, cg2c_buildParam(b, bbScratch), 0);
                mem = newValue3A(b, OpStoreScratch, cg2c_types[KindMem], store->aux, mem, p, v);
                cg2c_regionTerminate(bbScratch, cg2c_buildMakeTuple1(b, mem));
            }

            return cg2c_buildExtract(b, cg2c_buildIf(b, isGlobal, bbGlobal, bbScratch, cg2c_buildMakeTuple1(b, mem)), 0);
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="rmw", op="AtomicRMW", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        replace="""
            // TODO: emit a ladder once we get shared working
            return cg2c_buildValue(b, OpAtomicRMWGlobal, rmw->type, rmw->aux, rmw->args);
        """,
    ),
]
print(RULES_C.render(label="cg2c_genericAddressingToTags", rules=rules))
