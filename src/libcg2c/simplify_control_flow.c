#include "all.h"

static Value* simplifyControlFlow(Builder b, Map *re, Value *v, void *changed);

static Region*
simplifyControlFlowRegion(Builder b, Map *re, Region *r, bool *changed)
{
	return cg2c_rewriteRegion(b, re, simplifyControlFlow, changed, r);
}

static Value*
simplifyControlFlowValue(Builder b, Map *re, Value *v, bool *changed)
{
	return cg2c_rewriteValue(b, re, simplifyControlFlow, changed, v);
}

Value*
simplifyControlFlow(Builder b, Map *re, Value *v, void *changed)
{
	GC *gc = b.ctxt->gc;

	switch (v->op) {
	case OpRegion: {
		Region *r = v->aux;
		Region *rr = *(Region* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &r);
		assert(rr != NULL);
		return cg2c_buildRegionValue(b, rr);
	}

	case OpIf: {
		Value *arg = simplifyControlFlowValue(b, re, cg2c_valueArg(v, 3), changed);
		Value *cond = simplifyControlFlowValue(b, re, cg2c_valueArg(v, 0), changed);

		if (cond->op == OpConst) {
			*(bool*)changed = true;

			Region *target = cg2c_valueArgRegion(v, cg2c_valueAuxUint(cond) != 0 ? 1 : 2);

			Value *param = cg2c_buildParam(b, target);
			*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &param, gc) = arg;

			return simplifyControlFlowValue(b, re, cg2c_regionTerminator(target), changed);
		}

		for (size_t i = 0; i < 2; i++)
			simplifyControlFlowRegion(b, re, cg2c_valueArgRegion(v, 1 + i), changed);

		break;
	}

	case OpLoop: {
		// Loops are tail-controlled, thus we need to rewrite the body
		// before we can check the condition.

		Value *arg = simplifyControlFlowValue(b, re, cg2c_valueArg(v, 0), changed);
		Region *body = simplifyControlFlowRegion(b, re, cg2c_valueArgRegion(v, 1), changed);

		Value *cond = cg2c_buildExtract(b, cg2c_regionTerminator(body), 0);
		if (cond->op == OpConst && cg2c_valueAuxUint(cond) == 0) {
			*(bool*)changed = true;

			Value *param = cg2c_buildParam(b, body);
			*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &param, gc) = arg;

			return cg2c_buildExtract(b, simplifyControlFlowValue(b, re, cg2c_regionTerminator(body), changed), 1);
		}

		break;
	}

	default:
		break;
	}

	return cg2c_rewriteValueDefault(b, re, simplifyControlFlow, changed, v);
}

Region*
cg2c_simplifyControlFlow(Builder b, Region *r, bool *changed)
{
	return simplifyControlFlowRegion(b, cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc), r, changed);
}
