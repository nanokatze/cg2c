#include "all.h"

typedef struct Scheduler Scheduler;
struct Scheduler {
	Compile *ctxt;

	// Maps a Value* to a Slice of Value* that depend on it.
	Map *uses;

	// Maps of Value* to the region (path to the region) it has been
	// scheduled into.
	Map *scheduled;
};

static void
analyze(Compile *ctxt, Value *v, void *_etc)
{
	Scheduler *sched = _etc;
	GC *gc = ctxt->gc;

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);

		Slice uses = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, sched->uses, &a);
		uses = cg2c_slappend(sizeof(Value*), uses, &v, gc);
		*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, sched->uses, &a, gc) = uses;
	}
}

// TODO: set this in opInfo, though I think a better idea is to introduce a type
// along the lines of "MemRef"
static bool
borrowsMem(Value *v)
{
	switch (v->op) {
	case OpLoad:
	case OpTex:
	case OpImageLoad:
	case OpLoadGlobal:
	case OpLoadShared:
	case OpLoadScratch:
		return true;

	default:
		return false;
	}
}

static void
into(Scheduler *sched, Slice path, Value *v)
{
	GC *gc = sched->ctxt->gc;

	Region *f = cg2c_slindex(Region*, path, cg2c_sllen(path)-1);

	if (cg2c_mapcontains(cg2c_ptr_slice_map, sched->scheduled, &v)) {
		// TODO: ICE here
		assert(0);
	}
	*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, sched->scheduled, &v, gc) = path;

	f->values = cg2c_slappend(sizeof(Value*), f->values, &v, gc);
}

static Slice
commonPrefix(Slice a, Slice b)
{
	size_t i = 0;
	for (; i < cg2c_sllen(a) && i < cg2c_sllen(b); i++) {
		if (cg2c_slindex(Region*, a, i) != cg2c_slindex(Region*, b, i))
			break;
	}
	return cg2c_slice(sizeof(Region*), a, 0, i);
}

static void schedule2(Scheduler *sched, Slice p, Region *f);
static void schedule(Scheduler *sched, Slice p, Value *v);

void
schedule2(Scheduler *sched, Slice p, Region *f)
{
	GC *gc = sched->ctxt->gc;

	schedule(sched,
		cg2c_slappend(sizeof(Region*), cg2c_slclone(sizeof(Region*), p, gc), &f, gc),
		cg2c_regionTerminator(f));
}

void
schedule(Scheduler *sched, Slice p, Value *v)
{
	GC *gc = sched->ctxt->gc;

	if (cg2c_mapcontains(cg2c_ptr_slice_map, sched->scheduled, &v))
		return;

	// Check whether all uses of v have been scheduled and compute path to
	// their lowest common ancestor.

	// BUG: ensure destructive uses of mem are scheduled after
	// non-destructive ones.

	Slice uses = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, sched->uses, &v);
	for (size_t i = 0; i < cg2c_sllen(uses); i++) {
		Value *use = cg2c_slindex(Value*, uses, i);
		Slice puse = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, sched->scheduled, &use);
		if (cg2c_sllen(puse) == 0)
			return;
		p = commonPrefix(p, puse);
	}

	// Schedule v into the lca of its uses

	into(sched, p, v);

	// Try to schedule v's arguments

	for (size_t i = 0; i < cg2c_valueNArg(v); i++)
		schedule(sched, p, cg2c_valueArg(v, i));

	// If this was a control instruction, schedule instructions in the
	// regions

	switch (v->op) {
	case OpIf: {
		schedule2(sched, p, cg2c_valueArgRegion(v, 1));
		schedule2(sched, p, cg2c_valueArgRegion(v, 2));
		break;
	}

	case OpLoop: {
		schedule2(sched, p, cg2c_valueArgRegion(v, 1));
		break;
	}

	default:
		break;
	}
}

static void
reverse(Region *f)
{
	size_t n = cg2c_sllen(f->values);

	for (size_t i = 0; i < n / 2; i++) {
		Value **p = &cg2c_slindex(Value*, f->values, i);
		Value **q = &cg2c_slindex(Value*, f->values, n-1-i);
		Value *tmp = *p;
		*p = *q;
		*q = tmp;
	}

	for (size_t i = 0; i < n; i++) {
		Value *v = cg2c_slindex(Value*, f->values, i);

		switch (v->op) {
		case OpIf: {
			reverse(cg2c_valueArgRegion(v, 1));
			reverse(cg2c_valueArgRegion(v, 2));
			break;
		}

		case OpLoop: {
			reverse(cg2c_valueArgRegion(v, 1));
			break;
		}

		default:
			break;
		}
	}
}

void
cg2c_schedule(Compile *ctxt, Region *f)
{
	// Schedule instructions as soon as all of their inputs are available
	// and other constraints are met

	Scheduler *etc = &(Scheduler) {
		.ctxt = ctxt,
		.uses = cg2c_mkmap(cg2c_ptr_slice_map, ctxt->gc),
		.scheduled = cg2c_mkmap(cg2c_ptr_slice_map, ctxt->gc),
	};

	cg2c_foreach(ctxt, analyze, etc, f);

	schedule2(etc, NULL_SLICE, f);

	// Scheduler placed instructions in the reverse order. Reverse them back

	reverse(f);

	cg2c_validateSchedule(f, ctxt->gc);
}

static bool
wasObserved(Slice observed, Value *v)
{
	for (size_t i = cg2c_sllen(observed); i-- > 0;) {
		Map *obsm = cg2c_slindex(Map*, observed, i);

		if (cg2c_mapcontains(cg2c_ptr_set, obsm, &v))
			return true;
	}

	return false;
}

// TODO: maintain a second hash table of all values that have been scheduled so
// that when we say something is unscheduled, but it's actually scheduled
// incorrectly, we are more informative.

static void
validateSchedule(Region *f, Slice observed, GC *gc)
{
	observed = cg2c_slappend(sizeof(Map*), observed, &(Map*) {cg2c_mkmap(cg2c_ptr_set, gc)}, gc);

	bool scheduledTerminator = false;

	for (size_t i = 0; i < cg2c_sllen(f->values); i++) {
		Value *v = cg2c_slindex(Value*, f->values, i);

		for (size_t j = 0; j < cg2c_valueNArg(v); j++) {
			Value *a = cg2c_valueArg(v, j);

			if (!wasObserved(observed, a)) {
				// TODO: cg2c_fatalf an internal compiler error
				fprintf(stderr, "internal compiler error: v%"PRIi64" uses v%"PRIi64" that is unscheduled\n", v->id, a->id);
			}
		}

		switch (v->op) {
		case OpParam: {
			Region *g = cg2c_valueArgRegion(v, 0);
			if (g != f) {
				fprintf(stderr, "internal compiler error: v%"PRIi64" Param L%"PRIi64" is incorrectly scheduled in L%"PRIi64"\n", v->id, g->id, f->id);
			}

			break;
		}

		case OpIf: {
			validateSchedule(cg2c_valueArgRegion(v, 1), observed, gc);
			validateSchedule(cg2c_valueArgRegion(v, 2), observed, gc);
			break;
		}

		case OpLoop: {
			validateSchedule(cg2c_valueArgRegion(v, 1), observed, gc);
			break;
		}

		default:
			break;
		}

		if (wasObserved(observed, v)) {
			fprintf(stderr, "internal compiler error: v%"PRIi64" scheduled multiple times\n", v->id);
		}
		cg2c_mapassign(cg2c_ptr_set, cg2c_slindex(Map*, observed, cg2c_sllen(observed)-1), &v, gc);

		if (v == f->terminator)
			scheduledTerminator = true;
	}

	if (!scheduledTerminator) {
		fprintf(stderr, "internal compiler error: %p terminator not scheduled\n", f);
	}
}

void cg2c_validateSchedule(Region *f, GC *gc) { validateSchedule(f, NULL_SLICE, gc); }
