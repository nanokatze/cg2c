#include "all.h"

#include "map_type.h"

struct Rewriter {
	Slice rules;

	// TODO: acceleration structure
};

typedef struct CSEKey CSEKey;
struct CSEKey {
	Op    op;
	Type  *type;
	void  *aux;
	Slice args; // of Value*
};

static const MapType* cseKey_ptr_map;

static void typecheck(Compile *ctxt, Value *v);

static bool patternSatisfied(Pattern *pat, Value *v);

Builder
cg2c_builderWithPos(Builder b, Pos *pos)
{
	b.pos = pos;
	return b;
}

Rewriter*
cg2c_newRewriter(Slice rules, GC *gc)
{
	Rewriter *rewriter = cg2c_malloc(sizeof *rewriter, gc);
	rewriter->rules = rules;
	return rewriter;
}

CSE* cg2c_newCSE(GC *gc) { return (CSE*)cg2c_mkmap(cseKey_ptr_map, gc); }

Value*
cg2c_buildValue(Builder b, Op op, Type *type, void *aux, Slice args)
{
	Compile *ctxt = b.ctxt;
	Rewriter *rewriter = b.rewriter;
	Map *cse = (Map*)b.cse;
	GC *gc = ctxt->gc;

	Value *tmp = &(Value) {
		.op   = op,
		.type = type,
		.aux  = aux,
		.args = args,
		// .pos  = b.pos,
	};

	// TODO: have an option to turn this off
	typecheck(ctxt, tmp);

	// Apply rewrite rules

	for (size_t i = 0; i < cg2c_sllen(rewriter->rules); i++) {
		RewriteRule rule = cg2c_slindex(RewriteRule, rewriter->rules, i);
		if (patternSatisfied(rule.pattern, tmp)) {
			Value *v = rule.replace(b, tmp);
			assert(v != NULL);
			assert(v->type == type);
			if (v != tmp)
				return v;
		}
	}

	Value *v = *(Value* const*)cg2c_mapaccess1(cseKey_ptr_map, cse, &(CSEKey) {op, type, aux, args});
	if (v != NULL)
		return v;

	v = cg2c_malloc(sizeof *v, gc);
	v->op = op;
	v->type = type;
	if (!cg2c_opInfo[op].smallaux) {
		v->aux = aux;
	} else {
		v->aux = &v->auxstorage;
		*(uint64_t*)v->aux = *(uint64_t*)aux;
	}
	// Don't needlessly allocate empty slices.
	if (cg2c_sllen(args) > 0)
		v->args = cg2c_slclone(sizeof(Value*), args, gc);
	v->pos = b.pos;
	v->id = ++b.ctxt->idctr;
	// if (true)
	//	v->trace = b.trace;

	*(Value**)cg2c_mapassign(cseKey_ptr_map, cse, &(CSEKey) {op, type, v->aux, v->args}, gc) = v;
	return v;
}

// TODO: pick up type constraints from ops.py
//
// TODO: return bool and print diagnostic message somewhere instead of blowing
// up outright
void
typecheck(Compile *ctxt, Value *v)
{
	switch (v->op) {
	case OpAdd:
	case OpSub: {
		assert(cg2c_sllen(v->args) == 2);
		Value *x = cg2c_valueArg(v, 0);
		Value *y = cg2c_valueArg(v, 1);
		assert(x->type == y->type);
		assert(v->type == x->type);
		break;
	}

	case OpParam: {
		assert(cg2c_sllen(v->args) == 1);
		Region *r = cg2c_valueArgRegion(v, 0);
		assert(v->type == cg2c_funcParam(r->type));
		break;
	}

	case OpRegion: {
		assert(cg2c_sllen(v->args) == 0);
		Region *r = v->aux;
		assert(v->type == r->type);
		break;
	}

	case OpPtrToInt: {
		assert(cg2c_sllen(v->args) == 1);
		Value *x = cg2c_valueArg(v, 0);
		assert(cg2c_typeIs(x->type, KindPtr));
		assert(v->type == cg2c_intptr(ctxt));
		break;
	}

	case OpIntToPtr: {
		assert(cg2c_sllen(v->args) == 1);
		Value *x = cg2c_valueArg(v, 0);
		assert(x->type == cg2c_intptr(ctxt));
		assert(cg2c_typeIs(v->type, KindPtr));
		break;
	}

	case OpAddPtr: {
		assert(cg2c_sllen(v->args) == 2);
		Value *base = cg2c_valueArg(v, 0);
		Value *off = cg2c_valueArg(v, 1);
		assert(cg2c_typeIs(base->type, KindPtr));
		assert(off->type == cg2c_intptr(ctxt));
		assert(cg2c_typeIs(v->type, KindPtr));
		break;
	}

	case OpLoad: {
		assert(cg2c_sllen(v->args) == 2);
		Value *mem = cg2c_valueArg(v, 0);
		assert(cg2c_typeIs(mem->type, KindMem));
		break;
	}

	case OpStore: {
		assert(cg2c_sllen(v->args) == 3);
		Value *mem = cg2c_valueArg(v, 0);
		assert(cg2c_typeIs(mem->type, KindMem));
		break;
	}

	case OpIf: {
		assert(cg2c_sllen(v->args) == 4);
		Value *cond = cg2c_valueArg(v, 0);
		Region *then = cg2c_valueArgRegion(v, 1);
		Region *else_ = cg2c_valueArgRegion(v, 2);
		Value *arg = cg2c_valueArg(v, 3);
		assert(then->type == else_->type);
		assert(cg2c_funcParam(then->type) == arg->type);
		assert(v->type == cg2c_funcResult(then->type));
		break;
	}

	case OpLoop: {
		assert(cg2c_sllen(v->args) == 2);
		Value *arg = cg2c_valueArg(v, 0);
		Region *body = cg2c_valueArgRegion(v, 1);
		assert(cg2c_funcParam(body->type) == arg->type);
		Type *result = cg2c_funcResult(body->type);
		assert(cg2c_tupleNElem(result) == 2);
		assert(cg2c_typeIs(cg2c_tupleElem(result, 0), KindBool));
		assert(v->type == cg2c_tupleElem(result, 1));
		break;
	}

	case OpCall: {
		assert(cg2c_sllen(v->args) == 2);
		Value *target = cg2c_valueArg(v, 0);
		Value *arg = cg2c_valueArg(v, 1);
		assert(cg2c_funcParam(target->type) == arg->type);
		assert(v->type == cg2c_funcResult(target->type));
		break;
	}

	default:
		break;
	}
}

size_t cg2c_valueNArg(Value *v) { return cg2c_sllen(v->args); }

Value* cg2c_valueArg(Value *v, size_t i) { return cg2c_slindex(Value*, v->args, i); }

bool
patternSatisfied(Pattern *pat, Value *v)
{
	bool ok = true;

	// Any value trivially satisfies an empty pattern.
	if (pat != NULL) {
		ok = ok && v->op == pat->op;

		// If this pattern's parameters list does not end in ellipsis,
		// the number of arguments has to match.
		if (!pat->ddd)
			ok = ok && cg2c_valueNArg(v) == pat->narg;
		else
			ok = ok && cg2c_valueNArg(v) >= pat->narg;

		for (size_t i = 0; i < pat->narg; i++)
			ok = ok && patternSatisfied(pat->args[i], cg2c_valueArg(v, i));
	}
	return ok;
}

static bool
cseKeyEqual(const void *p, const void *q)
{
	const CSEKey *v = p;
	const CSEKey *u = q;

	if (v->op != u->op)
		return false;

	const OpInfo *opInfo = &cg2c_opInfo[v->op];

	if (v->type != u->type)
		return false;

	if (!opInfo->smallaux) {
		if (v->aux != u->aux)
			return false;
	} else {
		if (*(uint64_t*)v->aux != *(uint64_t*)u->aux)
			return false;
	}

	if (cg2c_sllen(v->args) != cg2c_sllen(u->args))
		return false;
	size_t i = 0;
	if (opInfo->commutative) {
		Value *v_y = cg2c_slindex(Value*, v->args, 1);
		Value *v_x = cg2c_slindex(Value*, v->args, 0);
		Value *u_y = cg2c_slindex(Value*, u->args, 1);
		Value *u_x = cg2c_slindex(Value*, u->args, 0);
		if (!((v_x == u_x && v_y == u_y) || (v_x == u_y && v_y == u_x)))
			return false;
		i = 2;
	}
	for (; i < cg2c_sllen(v->args); i++) {
		if (cg2c_slindex(Value*, v->args, i) != cg2c_slindex(Value*, u->args, i))
			return false;
	}

	return true;
}

static size_t
cseKeyHash(const void *p, size_t h)
{
	const CSEKey *v = p;

	h = cg2c_memhash(&v->op, sizeof v->op, h);

	const OpInfo *opInfo = &cg2c_opInfo[v->op];

	h = cg2c_memhash(&v->type, sizeof v->type, h);

	if (!opInfo->smallaux) {
		h = cg2c_memhash(&v->aux, sizeof v->aux, h);
	} else {
		uint64_t aux = *(uint64_t*)v->aux;
		h = cg2c_memhash(&aux, sizeof aux, h);
	}

	Slice args = v->args;
	if (opInfo->commutative) {
		Value *x = cg2c_slindex(Value*, v->args, 0);
		Value *y = cg2c_slindex(Value*, v->args, 1);
		h = cg2c_memhash(&x, sizeof x, h) ^ cg2c_memhash(&y, sizeof y, h);
		args = cg2c_slice(sizeof(Value*), args, 2, cg2c_sllen(args));
	}
	h = cg2c_memhash(args.data, args.len*sizeof(Value*), h);

	return h;
}

static const MapType* cseKey_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = cseKeyEqual,
	.hasher   = cseKeyHash,
	.keysize  = sizeof(CSEKey),
	.elemsize = sizeof(void*),
};
