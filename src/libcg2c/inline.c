#include "all.h"

typedef struct Inline Inline;
struct Inline {
	Region  *f;
	Value *a;
};

static Value*
inliner(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	Inline *inl = etc;

	switch (op) {
	case OpParam:
		if (cg2c_valueArgRegion(u, 0) == inl->f)
			return inl->a;
		break;

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

Value*
cg2c_inline(Builder b, Region *r, Value *a)
{
	assert(cg2c_funcParam(r->type) == a->type);
	Region *tmp = cg2c_rewrite(b, inliner, &(Inline) {r, a}, r);
	return cg2c_regionTerminator(tmp);
}
