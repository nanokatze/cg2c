#include "slice.h"

#include <assert.h>
#include <string.h>

#include "gc.h"

static void
validateElemsize(size_t elemsize, Slice s)
{
	assert(s.cap == 0 || s._elemsize == elemsize);
}

static Slice growslice(size_t, Slice, size_t, GC *gc);

Slice
cg2c_mkslice(size_t elemsize, const void *elems, size_t n, GC *gc)
{
	return (Slice) {
		.data = cg2c_memdup(elems, n*elemsize, gc),
		.len = n,
		.cap = n,
		._elemsize = elemsize,
	};
}

Slice
cg2c_mkslicezeroed(size_t elemsize, size_t n, GC *gc)
{
	return (Slice) {
		.data = cg2c_malloc(n*elemsize, gc),
		.len = n,
		.cap = n,
		._elemsize = elemsize,
	};
}

Slice
cg2c_mkslicenocopy(size_t elemsize, void *elems, size_t n)
{
	return (Slice) {
		.data = elems,
		.len = n,
		.cap = n,
		._elemsize = elemsize,
	};
}

void*
(cg2c_slindex)(size_t elemsize, Slice s, size_t i)
{
	validateElemsize(elemsize, s);

	if (i >= s.len)
		assert(!"slice index out of range");

	return (char*)s.data + i*elemsize;
}

Slice
cg2c_slice(size_t elemsize, Slice s, size_t i, size_t j)
{
	validateElemsize(elemsize, s);

	if (i > j || j > s.cap)
		assert(!"slice bounds out of range");

	return (Slice) {
		.data      = i > 0 ? (char*)s.data+i*elemsize : s.data,
		.len       = j - i,
		.cap       = s.cap - i,
		._elemsize = elemsize,
	};
}

Slice
cg2c_slappend(size_t elemsize, Slice s, const void *e, GC *gc)
{
	validateElemsize(elemsize, s);

	size_t i = cg2c_sllen(s);
	size_t n = i + 1;
	if (cg2c_slcap(s) < n)
		s = growslice(elemsize, s, n, gc);
	s = cg2c_slice(elemsize, s, 0, n);
	memmove((cg2c_slindex)(elemsize, s, i), e, elemsize);
	return s;
}

Slice
cg2c_slcat(size_t elemsize, Slice s1, Slice s2, GC *gc)
{
	validateElemsize(elemsize, s1);
	validateElemsize(elemsize, s2);

	if (cg2c_sllen(s2) == 0)
		return s1;
	size_t i = cg2c_sllen(s1);
	size_t n = cg2c_sllen(s1) + cg2c_sllen(s2); // TODO: overflow check?
	if (cg2c_slcap(s1) < n)
		s1 = growslice(elemsize, s1, n, gc);
	s1 = cg2c_slice(elemsize, s1, 0, n);
	memmove((cg2c_slindex)(elemsize, s1, i), (cg2c_slindex)(elemsize, s2, 0), cg2c_sllen(s2)*elemsize);
	return s1;
}

Slice cg2c_slclone(size_t elemsize, Slice s, GC *gc) { return cg2c_slcat(elemsize, NULL_SLICE, s, gc); }

/*
Slice
cg2c_slprintf(Slice s, GC *gc, const char *format, ...)
{
	size_t i = cg2c_sllen(s);

	for (size_t k = 0;; k++) {
		assert(k < 2);

		char *p = &cg2c_slindex(char, cg2c_slice(sizeof(char), s, 0, cg2c_slcap(s)), cg2c_sllen(s));

		va_list ap;
		va_start(ap, format);
		int n = vsnprintf(p, cg2c_slcap(s)-cg2c_sllen(s), format, ap);
		va_end(ap);

		assert(n >= 0);
		size_t needcap = i + (size_t)n;

		if (needcap < cg2c_slcap(s))
			return cg2c_slice(sizeof(char), s, 0, needcap);

		s = growslice(sizeof(char), s, needcap, gc);
	}
}
*/

Slice
growslice(size_t elemsize, Slice old, size_t cap, GC *gc)
{
	// TODO: add overflow checks

	size_t newcap = 1;
	while (0 < newcap && newcap < cap)
		newcap *= 2;

	void *p = cg2c_malloc(newcap*elemsize, gc);
	if (old.len > 0)
		memmove(p, old.data, old.len*elemsize);

	return (Slice) {
		.data      = p,
		.len       = old.len,
		.cap       = newcap,
		._elemsize = elemsize,
	};
}
