#include "string_.h"

#include <assert.h>
#include <limits.h>
#include <string.h>

String
cg2c_stringnocopy(const char *s)
{
	if (s == NULL)
		return (String) {0};
	return (String) {s, strlen(s)};
}

int
cg2c_strcmp(String a, String b)
{
	int r = 0;

	size_t n = a.len < b.len ? a.len : b.len;
	// memcmp requires the pointers to be non-NULL even when n is zero.
	if (n > 0)
		r = memcmp(a.data, b.data, n);
	if (r == 0) {
		if (a.len < b.len)
			r = -1;
		else if (a.len > b.len)
			r = 1;
	}
	return r;
}

const char* cg2c_strdata(String s) { return s.len > 0 ? s.data : ""; }

size_t cg2c_strlen(String s) { return s.len; }

int
cg2c_strlen2(String s)
{
	size_t n = cg2c_strlen(s);
	assert(n <= INT_MAX);
	return (int)n;
}

String
cg2c_strslice(String s, size_t i, size_t j)
{
	if (i > j || j > s.len)
		assert(!"slice bounds out of range");
	return (String) {i > 0 ? s.data+i : s.data, j - i};
}
