#pragma once

#include <stddef.h>

typedef struct GC GC;

typedef struct Slice Slice;
struct Slice {
	void   *data;
	size_t len;
	size_t cap;

	size_t _elemsize; // for diagnostics
};

#define NULL_SLICE (Slice) {0}

Slice cg2c_mkslice(size_t elemsize, const void *elems, size_t n, GC *gc);
Slice cg2c_mkslicezeroed(size_t elemsize, size_t n, GC *gc);
Slice cg2c_mkslicenocopy(size_t elemsize, void *elems, size_t n);

static size_t cg2c_sllen(Slice s) { return s.len; }
static size_t cg2c_slcap(Slice s) { return s.cap; }

void* cg2c_slindex(size_t elemsize, Slice s, size_t i);

// TODO: use typeof once we switch to C23
#define cg2c_slindex(elemt, s, i) (*(elemt*)cg2c_slindex(sizeof(elemt), s, i))

Slice cg2c_slice(size_t elemsize, Slice s, size_t i, size_t j);
Slice cg2c_slappend(size_t elemsize, Slice s, const void *e, GC *gc);
Slice cg2c_slcat(size_t elemsize, Slice s1, Slice s2, GC *gc);
Slice cg2c_slclone(size_t elemsize, Slice s, GC *gc);
