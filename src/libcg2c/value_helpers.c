#include "all.h"

// TODO: remove assertions about types once cg2c_buildValue gets its own
// type-checking.

Value*
cg2c_buildArith2(Builder b, Op op, Value *x, Value *y)
{
	// TODO: assert op
	Value *args[] = {x, y};
	return cg2c_buildValue(b, op, x->type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildShift(Builder b, Op op, Value *x, Value *y)
{
	// TODO: replace this assert with assert for "builder class" from opInfo
	assert(op == OpLsh || op == OpRsh || op == OpURsh);
	Value *args[] = {x, y};
	return cg2c_buildValue(b, op, x->type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

// or buildShiftByConst
Value*
cg2c_buildShiftConst(Builder b, Op op, Value *x, int k)
{
	Value *y = cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)k);
	return cg2c_buildShift(b, op, x, y);
}

Value*
cg2c_buildCmp(Builder b, Op op, Value *x, Value *y)
{
	Value *args[] = {x, y};
	return cg2c_buildValue(b, op, cg2c_types[KindBool], NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildParam(Builder b, Region *r)
{
	Value *args[] = {cg2c_buildRegionValue(b, r)};
	return cg2c_buildValue(b, OpParam, cg2c_funcParam(r->type), NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static uint64_t limits[KindLast] = {
	[KindBool]  = 1,
	[KindInt8]  = 0xff,
	[KindInt16] = 0xffff,
	[KindInt32] = 0xffffffff,
	[KindInt64] = 0xffffffffffffffff,
};

Value*
cg2c_buildConst(Builder b, Type *type, uint64_t val)
{
	// BUG: doesn't belong here
	uint64_t limit = limits[cg2c_typeKind(type)];
	assert(limit > 0 && val <= limit);

	b = cg2c_builderWithPos(b, NULL);
	return cg2c_buildValue(b, OpConst, type, &val, NULL_SLICE);
}

Value*
cg2c_buildRegionValue(Builder b, Region *r)
{
	b = cg2c_builderWithPos(b, NULL);
	return cg2c_buildValue(b, OpRegion, r->type, r, NULL_SLICE);
}

Value*
cg2c_buildSymbol(Builder b, Symbol *s)
{
	b = cg2c_builderWithPos(b, NULL);
	return cg2c_buildValue(b, OpSymbol, s->type, s, NULL_SLICE);
}

Value*
cg2c_buildPtrToInt(Builder b, Value *v)
{
	Value *args[] = {v};
	return cg2c_buildValue(b, OpPtrToInt, cg2c_intptr(b.ctxt), NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildIntToPtr(Builder b, Value *v)
{
	Value *args[] = {v};
	return cg2c_buildValue(b, OpIntToPtr, cg2c_types[KindPtr], NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildMakeTuple(Builder b, Slice elems)
{
	Slice elemts = NULL_SLICE;
	elemts.data = (Type*[5]) {0};
	elemts.cap = 5;
	elemts._elemsize = sizeof(Type*);
	for (size_t i = 0; i < cg2c_sllen(elems); i++) {
		Value *e = cg2c_slindex(Value*, elems, i);
		elemts = cg2c_slappend(sizeof(Type*), elemts, &e->type, b.ctxt->gc);
	}
	Type *type = cg2c_tuple(b.ctxt, elemts);
	return cg2c_buildValue(b, OpMakeTuple, type, NULL, elems);
}

Value*
cg2c_buildMakeTuple1(Builder b, Value *e)
{
	Value *args[] = {e};
	return cg2c_buildMakeTuple(b, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildMakeTuple2(Builder b, Value *e0, Value *e1)
{
	Value *args[] = {e0, e1};
	return cg2c_buildMakeTuple(b, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildMakeTuple4(Builder b, Value *e0, Value *e1, Value *e2, Value *e3)
{
	Value *args[] = {e0, e1, e2, e3};
	return cg2c_buildMakeTuple(b, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildExtract(Builder b, Value *tuple, size_t i)
{
	b = cg2c_builderWithPos(b, tuple->pos);

	Type *type = cg2c_tupleElem(tuple->type, i);
	uint64_t aux = (uint64_t)i;
	Value *args[] = {tuple};
	return cg2c_buildValue(b, OpExtract, type, &aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildAddPtr(Builder b, Value *base, Value *off)
{
	Value *args[] = {base, off};
	return cg2c_buildValue(b, OpAddPtr, base->type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildAddPtrConst(Builder b, Value *base, int64_t off)
{
	return cg2c_buildAddPtr(b, base, cg2c_buildConst(b, cg2c_intptr(b.ctxt), (uint64_t)off));
}

Value*
cg2c_buildVar(Builder b, int64_t size, int64_t align)
{
	int64_t id = ++b.ctxt->varId;
	Value *args[] = {
		cg2c_buildConst(b, cg2c_intptr(b.ctxt), (uint64_t)size),
		cg2c_buildConst(b, cg2c_intptr(b.ctxt), (uint64_t)align),
	};
	return cg2c_buildValue(b, OpVar, cg2c_types[KindPtr], &id, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildLoad(Builder b, Type *type, int64_t align, Value *mem, Value *ptr)
{
	Value *args[] = {mem, ptr};
	return cg2c_buildValue(b, OpLoad, type, &align, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildStore(Builder b, int64_t align, Value *mem, Value *ptr, Value *val)
{
	Value *args[] = {mem, ptr, val};
	return cg2c_buildValue(b, OpStore, cg2c_types[KindMem], &align, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildIf(Builder b, Value *cond, Region *then, Region *else_, Value *arg)
{
	Type *type = cg2c_funcResult(then->type);
	Value *args[] = {cond, cg2c_buildRegionValue(b, then), cg2c_buildRegionValue(b, else_), arg};
	return cg2c_buildValue(b, OpIf, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildLoop(Builder b, Value *arg, Region *body)
{
	Type *type = cg2c_tupleElem(cg2c_funcResult(body->type), 1);
	Value *args[] = {arg, cg2c_buildRegionValue(b, body)};
	return cg2c_buildValue(b, OpLoop, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

Value*
cg2c_buildCall(Builder b, Value *target, Value *arg)
{
	Type *type = cg2c_funcResult(target->type);
	Value *args[] = {target, arg};
	return cg2c_buildValue(b, OpCall, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static uint64_t intbits[KindLast] = {
	[KindInt8]  = 0xff,
	[KindInt16] = 0xffff,
	[KindInt32] = 0xffffffff,
	[KindInt64] = 0xffffffffffffffff,
};

int64_t
cg2c_valueAuxInt(Value *v)
{
	assert(v->op == OpConst);
	uint64_t x = *(uint64_t*)v->aux;
	uint64_t y = ~intbits[cg2c_typeKind(v->type)];
	// TODO: we shouldn't sign-extend bool values
	if ((x & (y >> 1)) != 0)
		x |= y;
	return (int64_t)x;
}

uint64_t
cg2c_valueAuxUint(Value *v)
{
	assert(v->op == OpConst);
	return *(uint64_t*)v->aux;
}

Region*
cg2c_valueAuxRegion(Value *v)
{
	assert(v->op == OpRegion);
	return v->aux;
}

Symbol*
cg2c_valueAuxSymbol(Value *v)
{
	assert(v->op == OpSymbol);
	return v->aux;
}

Region* cg2c_valueArgRegion(Value *v, size_t i) { return cg2c_valueAuxRegion(cg2c_valueArg(v, i)); }
