# Common rules that can't be gone wrong with.
#
# Rules that make assumptions about cost of instructions should not be added
# here. For example, an optimization that turns integer division by a constant
# into a right shift and a bunch of other ops does not belong to common rules.

from rulegen import Pattern, RewriteRule, RULES_C


print("""#include "all.h"

#include <string.h>

static uint64_t intMask[KindLast] = {
    [KindBool]  = 0x1,
    [KindInt8]  = 0xff,
    [KindInt16] = 0xffff,
    [KindInt32] = 0xffffffff,
    [KindInt64] = 0xffffffffffffffff,
};

// TODO: rename to make it clear it's about Const?
static bool cg2c_valueAuxIntZero(Value *v) { return cg2c_valueAuxUint(v) == 0; }

static size_t
cg2c_valueAuxSize(Value *v)
{
    assert(v->op == OpExtract);
    return (size_t)*(uint64_t*)v->aux;
}

static int64_t
cg2c_valueAuxAlign(Value *v)
{
    assert(v->op == OpLoad || v->op == OpStore);
    return (int64_t)*(uint64_t*)v->aux;
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue4A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1, Value *a2, Value *a3)
{
    Value *args[] = {a0, a1, a2, a3};
    return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newConst(Builder b, Type *type, uint64_t aux)
{
    return cg2c_buildConst(b, type, aux&intMask[cg2c_typeKind(type)]);
}

static Value*
buildBoolToInt(Builder b, Type *type, Value *x)
{
    assert(cg2c_typeIs(x->type, KindBool));
    Value *zero = cg2c_buildConst(b, type, 0);
    Value *one = cg2c_buildConst(b, type, 1);
    return newValue3(b, OpCondSelect, type, x, one, zero);
}

static uint32_t
floatbits(float x)
{
    uint32_t y;
    memmove(&y, &x, sizeof y);
    return y;
}

static double
doublefrombits(uint64_t x)
{
    double y;
    memmove(&y, &x, sizeof y);
    return y;
}

static float
floatfrombits(uint32_t x)
{
    float y;
    memmove(&y, &x, sizeof y);
    return y;
}

static double
cg2c_valueAuxFloat(Value *v)
{
    uint64_t bits = cg2c_valueAuxUint(v);

    switch (cg2c_typeKind(v->type)) {
    case KindInt32:
        return (double)floatfrombits((uint32_t)bits);

    case KindInt64:
        return doublefrombits(bits);

    default:
        assert(0);
        return 0;
    }
}

static bool
isMakeTupleOfExtractsUnpermuted(Value *v, Value *u)
{
    assert(v->op == OpMakeTuple);
    if (v->type != u->type)
        return false;
    for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
        Value *x = cg2c_valueArg(v, i);
        if (x->op != OpExtract || cg2c_valueArg(x, 0) != u || cg2c_valueAuxSize(x) != i)
            return false;
    }
    return true;
}
""")

rules = [
    # Constant folding

    RewriteRule(
        name="Add of constants",
        pattern=Pattern(var="v", op="Add", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)+cg2c_valueAuxUint(y));",
    ),
    RewriteRule(
        name="Sub of constants",
        pattern=Pattern(var="v", op="Sub", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)-cg2c_valueAuxUint(y));",
    ),

    RewriteRule(
        name="Mul of constants",
        pattern=Pattern(var="v", op="Mul", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)*cg2c_valueAuxUint(y));",
    ),

    # TODO: for fp rules, we might not want to fold expressions that produce
    # NaN, as that will destroy extra NaN bits, which could e.g. embed pc in
    # theory
    RewriteRule(
        name="FDiv of constants",
        pattern=Pattern(var="v", op="FDiv", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="""
            switch (cg2c_typeKind(v->type)) {
            case KindInt32: {
                float x_ = floatfrombits((uint32_t)cg2c_valueAuxUint(x));
                float y_ = floatfrombits((uint32_t)cg2c_valueAuxUint(y));
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits(x_ / y_));
            }

            default:
                assert(0); // not implemented
                return NULL;
            }
        """,
    ),

    RewriteRule(
        name="Equal of constants",
        pattern=Pattern(op="Equal", args=[Pattern(var="k0", op="Const"), Pattern(var="k1", op="Const")]),
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], k0 == k1);",
    ),
    RewriteRule(
        name="NotEqual of constants",
        pattern=Pattern(op="NotEqual", args=[Pattern(var="k0", op="Const"), Pattern(var="k1", op="Const")]),
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], k0 != k1);",
    ),

    RewriteRule(
        name="Add with zero",
        pattern=Pattern(op="Add", args=[Pattern(var="x"), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return x;",
    ),
    RewriteRule(
        name="Sub of zero",
        pattern=Pattern(op="Sub", args=[Pattern(var="x"), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return x;",
    ),

    RewriteRule(
        name="Mul by zero",
        pattern=Pattern(var="v", op="Mul", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),
    RewriteRule(
        name="MulHi by zero",
        pattern=Pattern(var="v", op="MulHi", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),
    RewriteRule(
        name="UMulHi by zero",
        pattern=Pattern(var="v", op="UMulHi", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),

    # TODO: maybe somehow make these rules more general

    # (c ? k1 : k0) != k0 => c, where k0, k1 are constants and k0 != k1
    RewriteRule(
        pattern=Pattern(op="NotEqual", args=[
            Pattern(op="CondSelect", args=[Pattern(var="c"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(var="k0_", op="Const"),
        ]),
        guard="k0 != k1 && k0 == k0_",
        replace="return c;",
    ),

    # TODO: make these into something more general? to support for example fp
    # conditionals: (c0 ? 1.0 : 0.0) & (c1 ? 1.0 : 0.0)

    RewriteRule(
        pattern=Pattern(var="v", op="And", args=[
            Pattern(op="CondSelect", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="CondSelect", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, cg2c_buildArith2(b, OpAnd, c0, c1));
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Or", args=[
            Pattern(op="CondSelect", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="CondSelect", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, cg2c_buildArith2(b, OpOr, c0, c1));
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Xor", args=[
            Pattern(op="CondSelect", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="CondSelect", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, cg2c_buildCmp(b, OpNotEqual, c0, c1));
        """,
    ),

    # Fold selections by a constant.

    RewriteRule(
        pattern=Pattern(op="CondSelect", args=[Pattern(var="c", op="Const"), Pattern(var="x"), Pattern(var="y")]),
        replace="return !cg2c_valueAuxIntZero(c) ? x : y;",
    ),

    # Fold conversions

    RewriteRule(
        name="SignExt of a constant",
        pattern=Pattern(var="v", op="SignExt", args=[Pattern(var="k", op="Const")]),
        replace="return newConst(b, v->type, (uint64_t)cg2c_valueAuxInt(k));",
    ),
    RewriteRule(
        name="ZeroExt of a constant",
        pattern=Pattern(var="v", op="ZeroExt", args=[Pattern(var="k", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(k));",
    ),

    RewriteRule(
        name="IToF of a constant",
        pattern=Pattern(var="w", op="IToF", args=[Pattern(var="v", op="Const")]),
        replace="""
            double y = (double)cg2c_valueAuxInt(v);

            switch (cg2c_typeKind(w->type)) {
            case KindInt32:
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits((float)y));

            default:
                assert(0); // not implemented
                return NULL;
            }
        """,
    ),

    RewriteRule(
        name="UToF of a constant",
        pattern=Pattern(var="w", op="UToF", args=[Pattern(var="v", op="Const")]),
        replace="""
            double y = (double)cg2c_valueAuxUint(v);

            switch (cg2c_typeKind(w->type)) {
            case KindInt32:
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits((float)y));

            default:
                assert(0); // not implemented
                return NULL;
            }
        """,
    ),

    RewriteRule(
        name="FToF of a constant",
        pattern=Pattern(var="w", op="FToF", args=[Pattern(var="v", op="Const")]),
        replace="""
            double y = cg2c_valueAuxFloat(v);

            switch (cg2c_typeKind(w->type)) {
            case KindInt32:
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits((float)y));

            default:
                assert(0); // not implemented
                return NULL;
            }
        """,
    ),

    # Get rid of useless conversions.
    #
    # These rules are required to produce valid SPIR-V.
    #
    # BUG: these are deprecated, we'll disallow useless conversions.

    RewriteRule(pattern=Pattern(var="v", op="SignExt", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),
    RewriteRule(pattern=Pattern(var="v", op="ZeroExt", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),
    RewriteRule(pattern=Pattern(var="v", op="FToF", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),

    # A PtrToInt (IntToPtr x) => x rule is not valid, as such a construct
    # performs erasure of provenance information. See the following link for
    # details
    # https://nhaehnle.blogspot.com/2021/06/can-memcpy-be-implemented-in-llvm-ir.html

    RewriteRule(name="IntToPtr of PtrToInt", pattern=Pattern(op="IntToPtr", args=[Pattern(op="PtrToInt", args=[Pattern(var="x")])]), replace="return x;"),

    RewriteRule(
        name="unpermuted MakeTuple of Extracts from same tuple",
        pattern=Pattern(var="v", op="MakeTuple", args=[Pattern(op="Extract", args=[Pattern(var="u")])], ddd=True),
        guard="isMakeTupleOfExtractsUnpermuted(v, u)",
        replace="return u;",
    ),

    RewriteRule(
        name="Extract of MakeTuple",
        pattern=Pattern(var="x", op="Extract", args=[Pattern(var="constructor", op="MakeTuple", ddd=True)]),
        replace="return cg2c_valueArg(constructor, cg2c_valueAuxSize(x));",
    ),

    RewriteRule(
        name="AddPtr with zero offset",
        pattern=Pattern(op="AddPtr", args=[Pattern(var="base"), Pattern(var="off", op="Const")]),
        guard="cg2c_valueAuxIntZero(off)",
        replace="return base;",
    ),

    # Collapse composite AddPtrs and let other transformations deal with offset
    # arithmetic.
    #
    # Note: we could mark the Add no-signed-overflow. c/gen.c could also mark
    # Mul by sizeof as no-signed-overflow. Not clear whether it's worth doing.
    RewriteRule(
        name="AddPtr of AddPtr",
        pattern=Pattern(op="AddPtr", args=[Pattern(op="AddPtr", args=[Pattern(var="base"), Pattern(var="off0")]), Pattern(var="off1")]),
        replace="return cg2c_buildAddPtr(b, base, cg2c_buildArith2(b, OpAdd, off0, off1));",
    ),

    # Split loads and stores of tuples.

    RewriteRule(
        name="split Load of a tuple",
        pattern=Pattern(var="load", op="Load", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="cg2c_typeIs(load->type, KindTuple)",
        replace="""
            int64_t align = cg2c_valueAuxAlign(load);

            size_t n = cg2c_tupleNElem(load->type);

            Slice result = cg2c_mkslicezeroed(sizeof(Value*), n, b.ctxt->gc);
            int64_t off = 0;
            for (size_t i = 0; i < n; i++) {
                Type *elemt = cg2c_tupleElem(load->type, i);

                Value *q = cg2c_buildAddPtrConst(b, p, off);
                Value *e = cg2c_buildLoad(b, elemt, cg2c_gcd(align, off), mem, q);

                cg2c_slindex(Value*, result, i) = e;
                off += cg2c_typeSize(b.ctxt, elemt);
            }

            // Extract forwarding will take care from here on.
            return cg2c_buildMakeTuple(b, result);
        """,
    ),
    RewriteRule(
        name="split Store of a tuple",
        pattern=Pattern(var="store", op="Store", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        guard="cg2c_typeIs(v->type, KindTuple)",
        replace="""
            int64_t align = cg2c_valueAuxAlign(store);

            int64_t off = 0;
            for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
                Type *elemt = cg2c_tupleElem(v->type, i);

                Value *q = cg2c_buildAddPtrConst(b, p, off);
                Value *e = cg2c_buildExtract(b, v, i);
                mem = cg2c_buildStore(b, cg2c_gcd(align, off), mem, q, e);
                off += cg2c_typeSize(b.ctxt, elemt);
            }

            return mem;
        """,
    ),

    # TOOD: remove the remaining hack rules
    RewriteRule(
        pattern=Pattern(var="v", op="Call", args=[
            Pattern(var="s", op="Symbol"),
            Pattern(op="MakeTuple", args=[
                Pattern(var="mem"),
                Pattern(var="im"),
                Pattern(var="sampler"),
                Pattern(var="P"),
            ]),
        ]),
        guard="cg2c_strcmp(cg2c_valueAuxSymbol(s)->name, cg2c_stringnocopy(\"__tex2D\")) == 0",
        replace="""
            AuxImage *aux = cg2c_malloc(sizeof *aux, b.ctxt->gc);
            aux->comp = 1;
            aux->dim = Dim2D;
            aux->arrayed = false;
            aux->ms = false;
            Value *result = newValue4A(b, OpTex, cg2c_tupleElem(v->type, 1), aux, mem, im, sampler, P);
            return cg2c_buildMakeTuple2(b, mem, result);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Call", args=[
            Pattern(var="s", op="Symbol"),
            Pattern(op="MakeTuple", args=[
                Pattern(var="mem"),
                Pattern(var="im"),
                Pattern(var="sampler"),
                Pattern(var="P"),
            ]),
        ]),
        guard="cg2c_strcmp(cg2c_valueAuxSymbol(s)->name, cg2c_stringnocopy(\"__texCube\")) == 0",
        replace="""
            AuxImage *aux = cg2c_malloc(sizeof *aux, b.ctxt->gc);
            aux->comp = 1;
            aux->dim = DimCube;
            aux->arrayed = false;
            aux->ms = false;
            Value *result = newValue4A(b, OpTex, cg2c_tupleElem(v->type, 1), aux, mem, im, sampler, P);
            return cg2c_buildMakeTuple2(b, mem, result);
        """,
    ),
]
print(RULES_C.render(label="cg2c_commonRules", rules=rules))
