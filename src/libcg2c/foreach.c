#include "all.h"

static void
foreach(Compile *ctxt, VisitorFunc visitor, void *etc, Value *v, Map *visited)
{
	GC *gc = ctxt->gc;

	if (cg2c_mapcontains(cg2c_ptr_set, visited, &v))
		return;

	cg2c_mapassign(cg2c_ptr_set, visited, &v, gc);

	switch (v->op) {
	case OpIf: {
		foreach(ctxt, visitor, etc, cg2c_regionTerminator(cg2c_valueArgRegion(v, 1)), visited);
		foreach(ctxt, visitor, etc, cg2c_regionTerminator(cg2c_valueArgRegion(v, 2)), visited);
		break;
	}

	case OpLoop: {
		foreach(ctxt, visitor, etc, cg2c_regionTerminator(cg2c_valueArgRegion(v, 1)), visited);
		break;
	}

	default:
		break;
	}

	for (size_t i = 0; i < cg2c_valueNArg(v); i++)
		foreach(ctxt, visitor, etc, cg2c_valueArg(v, i), visited);

	visitor(ctxt, v, etc);
}

void
cg2c_foreach(Compile *ctxt, VisitorFunc visitor, void *etc, Region *r)
{
	foreach(ctxt, visitor, etc, cg2c_regionTerminator(r), cg2c_mkmap(cg2c_ptr_set, ctxt->gc));
}
