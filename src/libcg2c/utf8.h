#pragma once

#include <stddef.h>
#include <stdint.h>

typedef int32_t Rune;

enum {
	Runeerror = 0xFFFD, // error rune or "Unicode replacement character"
	Runeself  = 0x80,   // runes represented in a single byte
	UTFmax    = 4,      // maximum length of any UTF sequence
};

// cg2c_runetochar writes the UTF sequence of rune r to s.
int cg2c_runetochar(char *s, Rune r);

// cg2c_chartorune decodes an at most n-byte long UTF sequence starting at s,
// writes the corresponding rune to r and returns the length of the sequence.
int cg2c_chartorune(Rune *r, const char *s, size_t n);

