#include "all.h"

static const char *kindnames[] = {
	[KindBool]  = "bool",
	[KindInt8]  = "int8",
	[KindInt16] = "int16",
	[KindInt32] = "int32",
	[KindInt64] = "int64",
	[KindPtr]   = "ptr",
	[KindMem]   = "mem",
	[KindRMem]  = "rmem",
	[KindNoret] = "noret",
};

static void
fdumptype(Type *type, FILE *f)
{
	Kind kind = cg2c_typeKind(type);
	switch (kind) {
	case KindBool:
	case KindInt8: case KindInt16: case KindInt32: case KindInt64:
	case KindMem:
	case KindRMem:
	case KindNoret:
		fprintf(f, "%s", kindnames[kind]);
		break;

	case KindPtr:
		fprintf(f, "ptr");
		break;

	case KindFunc:
		fprintf(f, "func ");
		fdumptype(cg2c_funcParam(type), f);
		fprintf(f, " ");
		fdumptype(cg2c_funcResult(type), f);
		break;

	case KindTuple:
		fprintf(f, "(");
		for (size_t i = 0; i < cg2c_tupleNElem(type); i++) {
			if (i > 0)
				fprintf(f, ", ");
			fdumptype(cg2c_tupleElem(type, i), f);
		}
		fprintf(f, ")");
		break;

	default:
		assert(0);
	}
}

void
cg2c_dumptype(Type *type)
{
	fdumptype(type, stderr);
	fprintf(stderr, "\n");
}

void
cg2c_dump(Compile *ctxt, const char *pass, Symbol *s)
{
	if (ctxt->dump == NULL)
		return;

	FILE *f = ctxt->dump;

	fflush(f);
}
