#include "all.h"

PosBase*
cg2c_newPosBase(String filename, GC *gc)
{
	PosBase *base = cg2c_malloc(sizeof *base, gc);
	base->filename = filename;
	return base;
}

Pos*
cg2c_newPos(PosBase *base, uint32_t line, uint32_t col, GC *gc)
{
	assert(base != NULL);

	Pos *pos = cg2c_malloc(sizeof *pos, gc);
	pos->base = base;
	pos->line = line;
	pos->col = col;
	return pos;
}

bool cg2c_posEqual(Pos a, Pos b) { return a.base == b.base && a.line == b.line && a.col == b.col; }
