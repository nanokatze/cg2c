#include "all.h"

static int
snprintpos(char *buf, size_t size, Pos *pos)
{
	if (pos == NULL) {
		if (size > 0)
			buf[0] = '\0';
		return 0;
	}

	return snprintf(buf, size, "%.*s:%d:%d",
		cg2c_strlen2(pos->base->filename), cg2c_strdata(pos->base->filename),
		pos->line,
		pos->col);
}

void
cg2c_warnfAt(Compile *ctxt, Pos *pos, const char *format, ...)
{
	char posbuf[1024] = {0};
	snprintpos(posbuf, sizeof posbuf, pos);

	char buf[1024] = {0};
	va_list arg;
	va_start(arg, format);
	vsnprintf(buf, sizeof buf, format, arg);
	va_end(arg);

	cg2c_warnf(ctxt, "%s: %s", posbuf, buf);
}

void
cg2c_warnf(Compile *ctxt, const char *format, ...)
{
	char buf[1024] = {0};
	va_list arg;
	va_start(arg, format);
	vsnprintf(buf, sizeof buf, format, arg);
	va_end(arg);
	ctxt->diag(buf, 1, ctxt->diagUserData);
}

void
cg2c_errorfAt(Compile *ctxt, Pos *pos, const char *format, ...)
{
	char posbuf[1024] = {0};
	snprintpos(posbuf, sizeof posbuf, pos);

	char buf[1024] = {0};
	va_list arg;
	va_start(arg, format);
	vsnprintf(buf, sizeof buf, format, arg);
	va_end(arg);

	cg2c_errorf(ctxt, "%s: %s", posbuf, buf);
}

void
cg2c_errorf(Compile *ctxt, const char *format, ...)
{
	va_list arg;
	va_start(arg, format);
	cg2c_verrorf(ctxt, format, arg);
	va_end(arg);
}

void
cg2c_verrorf(Compile *ctxt, const char *format, va_list arg)
{
	char buf[1024] = {0};
	vsnprintf(buf, sizeof buf, format, arg);
	ctxt->diag(buf, 0, ctxt->diagUserData);
	ctxt->errored = true;
}
