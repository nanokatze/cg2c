#pragma once

#include <stddef.h>

typedef struct Cg2cAllocFuncs Cg2cAllocFuncs;

typedef struct GC GC;

void* cg2c_malloc(size_t size, GC *gc) __attribute__((malloc));
void* cg2c_memdup(const void *p, size_t size, GC *gc) __attribute__((malloc));

int cg2c_gccall(void (*f)(void*, GC*), void *arg, const Cg2cAllocFuncs *_unused);
