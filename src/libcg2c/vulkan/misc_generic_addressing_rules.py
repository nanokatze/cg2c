from rulegen import Pattern, RewriteRule, RULES_C


print("""
#include "all.h"

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
	Value *args[] = {a0};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue2I(Builder b, Op op, Type *type, int64_t aux, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, &aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue3I(Builder b, Op op, Type *type, int64_t aux, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, &aux, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}
""")

rules = [
    RewriteRule(
        name="AddPtr to integer arithmetic",
        pattern=Pattern(op="AddPtr", args=[Pattern(var="p"), Pattern(var="off")]),
        replace="return cg2c_buildIntToPtr(b, cg2c_buildArith2(b, OpAdd, cg2c_buildPtrToInt(b, p), off));",
    ),

    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(var="p")]),
        replace="""
            Type *intptr = cg2c_intptr(b.ctxt);
            assert(cg2c_typeIs(intptr, KindInt64));

            Value *tag = cg2c_buildShiftConst(b, OpURsh, cg2c_buildPtrToInt(b, p), 62);
            Value *global = cg2c_buildConst(b, intptr, 0b00);
            Value *global2 = cg2c_buildConst(b, intptr, 0b11);
            return cg2c_buildArith2(b, OpOr,
                cg2c_buildCmp(b, OpEqual, tag, global),
                cg2c_buildCmp(b, OpEqual, tag, global2));
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="load", op="LoadGlobal", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="cg2c_typeIs(load->type, KindPtr)",
        replace="""
            int64_t align = (int64_t)*(uint64_t*)load->aux;

            return cg2c_buildIntToPtr(b, newValue2I(b, OpLoadGlobal, cg2c_intptr(b.ctxt), align, mem, p));
        """,
    ),

    # Split scratch loads and stores to individual bytes (int32s for now)

    RewriteRule(
        pattern=Pattern(var="load", op="LoadScratch", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="cg2c_typeIs(load->type, KindPtr)",
        replace="""
            int64_t align = (int64_t)*(uint64_t*)load->aux;

            return cg2c_buildIntToPtr(b, newValue2I(b, OpLoadScratch, cg2c_intptr(b.ctxt), align, mem, p));
        """,
    ),
    RewriteRule(
        name="split scratch Load",
        pattern=Pattern(var="load", op="LoadScratch", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="cg2c_typeIs(load->type, KindInt64)",
        replace="""
            int64_t align = (int64_t)*(uint64_t*)load->aux;

            Value *q = cg2c_buildAddPtrConst(b, p, 4);

            Value *lo = newValue1(b, OpZeroExt, cg2c_types[KindInt64],
                newValue2I(b, OpLoadScratch, cg2c_types[KindInt32], align, mem, p));
            Value *hi = newValue1(b, OpZeroExt, cg2c_types[KindInt64],
                newValue2I(b, OpLoadScratch, cg2c_types[KindInt32], cg2c_gcd(align, 4), mem, q));

            return cg2c_buildArith2(b, OpOr, lo, cg2c_buildShiftConst(b, OpLsh, hi, 32));
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="store", op="StoreScratch", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="val")]),
        guard="cg2c_typeIs(val->type, KindPtr)",
        replace="""
            int64_t align = (int64_t)*(uint64_t*)store->aux;

            return newValue3I(b, OpStoreScratch, cg2c_types[KindMem], align, mem, p, cg2c_buildPtrToInt(b, val));
        """,
    ),
    RewriteRule(
        name="split scratch Store",
        pattern=Pattern(var="store", op="StoreScratch", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        guard="cg2c_typeIs(v->type, KindInt64)",
        replace="""
            int64_t align = (int64_t)*(uint64_t*)store->aux;

            Value *lo = newValue1(b, OpTrunc, cg2c_types[KindInt32], v);
            Value *hi = newValue1(b, OpTrunc, cg2c_types[KindInt32], cg2c_buildShiftConst(b, OpURsh, v, 32));

            Value *q = cg2c_buildAddPtrConst(b, p, 4);

            mem = newValue3I(b, OpStoreScratch, cg2c_types[KindMem], align, mem, p, lo);
            mem = newValue3I(b, OpStoreScratch, cg2c_types[KindMem], cg2c_gcd(align, 4), mem, q, hi);

            return mem;
        """,
    ),
]
print(RULES_C.render(label="cg2c_miscGenericAddressingRulesVk", rules=rules))
