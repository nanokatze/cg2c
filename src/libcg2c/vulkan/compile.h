#include "all.h"

#include <vulkan/vulkan_core.h>

#include <spirv/unified1/spirv.h>
#include <spirv/unified1/GLSL.std.450.h>

#include <cg2c/cg2c_vulkan.h>

#include "scratch.h"
#include "spirvutil/spirvutil.h"

#include "device_features.h"

// cg2c_toSpvVk produces a Vulkan SPIR-V shader blob.
//
// It makes the following assumptions about the program:
//
//	* all regions are scheduled
//	* loads load and stores store integer or pointer -typed values only
//	* region parameters and results are flat tuples (i.e. do not contain tuples)
//
// TODO: document more assumptions
//
// TODO: pass the entire list of Symbol* in addition to entry point
Slice cg2c_toSpvVk(Compile *ctxt, const Cg2cCompileInfoVk *compileInfo, Symbol *s);

void cg2c_checkVk(Compile *ctxt, Symbol *s);

extern Slice cg2c_vulkanRules;
extern Slice cg2c_miscGenericAddressingRulesVk;
