#include "compile.h"

static void
check(Compile *ctxt, Value *v, void *_unused)
{
	switch (v->op) {
	case OpDiv: case OpUDiv: case OpRem: case OpURem: {
		Value *x = cg2c_valueArg(v, 0);
		Value *y = cg2c_valueArg(v, 1);

		if (y->op != OpConst)
			cg2c_warnfAt(ctxt, v->pos, "integer division by a non-constant");

		break;
	}

	default:
		break;
	}
}

void
cg2c_checkVk(Compile *ctxt, Symbol *s)
{
	cg2c_foreach(ctxt, check, NULL, s->def);
}
