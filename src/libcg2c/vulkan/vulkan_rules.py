from rulegen import Pattern, RewriteRule, RULES_C


print("""
#include "all.h"

static bool
allLeavesCanOnlyBeGlobalAddresses(Value *v)
{
    switch (v->op) {
    case OpOr:
    case OpLsh:
    case OpZeroExt:
    case OpSignExt:
    case OpIntToPtr: {
        for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
            if (!allLeavesCanOnlyBeGlobalAddresses(cg2c_valueArg(v, i)))
                return false;
        }
        return true;
    }

    case OpConst:
        return true;

    case OpGetIndirectConstant:
    case OpGetPushConstant:
        return true;

    default:
        return false;
    }
}
""")

rules = [
    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(op="Var", args=[Pattern(), Pattern()])]),
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], false);",
    ),

    # TODO: remove this PtrIsGlobal rule in favor of a dedicated pass that can run a proper analysis
    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(var="p")]),
        guard="allLeavesCanOnlyBeGlobalAddresses(p)",
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], true);",
    ),
]
print(RULES_C.render(label="cg2c_vulkanRules", rules=rules))
