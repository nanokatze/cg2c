#include "compile.h"

#include "string_.h"

static uint32_t
typeBits(Type *type)
{
	switch (cg2c_typeKind(type)) {
	case KindInt16:
		return 16;
	case KindInt32:
		return 32;
	case KindInt64:
		return 64;
	default:
		assert(0);
		return 42;
	}
}

static Type*
homogeneousTuple(Type *tuple)
{
	Type *e0 = cg2c_tupleElem(tuple, 0);
	for (size_t i = 1; i < cg2c_tupleNElem(tuple); i++) {
		if (cg2c_tupleElem(tuple, i) != e0)
			return NULL;
	}
	return e0;
}

static SpvDim
cg2cDimToSpvDim(Dim dim)
{
	switch (dim) {
	case Dim1D:
		return SpvDim1D;
	case Dim2D:
		return SpvDim2D;
	case Dim3D:
		return SpvDim3D;
	case DimCube:
		return SpvDimCube;
	default:
		assert(0);
		return 42;
	}
}

// TODO: move into spvutil?
static Slice
spvAppends(Slice buf, String s, GC *gc)
{
	const char *c = cg2c_strdata(s);

	for (size_t i = 0; i < cg2c_strlen(s);) {
		uint32_t word = 0;
		for (size_t j = 0; j < 4 && i < cg2c_strlen(s); j++, i++)
			word |= (uint32_t)(unsigned char)c[i] << (j * 8);
		buf = cg2c_slappend(sizeof(SpvOperand), buf, &(SpvOperand) {.lit = word}, gc);
	}
	if (cg2c_strlen(s) % 4 == 0)
		buf = cg2c_slappend(sizeof(SpvOperand), buf, &(SpvOperand) {.lit = 0}, gc);
	return buf;
}

// TODO: move operandId and operandLiteral into spvutil?

static SpvDef*
operandId(SpvDef *v, size_t i)
{
	SpvOperand operand = cg2c_slindex(SpvOperand, v->operands, i);
	assert(operand.id != NULL);
	return operand.id;
}

static uint32_t
operandLiteral(SpvDef *v, size_t i)
{
	SpvOperand operand = cg2c_slindex(SpvOperand, v->operands, i);
	assert(operand.id == NULL);
	return operand.lit;
}

typedef struct Shader Shader;
struct Shader {
	Compile *ctxt;

	// TODO: should these fields that come from CompileInfo be placed before ctxt?

	// DeviceFeatures enabledDeviceFeatures;

	struct {
		const Cg2cBindlessVk* samplers;
		const Cg2cBindlessVk* sampledImages;
		const Cg2cBindlessVk* storageImages;
	} bindless;

	Map *extInstSets; // String -> SpvDef*

	/*
	SpvDef *intTypes[4];
	SpvDef *floatTypes[4];
	*/
	SpvDef *ptrType; // genericPtrType

	SpvDef *samplerDescriptors;
	SpvDef *textureDescriptors[5 /* sampled type */][DimLast][2 /* arrayed */][2 /* ms */];
	SpvDef *storageImageSets[5 /* sampled type */][DimLast][2 /* arrayed */][2 /* ms */];
	SpvDef *pushConstant;
	SpvDef *scratch;
	SpvDef *in[10];
	SpvDef *out[10];
	SpvDef *builtins[BuiltinLast];
	Slice interface;

	Map *map; // Value* -> SpvDef* if key is a non-tuple or Slice* otherwise

	Slice caps;
	Slice extInstImports;
	Slice memoryModel;
	Slice entryPoints;
	Slice execModes;
	Slice annot;
	Slice types;
	Slice code;

	Map *instrs; // hashed instrs
};

static SpvDef*
getExtInstSet(Shader *shader, String ext)
{
	GC *gc = shader->ctxt->gc;

	SpvDef *set = *(SpvDef* const*)cg2c_mapaccess1(cg2c_str_ptr_map, shader->extInstSets, &ext);
	if (set == NULL) {
		set = cg2c_spvDefsAppend(&shader->extInstImports, SpvOpExtInstImport, cg2c_spvNoTypeId, spvAppends(NULL_SLICE, ext, gc), gc);
		*(SpvDef**)cg2c_mapassign(cg2c_str_ptr_map, shader->extInstSets, &ext, gc) = set;
	}
	return set;
}

static SpvDef*
spvTypeVoid(Shader *shader)
{
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeVoid, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);
}

// This needs to be very fast
static SpvDef*
spvTypeBool(Shader *shader)
{
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeBool, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);
}

// This also needs to be very fast
static SpvDef*
spvTypeInt(Shader *shader, uint32_t width)
{
	SpvOperand o[] = {
		{.lit = width},
		{.lit = 0}, // signedness
	};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeInt, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

// This too needs to be very fast
static SpvDef*
spvTypeFloat(Shader *shader, uint32_t width)
{
	SpvOperand o[] = {{.lit = width}};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeFloat, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef* constInt(Shader *shader, SpvDef *type, uint32_t value);

static SpvDef*
spvTypeVector(Shader *shader, SpvDef *elem, uint32_t n)
{
	SpvOperand o[] = {{.id = elem}, {.lit = n}};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeVector, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
spvTypeImage(Shader *shader, SpvDef *sampled, SpvDim dim, bool arrayed, bool ms, bool sampledOrStorage)
{
	SpvOperand o[] = {
		{.id = sampled},
		{.lit = dim},
		{.lit = 2},
		{.lit = arrayed},
		{.lit = ms},
		{.lit = sampledOrStorage ? 2 : 1},
		{.lit = SpvImageFormatUnknown},
	};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeImage, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

// TODO: could we figure stride from elem?
static SpvDef*
spvTypeArray(Shader *shader, SpvDef *elem, uint32_t n, uint32_t stride)
{
	SpvDef *type;
	{
		SpvOperand o[] = {{.id = elem}, {.id = constInt(shader, spvTypeInt(shader, 32), n)}};
		type = cg2c_spvDefsAppend(&shader->types, SpvOpTypeArray, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	{
		SpvOperand o[] = {{.id = type}, {.lit = SpvDecorationArrayStride}, {.lit = stride}};
		cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	return type;
}

static SpvDef*
spvTypePointer(Shader *shader, SpvStorageClass class, SpvDef *elem)
{
	SpvOperand o[] = {{.lit = class}, {.id = elem}};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypePointer, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
spvTypeFunction(Shader *shader, SpvDef *result, Slice params)
{
	Slice o = NULL_SLICE;
	o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = result}, shader->ctxt->gc);
	o = cg2c_slcat(sizeof(SpvOperand), o, params, shader->ctxt->gc);
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeFunction, cg2c_spvNoTypeId, o, shader->ctxt->gc);
}

SpvDef*
constInt(Shader *shader, SpvDef *type, uint32_t value)
{
	SpvOperand o[] = {{.lit = value}};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpConstant, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
globalVariable(Shader *shader, SpvDef *type, SpvStorageClass storageClass)
{
	SpvOperand o[] = {{.lit = storageClass}};
	SpvDef *var = cg2c_spvDefsAppend(&shader->types, SpvOpVariable, spvTypePointer(shader, storageClass, type), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	shader->interface = cg2c_slappend(sizeof(SpvDef*), shader->interface, &var, shader->ctxt->gc);
	return var;
}

static SpvDef*
emitAccessChain(Shader *shader, SpvDef *elemt, SpvDef *base, Slice chain)
{
	assert(base->type->op == SpvOpTypePointer);
	SpvStorageClass storageClass = cg2c_slindex(SpvOperand, base->type->operands, 0).lit;

	Slice o = NULL_SLICE;
	o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = base}, shader->ctxt->gc);
	for (size_t i = 0; i < cg2c_sllen(chain); i++)
		o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = cg2c_slindex(SpvDef*, chain, i)}, shader->ctxt->gc);
	return cg2c_spvDefsAppend(&shader->code, SpvOpAccessChain, spvTypePointer(shader, storageClass, elemt), o, shader->ctxt->gc);
}

static SpvDef*
emitAccessChain1(Shader *shader, SpvDef *elemt, SpvDef *base, SpvDef *i0)
{
	SpvDef *chain[] = {i0};
	return emitAccessChain(shader, elemt, base, cg2c_mkslicenocopy(sizeof chain[0], chain, ARRAY_SIZE(chain)));
}

static SpvDef*
emitAccessChain2(Shader *shader, SpvDef *elemt, SpvDef *base, SpvDef *i0, SpvDef *i1)
{
	SpvDef *chain[] = {i0, i1};
	return emitAccessChain(shader, elemt, base, cg2c_mkslicenocopy(sizeof chain[0], chain, ARRAY_SIZE(chain)));
}

static SpvDef*
emitCompositeConstruct2(Shader *shader, SpvDef *type, SpvDef *e0, SpvDef *e1)
{
	SpvOperand o[] = {{.id = e0}, {.id = e1}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpCompositeConstruct, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
emitCompositeConstruct3(Shader *shader, SpvDef *type, SpvDef *e0, SpvDef *e1, SpvDef *e2)
{
	SpvOperand o[] = {{.id = e0}, {.id = e1}, {.id = e2}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpCompositeConstruct, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

// TODO: helpers to get operand as SpvDef* or uint32_t
static SpvDef*
getCompositeElementType(SpvDef *type, uint32_t i)
{
	switch (type->op) {
	case SpvOpTypeVector:
		return operandId(type, 0);

	default:
		assert(0);
		return NULL;
	}
}

static SpvDef*
emitCompositeExtract(Shader *shader, SpvDef *x, uint32_t i)
{
	SpvDef *type = getCompositeElementType(x->type, i);

	SpvOperand o[] = {{.id = x}, {.lit = i}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpCompositeExtract, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

enum Reinterpret {
	ReinterpretInt,
	ReinterpretFloat,
};

// TODO: naming
static SpvDef*
cg2cIntTypeToSpv(Shader *shader, Type *type, enum Reinterpret reinterpret)
{
	// TODO: this doesn't really belong here
	if (cg2c_typeKind(type) == KindBool)
		return spvTypeBool(shader);

	if (cg2c_typeKind(type) == KindPtr) {
		// TODO: maybe forbid use of cg2cIntTypeToSpv on pointers?
		return shader->ptrType;
	}

	switch (reinterpret) {
	case ReinterpretInt:
		return spvTypeInt(shader, typeBits(type));

	case ReinterpretFloat:
		return spvTypeFloat(shader, typeBits(type));

	default:
		assert(0);
		return NULL;
	}
}

static SpvDef*
maybeEmitBitcast(Shader *shader, SpvDef *type, SpvDef *x)
{
	if (x->type == type)
		return x;

	SpvOperand o[] = {{.id = x}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpBitcast, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void
emitSelectionMerge(Shader *shader, SpvDef *merge, uint32_t control)
{
	SpvOperand o[] = {{.id = merge}, {.lit = control}};
	cg2c_spvDefsAppend(&shader->code, SpvOpSelectionMerge, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void
emitBranch(Shader *shader, SpvDef *target)
{
	SpvOperand o[] = {{.id = target}};
	cg2c_spvDefsAppend(&shader->code, SpvOpBranch, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void
emitBranchConditional(Shader *shader, SpvDef *cond, SpvDef *then, SpvDef *else_)
{
	SpvOperand o[] = {{.id = cond}, {.id = then}, {.id = else_}};
	cg2c_spvDefsAppend(&shader->code, SpvOpBranchConditional, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
getId(Shader *shader, Value *v)
{
	// Tuples use something getIdTuple
	assert(!cg2c_typeIs(v->type, KindTuple));

	SpvDef *instr = *(SpvDef* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, shader->map, &v);
	assert(instr != NULL);
	return instr;
}

static SpvDef*
getIdTuple(Shader *shader, Value *v, size_t i)
{
	assert(cg2c_typeIs(v->type, KindTuple));

	Slice instrs = **(Slice* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, shader->map, &v);
	return cg2c_slindex(SpvDef*, instrs, i);
}

static void
maybeEmitLine(Shader *shader, Pos **curPos, Pos *pos)
{
	if (*curPos == pos)
		return;

	if (pos != NULL) {
		// cg2c_createAndAppendSpvDef(&shader->code, SpvOpLine, NULL, NULL_SLICE, shader->ctxt->gc);
	} else {
		cg2c_spvDefsAppend(&shader->code, SpvOpNoLine, NULL, NULL_SLICE, shader->ctxt->gc);
	}

	*curPos = pos;
}

// visitRegion generates SPIR-V code for a region r.
//
// TODO: document what's label and what visitRegion returns
//
// TODO: rename to something that's not "visit"?
static SpvDef* visitRegion(Shader *shader, ScratchLayout *scratchLayout, SpvDef *label, Region *r);

static void* nop(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused) { return NULL; }

typedef struct MySpvOp MySpvOp;
struct MySpvOp {
	const char *ext;
	uint32_t op;
};

typedef struct VisitOp VisitOp;
struct VisitOp {
	const char *params;
	// TODO: roll it into the params string, rename that to signature and move into MySpvOp probably
	const char *result;

	// SPIR-V ops to use depending on type of the first operand
	MySpvOp boolOp, intOp, ptrOp;
};

static void*
visitOp(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *etc_)
{
	VisitOp *etc = etc_;
	GC *gc = shader->ctxt->gc;

	MySpvOp op = {0};
	switch (cg2c_typeKind(cg2c_valueArg(v, 0)->type)) {
	case KindBool:
		op = etc->boolOp;
		break;
	case KindInt8: case KindInt16: case KindInt32: case KindInt64:
		op = etc->intOp;
		break;
	case KindPtr:
		op = etc->ptrOp;
		break;
	default:
		assert(0);
		break;
	}

	const char *p = etc->params;

	Slice spvArgs = NULL_SLICE;
	if (op.ext != NULL) {
		spvArgs = cg2c_slappend(sizeof(SpvOperand), spvArgs, &(SpvOperand) {.id = getExtInstSet(shader, cg2c_stringnocopy(op.ext))}, gc);
		spvArgs = cg2c_slappend(sizeof(SpvOperand), spvArgs, &(SpvOperand) {.lit = op.op}, gc);
	}
	for (size_t i = 0; i < cg2c_sllen(v->args); i++) {
		Value *arg = cg2c_valueArg(v, i);
		SpvDef *spvType = cg2cIntTypeToSpv(shader, arg->type, *p++ == 'f' ? ReinterpretFloat : ReinterpretInt);
		SpvDef *spvArg = maybeEmitBitcast(shader, spvType, getId(shader, arg));
		spvArgs = cg2c_slappend(sizeof(SpvOperand), spvArgs, &(SpvOperand) {.id = spvArg}, gc);
	}

	SpvDef *spvType = cg2cIntTypeToSpv(shader, v->type, etc->result ? ReinterpretFloat : ReinterpretInt);

	assert(*p == 0);

	return cg2c_spvDefsAppend(&shader->code, op.ext == NULL ? (SpvOp)op.op : SpvOpExtInst, spvType, spvArgs, gc);
}

static void*
visitMakeTuple(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	// TODO: make this visitor less ugly

	Slice elems = cg2c_mkslicezeroed(sizeof(SpvDef*), cg2c_valueNArg(v), shader->ctxt->gc);

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *v_i = cg2c_valueArg(v, i);
		if (cg2c_typeIs(v_i->type, KindMem))
			continue;
		cg2c_slindex(SpvDef*, elems, i) = getId(shader, v_i);
	}

	return cg2c_memdup(&elems, sizeof elems, shader->ctxt->gc);
}

static void*
visitExtract(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	if (cg2c_typeIs(v->type, KindMem))
		return NULL;

	uint32_t i = (uint32_t)*(uint64_t*)v->aux;

	Value *w = cg2c_valueArg(v, 0);

	return getIdTuple(shader, w, (size_t)i);
}

static void*
visitConst(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	GC *gc = shader->ctxt->gc;

	uint64_t val = cg2c_valueAuxUint(v);

	switch (cg2c_typeKind(v->type)) {
	case KindBool: {
		SpvOp op = val != 0 ? SpvOpConstantTrue : SpvOpConstantFalse;
		return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, op, spvTypeBool(shader), NULL_SLICE, gc);
	}

	case KindInt8: case KindInt16: case KindInt32: {
		SpvOperand o[] = {{.lit = (uint32_t)val}};
		return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpConstant, spvTypeInt(shader, typeBits(v->type)), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	case KindInt64: {
		SpvOperand o[] = {{.lit = (uint32_t)val}, {.lit = (uint32_t)(val >> 32)}};
		return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpConstant, spvTypeInt(shader, typeBits(v->type)), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	default:
		assert(0);
		return NULL;
	}
}

static void*
visitVar(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	SpvDef *spvInt32 = spvTypeInt(shader, 32);
	SpvDef *spvInt32x2 = spvTypeVector(shader, spvTypeInt(shader, 32), 2);

	bool ok;
	uint64_t off = *(const uint64_t*)cg2c_mapaccess2(cg2c_ptr_64_map, scratchLayout->offsets, &v, &ok);
	assert(ok);

	assert(off <= UINT32_MAX);

	// TODO: maybe we should just emit int64 constant and have Intel just
	// fix their driver?

	SpvDef *lo = constInt(shader, spvInt32, (uint32_t)off);
	SpvDef *hi = constInt(shader, spvInt32, (uint32_t)1 << 31);

	return maybeEmitBitcast(shader, cg2cIntTypeToSpv(shader, v->type, ReinterpretInt), emitCompositeConstruct2(shader, spvInt32x2, lo, hi));
}

// TODO: rename to be clear that it's for logical stuff only
static SpvDef*
emitLoad(Shader *shader, SpvDef *p)
{
	assert(p->type->op == SpvOpTypePointer);
	SpvDef *type = operandId(p->type, 1);

	SpvOperand o[] = {{.id = p}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpLoad, type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
cg2cTupleAsSpvVector(Shader *shader, SpvDef *elemt, Value *v)
{
	GC *gc = shader->ctxt->gc;

	// TODO: handle a case when v is not a tuple?

	assert(homogeneousTuple(v->type));

	Slice o = NULL_SLICE;
	for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
		SpvDef *e = maybeEmitBitcast(shader, elemt, getIdTuple(shader, v, i));
		o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = e}, gc);
	}
	return cg2c_spvDefsAppend(&shader->code, SpvOpCompositeConstruct, spvTypeVector(shader, elemt, (uint32_t)cg2c_tupleNElem(v->type)), o, gc);
}

// TODO: "emit" doesn't feel right, think about it more
static void*
emitCompositeExtracts(Shader *shader, SpvDef *v)
{
	uint32_t n = 0;
	switch (v->type->op) {
	case SpvOpTypeVector:
		n = operandLiteral(v->type, 1);
		break;

	default:
		assert(0);
		break;
	}

	Slice elems = cg2c_mkslicezeroed(sizeof(SpvDef*), n, shader->ctxt->gc);
	for (uint32_t i = 0; i < n; i++)
		cg2c_slindex(SpvDef*, elems, i) = emitCompositeExtract(shader, v, i);
	return cg2c_memdup(&elems, sizeof elems, shader->ctxt->gc);
}

// TODO: rename it to make it clearer that this is loading from a handle
static SpvDef*
emitLoadSampler(Shader *shader, SpvDef *handle)
{
	SpvDef *samplerType = cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeSampler, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);

	if (shader->samplerDescriptors == NULL) {
		SpvDef *runtimeArrayOfSamplers;
		{
			SpvOperand o[] = {{.id = samplerType}};
			runtimeArrayOfSamplers = cg2c_spvDefsAppend(&shader->types, SpvOpTypeRuntimeArray, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}

		shader->samplerDescriptors = globalVariable(shader, runtimeArrayOfSamplers, SpvStorageClassUniformConstant);

		{
			SpvOperand o[] = {{.id = shader->samplerDescriptors}, {.lit = SpvDecorationDescriptorSet}, {.lit = shader->bindless.samplers->set}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}
		{
			SpvOperand o[] = {{.id = shader->samplerDescriptors}, {.lit = SpvDecorationBinding}, {.lit = shader->bindless.samplers->binding}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}
	}

	return emitLoad(shader, emitAccessChain1(shader, samplerType, shader->samplerDescriptors, handle));
}

static void
decorateNonUniform(Shader *shader, SpvDef *id)
{
	SpvOperand o[] = {{.id = id}, {.lit = SpvDecorationNonUniform}};
	cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
spvTypeSampledImage(Shader *shader, SpvDef *imageType)
{
	SpvOperand o[] = {{.id = imageType}};
	return cg2c_spvDefsAppendHashed(&shader->types, shader->instrs, SpvOpTypeSampledImage, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static SpvDef*
emitSampledImage(Shader *shader, SpvDef *image, SpvDef *sampler)
{
	SpvOperand o[] = {{.id = image}, {.id = sampler}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpSampledImage, spvTypeSampledImage(shader, image->type), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void*
visitTex(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *tex, void *_unused)
{
	AuxImage *aux = tex->aux;

	Value *texture = cg2c_valueArg(tex, 1);
	assert(cg2c_typeIs(texture->type, KindInt32));
	Value *sampler = cg2c_valueArg(tex, 2);
	assert(cg2c_typeIs(sampler->type, KindInt32));
	Value *coord = cg2c_valueArg(tex, 3);
	assert(cg2c_typeIs(coord->type, KindTuple));

	SpvDef *samplerSpv = emitLoadSampler(shader, getId(shader, sampler));

	SpvDef *imageType = spvTypeImage(shader, spvTypeFloat(shader, 32), cg2cDimToSpvDim(aux->dim), aux->arrayed, aux->ms, false /* sampledOrStorage */);

	SpvDef **textureDescriptors = &shader->textureDescriptors[0][aux->dim][aux->arrayed][aux->ms];

	if (*textureDescriptors == NULL) {
		SpvDef *runtimeArrayOfImages;
		{
			SpvOperand o[] = {{.id = imageType}};
			runtimeArrayOfImages = cg2c_spvDefsAppend(&shader->types, SpvOpTypeRuntimeArray, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}

		*textureDescriptors = globalVariable(shader, runtimeArrayOfImages, SpvStorageClassUniformConstant);

		{
			SpvOperand o[] = {{.id = *textureDescriptors}, {.lit = SpvDecorationDescriptorSet}, {.lit = shader->bindless.sampledImages->set}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}
		{
			SpvOperand o[] = {{.id = *textureDescriptors}, {.lit = SpvDecorationBinding}, {.lit = shader->bindless.sampledImages->binding}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}
	}

	SpvDef *textureSpv = emitLoad(shader, emitAccessChain1(shader, imageType, *textureDescriptors, getId(shader, texture)));

	SpvDef *combined = emitSampledImage(shader, textureSpv, samplerSpv);

	decorateNonUniform(shader, combined);

	SpvDef *coordSpv = cg2cTupleAsSpvVector(shader, spvTypeFloat(shader, 32), coord);

	SpvDef *sample;
	{
		SpvOperand o[] = {{.id = combined}, {.id = coordSpv}};
		sample = cg2c_spvDefsAppend(&shader->code, SpvOpImageSampleImplicitLod, spvTypeVector(shader, spvTypeFloat(shader, 32), 4), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	return emitCompositeExtracts(shader, sample);
}

static void*
visitIf(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	Value *cond = cg2c_valueArg(v, 0);
	assert(cg2c_typeIs(cond->type, KindBool));
	Region *then = cg2c_valueArgRegion(v, 1);
	Region *else_ = cg2c_valueArgRegion(v, 2);

	SpvDef *spvThen = cg2c_newSpvDef(SpvOpLabel, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);
	SpvDef *spvElse = cg2c_newSpvDef(SpvOpLabel, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);
	SpvDef *spvMerge = cg2c_newSpvDef(SpvOpLabel, cg2c_spvNoTypeId, NULL_SLICE, shader->ctxt->gc);

	emitSelectionMerge(shader, spvMerge, SpvSelectionControlMaskNone);
	emitBranchConditional(shader, getId(shader, cond), spvThen, spvElse);

	spvThen = visitRegion(shader, scratchLayout, spvThen, then);
	emitBranch(shader, spvMerge);

	spvElse = visitRegion(shader, scratchLayout, spvElse, else_);
	emitBranch(shader, spvMerge);

	shader->code = cg2c_slappend(sizeof(SpvDef*), shader->code, &spvMerge, shader->ctxt->gc);

	*label = spvMerge;

	// BUG: we need to optionally bitcast terminators. We should do that
	// right before emitBranch

	Slice phis = cg2c_mkslicezeroed(sizeof(SpvDef*), cg2c_tupleNElem(v->type), shader->ctxt->gc);

	for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
		Type *et = cg2c_tupleElem(v->type, i);

		if (cg2c_typeIs(et, KindMem))
			continue;

		SpvOperand o[] = {
			{.id = getIdTuple(shader, then->terminator, i)}, {.id = spvThen},
			{.id = getIdTuple(shader, else_->terminator, i)}, {.id = spvElse},
		};
		cg2c_slindex(SpvDef*, phis, i) = cg2c_spvDefsAppend(&shader->code, SpvOpPhi, cg2cIntTypeToSpv(shader, et, ReinterpretInt), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	return cg2c_memdup(&phis, sizeof phis, shader->ctxt->gc);
}

static void*
visitLoop(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	assert(0);
	return NULL;
}

static void*
visitGetPushConstant(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused)
{
	uint32_t off = (uint32_t)cg2c_valueAuxUint(cg2c_valueArg(v, 0));

	SpvDef *typeSpv = cg2cIntTypeToSpv(shader, v->type, ReinterpretInt);

	SpvDef *p = emitAccessChain2(shader, typeSpv, shader->pushConstant, constInt(shader, spvTypeInt(shader, 32), 0), constInt(shader, spvTypeInt(shader, 32), off / 4));

	return emitLoad(shader, p);
}

// TODO: change GetInput to operate on scalars?
static void*
visitGetInput(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *get, void *_unused)
{
	SpvDef *spvFloat32 = spvTypeFloat(shader, 32);
	SpvDef *spvFloat32x4 = spvTypeVector(shader, spvFloat32, 4);

	uint64_t location = cg2c_valueAuxUint(cg2c_valueArg(get, 0));

	if (shader->in[location] == NULL) {
		shader->in[location] = globalVariable(shader, spvFloat32x4, SpvStorageClassInput);

		SpvOperand o[] = {{.id = shader->in[location]}, {.lit = SpvDecorationLocation}, {.lit = (uint32_t)location}};
		cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	SpvDef *loaded = emitLoad(shader, shader->in[location]);

	return emitCompositeExtracts(shader, loaded);
}

static void*
visitGetBuiltin(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *get, void *_unused)
{
	SpvDef *spvBool = spvTypeBool(shader);
	SpvDef *spvInt32 = spvTypeInt(shader, 32);

	Builtin builtin = (Builtin)cg2c_valueAuxUint(cg2c_valueArg(get, 0));

	switch (builtin) {
	case BuiltinVertexID: {
		if (shader->builtins[builtin] == NULL) {
			shader->builtins[builtin] = globalVariable(shader, spvInt32, SpvStorageClassInput);

			SpvOperand o[] = {{.id = shader->builtins[builtin]}, {.lit = SpvDecorationBuiltIn}, {.lit = SpvBuiltInVertexIndex}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}

		return emitLoad(shader, shader->builtins[builtin]);
	}

	case BuiltinFrontFacing: {
		if (shader->builtins[builtin] == NULL) {
			shader->builtins[builtin] = globalVariable(shader, spvTypeBool(shader), SpvStorageClassInput);

			SpvOperand o[] = {{.id = shader->builtins[builtin]}, {.lit = SpvDecorationBuiltIn}, {.lit = SpvBuiltInFrontFacing}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}

		return emitLoad(shader, shader->builtins[builtin]);
	}

	default:
		// TODO: be exhaustive
		assert(0);
		return NULL;
	}
}

static void*
visitStoreOutput(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *store, void *_unused)
{
	SpvDef *spvFloat32 = spvTypeFloat(shader, 32);
	SpvDef *spvFloat32x4 = spvTypeVector(shader, spvFloat32, 4);

	uint64_t location = cg2c_valueAuxUint(cg2c_valueArg(store, 1));
	Value *v = cg2c_valueArg(store, 2);

	if (shader->out[location] == NULL) {
		shader->out[location] = globalVariable(shader, spvFloat32x4, SpvStorageClassOutput);

		SpvOperand o[] = {{.id = shader->out[location]}, {.lit = SpvDecorationLocation}, {.lit = (uint32_t)location}};
		cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
	}

	SpvDef *vec4 = cg2cTupleAsSpvVector(shader, spvFloat32, v);

	SpvOperand o[] = {{.id = shader->out[location]}, {.id = vec4}};
	cg2c_spvDefsAppend(&shader->code, SpvOpStore, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);

	return NULL;
}

static void*
visitStoreBuiltin(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *store, void *_unused)
{
	SpvDef *spvFloat32 = spvTypeFloat(shader, 32);
	SpvDef *spvFloat32x4 = spvTypeVector(shader, spvFloat32, 4);

	Builtin builtin = (Builtin)cg2c_valueAuxUint(cg2c_valueArg(store, 1));
	Value *v = cg2c_valueArg(store, 2);

	switch (builtin) {
	case BuiltinPosition: {
		if (shader->builtins[builtin] == NULL) {
			shader->builtins[builtin] = globalVariable(shader, spvFloat32x4, SpvStorageClassOutput);

			SpvOperand o[] = {{.id = shader->builtins[builtin]}, {.lit = SpvDecorationBuiltIn}, {.lit = SpvBuiltInPosition}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
		}

		SpvDef *vec4 = cg2cTupleAsSpvVector(shader, spvFloat32, v);

		SpvOperand o[] = {{.id = shader->builtins[builtin]}, {.id = vec4}};
		cg2c_spvDefsAppend(&shader->code, SpvOpStore, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);

		return NULL;
	}

	default:
		assert(0);
		return NULL;
	}
}

static void*
visitStoreGlobal(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *store, void *_unused)
{
	uint32_t align = (uint32_t)*(int64_t*)store->aux;

	Value *p = cg2c_valueArg(store, 1);
	Value *v = cg2c_valueArg(store, 2);

	assert(0);
	return NULL;
}

// TODO: remove once we require generic addressing
static SpvDef*
emitArith2(Shader *shader, SpvOp op, SpvDef *x, SpvDef *y)
{
	assert(x->type == y->type);

	SpvOperand o[] = {{.id = x}, {.id = y}};
	return cg2c_spvDefsAppend(&shader->code, op, x->type, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void*
visitStoreScratch(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *store, void *_unused)
{
	SpvDef *spvInt32 = spvTypeInt(shader, 32);
	SpvDef *spvInt32x2 = spvTypeVector(shader, spvInt32, 2);

	uint32_t align = (uint32_t)*(int64_t*)store->aux;

	Value *p = cg2c_valueArg(store, 1);
	Value *v = cg2c_valueArg(store, 2);

	SpvDef *ptrSpv = getId(shader, p);
	SpvDef *offSpv = emitCompositeExtract(shader, maybeEmitBitcast(shader, spvInt32x2, ptrSpv), 0);

	assert(cg2c_typeIs(v->type, KindInt32));

	SpvDef *indexSpv = emitArith2(shader, SpvOpUDiv, offSpv, constInt(shader, spvInt32, 4));
	SpvDef *scratchPtrSpv = emitAccessChain1(shader, spvInt32, shader->scratch, indexSpv);

	SpvDef *valueSpv = maybeEmitBitcast(shader, spvInt32, getId(shader, v));

	SpvOperand o[] = {{.id = scratchPtrSpv}, {.id = valueSpv}};
	return cg2c_spvDefsAppend(&shader->code, SpvOpStore, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void*
visitLoadGlobal(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *load, void *_unused)
{
	uint32_t align = (uint32_t)*(int64_t*)load->aux;

	{
		Kind kind = cg2c_typeKind(load->type);
		assert(kind == KindInt8 || kind == KindInt16 || kind == KindInt32 || kind == KindInt64);
	}

	Value *p = cg2c_valueArg(load, 1);

	SpvDef *typeSpv = cg2cIntTypeToSpv(shader, load->type, ReinterpretInt);

	SpvDef *ptrSpv = maybeEmitBitcast(shader, spvTypePointer(shader, SpvStorageClassPhysicalStorageBuffer, typeSpv), getId(shader, p));

	SpvOperand o[] = {
		{.id = ptrSpv},
		{.lit = SpvMemoryAccessAlignedMask | SpvMemoryAccessNonPrivatePointerMask},
		{.lit = align}, // align
	};
	return cg2c_spvDefsAppend(&shader->code, SpvOpLoad, typeSpv, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), shader->ctxt->gc);
}

static void*
visitLoadScratch(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *load, void *_unused)
{
	SpvDef *spvInt32 = spvTypeInt(shader, 32);

	uint32_t align = (uint32_t)*(int64_t*)load->aux;

	Value *p = cg2c_valueArg(load, 1);

	assert(cg2c_typeIs(load->type, KindInt32));

	SpvDef *typeSpv = cg2cIntTypeToSpv(shader, load->type, ReinterpretInt);

	SpvDef *offSpv = emitCompositeExtract(shader, maybeEmitBitcast(shader, spvTypeVector(shader, spvInt32, 2), getId(shader, p)), 0);

	SpvDef *indexSpv = emitArith2(shader, SpvOpUDiv, offSpv, constInt(shader, spvInt32, 4));
	SpvDef *ptrSpv = emitAccessChain1(shader, spvInt32, shader->scratch, indexSpv);

	return emitLoad(shader, ptrSpv);
}

static struct {
	// If v->type is not a tuple, visitor must return an SpvDef*.
	//
	// If v->type is a tuple, visitor must return a pointer to a Slice of
	// SpvDef*, each SpvDef* corresponding to a tuple element.
	void *(*f)(Shader *shader, ScratchLayout *scratchLayout, SpvDef **label, Value *v, void *_unused);
	void *etc;
} visitors[OpLast] = {
	[OpAdd]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpIAdd, .params = "xx"}},
	[OpSub]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpISub, .params = "xx"}},
	[OpMul]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpIMul, .params = "xx"}},
	[OpUDiv]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpUDiv, .params = "xx"}}, // TODO: a token to ignore certain arguments, third one in case of div and rem
	[OpURem]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpUMod, .params = "xx"}},
	[OpFAdd]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFAdd, .params = "ff", .result = "f"}},
	[OpFSub]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFSub, .params = "ff", .result = "f"}},
	[OpFMul]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFMul, .params = "ff", .result = "f"}},
	[OpFMA]             = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Fma}, .params = "fff", .result = "f"}},
	[OpFDiv]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFDiv, .params = "ff", .result = "f"}},
	[OpFSqrt]           = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Sqrt}, .params = "f", .result = "f"}},
	[OpAnd]             = {visitOp, &(VisitOp) {.boolOp.op = SpvOpLogicalAnd, .intOp.op = SpvOpBitwiseAnd, .params = "xx"}},
	[OpOr]              = {visitOp, &(VisitOp) {.boolOp.op = SpvOpLogicalOr, .intOp.op = SpvOpBitwiseOr, .params = "xx"}},
	[OpXor]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpBitwiseXor, .params = "xx"}},
	[OpLsh]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpShiftLeftLogical, .params = "xx"}},
	[OpRsh]             = {visitOp, &(VisitOp) {.intOp.op = SpvOpShiftRightArithmetic, .params = "xx"}},
	[OpURsh]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpShiftRightLogical, .params = "xx"}},
	[OpNot]             = {visitOp, &(VisitOp) {.boolOp.op = SpvOpLogicalNot, .intOp.op = SpvOpNot, .params = "x"}},
	[OpCondSelect]      = {visitOp, &(VisitOp) {.boolOp.op = SpvOpSelect, .params = "xxx"}},
	[OpEqual]           = {visitOp, &(VisitOp) {.boolOp.op = SpvOpLogicalEqual, .intOp.op = SpvOpIEqual, .ptrOp.op = SpvOpPtrEqual, .params = "xx"}},
	[OpFEqual]          = {visitOp, &(VisitOp) {.intOp.op = SpvOpFOrdEqual, .params = "ff"}},
	[OpNotEqual]        = {visitOp, &(VisitOp) {.boolOp.op = SpvOpLogicalNotEqual, .intOp.op = SpvOpINotEqual, .ptrOp.op = SpvOpPtrNotEqual, .params = "xx"}},
	[OpFNotEqual]       = {visitOp, &(VisitOp) {.intOp.op = SpvOpFUnordNotEqual, .params = "ff"}},
	[OpLess]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpSLessThan, .params = "xx"}},
	[OpULess]           = {visitOp, &(VisitOp) {.intOp.op = SpvOpULessThan, .params = "xx"}},
	[OpFLess]           = {visitOp, &(VisitOp) {.intOp.op = SpvOpFOrdLessThan, .params = "ff"}},
	[OpGreaterEqual]    = {visitOp, &(VisitOp) {.intOp.op = SpvOpSGreaterThanEqual, .params = "xx"}},
	[OpUGreaterEqual]   = {visitOp, &(VisitOp) {.intOp.op = SpvOpUGreaterThanEqual, .params = "xx"}},
	[OpFGreaterEqual]   = {visitOp, &(VisitOp) {.intOp.op = SpvOpFOrdGreaterThanEqual, .params = "ff"}},
	[OpFMin]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450NMin}, .params = "ff", .result = "f"}},
	[OpFMax]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450NMax}, .params = "ff", .result = "f"}},
	[OpFAbs]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450FAbs}, .params = "f", .result = "f"}},
	[OpFNeg]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFNegate, .params = "f", .result = "f"}},
	[OpFFloor]          = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Floor}, .params = "f", .result = "f"}},
	[OpFCeil]           = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Ceil}, .params = "f", .result = "f"}},
	[OpFTrunc]          = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Trunc}, .params = "f", .result = "f"}},
	[OpFRoundEven]      = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450RoundEven}, .params = "f", .result = "f"}},
	// [OpLdexp]           = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Ldexp}, .params = "fx", .result = "f"}},
	[OpSin]             = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Sin}, .params = "f", .result = "f"}},
	[OpCos]             = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Cos}, .params = "f", .result = "f"}},
	[OpTanh]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Tanh}, .params = "f", .result = "f"}},
	[OpExp2]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Exp2}, .params = "f", .result = "f"}},
	[OpLog2]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Log2}, .params = "f", .result = "f"}},
	[OpPowr]            = {visitOp, &(VisitOp) {.intOp = {.ext = "GLSL.std.450", .op = GLSLstd450Pow}, .params = "ff", .result = "f"}},
	[OpMakeTuple]       = {visitMakeTuple},
	[OpExtract]         = {visitExtract},
	[OpParam]           = {nop},
	[OpConst]           = {visitConst},
	[OpRegion]          = {nop},
	[OpTrunc]           = {visitOp, &(VisitOp) {.intOp.op = SpvOpSConvert, .params = "x"}},
	[OpSignExt]         = {visitOp, &(VisitOp) {.intOp.op = SpvOpSConvert, .params = "x"}},
	[OpZeroExt]         = {visitOp, &(VisitOp) {.intOp.op = SpvOpUConvert, .params = "x"}},
	[OpIToF]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpConvertSToF, .params = "x", .result = "f"}},
	[OpUToF]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpConvertUToF, .params = "x", .result = "f"}},
	[OpFToI]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpConvertFToS, .params = "f"}},
	[OpFToU]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpConvertFToU, .params = "f"}},
	[OpFToF]            = {visitOp, &(VisitOp) {.intOp.op = SpvOpFConvert, .params = "f", .result = "f"}},
	[OpPtrToInt]        = {visitOp, &(VisitOp) {.ptrOp.op = SpvOpBitcast, .params = "x"}},
	[OpIntToPtr]        = {visitOp, &(VisitOp) {.intOp.op = SpvOpBitcast, .params = "x"}},
	[OpVar]             = {visitVar},
	[OpTex]             = {visitTex},
	// [OpImageStore]      = {visitImageStore},
	[OpIf]              = {visitIf},
	[OpLoop]            = {visitLoop},
	[OpGetPushConstant] = {visitGetPushConstant},
	[OpGetInput]        = {visitGetInput},
	[OpGetBuiltin]      = {visitGetBuiltin},
	[OpStoreOutput]     = {visitStoreOutput},
	[OpStoreBuiltin]    = {visitStoreBuiltin},
	[OpStoreGlobal]     = {visitStoreGlobal},
	[OpLoadGlobal]      = {visitLoadGlobal},
	// [OpAtomicRMWGlobal] = {visitAtomicRMWGlobal},
	[OpLoadScratch]     = {visitLoadScratch},
	[OpStoreScratch]    = {visitStoreScratch},
};

static SpvDef*
visitRegion(Shader *shader, ScratchLayout *scratchLayout, SpvDef *label, Region *r)
{
	GC *gc = shader->ctxt->gc;

	shader->code = cg2c_slappend(sizeof(SpvDef*), shader->code, &label, gc);

	Pos *curPos = NULL;
	for (size_t i = 0; i < cg2c_sllen(r->values); i++) {
		Value *v = cg2c_slindex(Value*, r->values, i);

		if (!visitors[v->op].f) {
			// TODO: panic here
			fprintf(stderr, "%s unhandled\n", cg2c_opInfo[v->op].name);
		}

		if (visitors[v->op].f != nop)
			maybeEmitLine(shader, &curPos, v->pos);

		void *spv = visitors[v->op].f(shader, scratchLayout, &label, v, visitors[v->op].etc);
		if (spv != NULL)
			*(SpvDef**)cg2c_mapassign(cg2c_ptr_ptr_map, shader->map, &v, gc) = spv;
	}

	return label;
}

static uint32_t
calcPushConstantSize(const Cg2cInput *inputs, size_t ninput)
{
	uint32_t size = 0;
	for (size_t i = 0; i < ninput; i++) {
		const Cg2cInput *input = &inputs[i];
		if (input->type == CG2C_IN_PUSH_CONSTANT) {
			uint32_t sizeNeeded = input->pushConstantSrcOffset + input->size;
			if (size < sizeNeeded)
				size = sizeNeeded;
		}
	}
	return size;
}

static SpvExecutionModel
shaderStageToSpvExecModel(VkShaderStageFlagBits stage)
{
	switch (stage) {
	case VK_SHADER_STAGE_VERTEX_BIT:
		return SpvExecutionModelVertex;
	case VK_SHADER_STAGE_FRAGMENT_BIT:
		return SpvExecutionModelFragment;
	case VK_SHADER_STAGE_COMPUTE_BIT:
		return SpvExecutionModelGLCompute;
	default:
		assert(0);
		return 42;
	}
}

Slice
cg2c_toSpvVk(Compile *ctxt, const Cg2cCompileInfoVk *compileInfo, Symbol *s)
{
	GC *gc = ctxt->gc;

	Shader *shader = &(Shader) {
		.ctxt = ctxt,
		// .enabledDeviceFeatures = enabledDeviceFeatures,
		.bindless = {
			.samplers = compileInfo->samplers,
			.sampledImages = compileInfo->sampledImages,
			.storageImages = compileInfo->storageImages,
		},
		.map = cg2c_mkmap(cg2c_ptr_ptr_map, gc),
		.extInstSets = cg2c_mkmap(cg2c_str_ptr_map, gc),
		.instrs = cg2c_newHashedSpvInstrs(gc),
	};

	// TODO: turn it into an informative message
	assert(compileInfo->spirvVersion == 0x10600);

	// TODO: associate features with caps and caps with extensions

	// These caps are required
	//
	// TODO: set caps as necessary to produce minimal .spv
	SpvCapability caps[] = {
		SpvCapabilityShader,
		SpvCapabilitySampled1D,
		SpvCapabilityImage1D,
		SpvCapabilityStorageImageReadWithoutFormat,
		SpvCapabilityStorageImageWriteWithoutFormat,
		SpvCapabilityInt64,
		SpvCapabilityInt16,
		SpvCapabilityStorageBuffer16BitAccess,
		SpvCapabilityStorageBuffer8BitAccess,
		SpvCapabilityInt8,
		SpvCapabilityStorageImageArrayNonUniformIndexing,
		SpvCapabilitySampledImageArrayNonUniformIndexing,
		SpvCapabilityRuntimeDescriptorArray,
		SpvCapabilityPhysicalStorageBufferAddresses,
		SpvCapabilityVulkanMemoryModel,
		SpvCapabilityVulkanMemoryModelDeviceScope,
		SpvCapabilityShaderNonUniform,
	};
	for (size_t i = 0; i < ARRAY_SIZE(caps); i++) {
		SpvOperand o[] = {{.lit = caps[i]}};
		cg2c_spvDefsAppendHashed(&shader->caps, shader->instrs, SpvOpCapability, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	{
		SpvOperand o[] = {
			{.lit = SpvAddressingModelPhysicalStorageBuffer64},
			{.lit = SpvMemoryModelVulkan},
		};
		cg2c_spvDefsAppend(&shader->memoryModel, SpvOpMemoryModel, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	{
		SpvOperand o[] = {{.lit = SpvStorageClassPhysicalStorageBuffer}, {.id = spvTypeInt(shader, 8)}};
		shader->ptrType = cg2c_spvDefsAppend(&shader->types, SpvOpTypePointer, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	uint32_t pushConstantSize = calcPushConstantSize(compileInfo->inputs, compileInfo->inputCount);
	if (pushConstantSize > 0) {
		SpvDef *arrayType = spvTypeArray(shader, spvTypeInt(shader, 32), (pushConstantSize + 3) / 4, 4);

		SpvDef *blockType;
		{
			SpvOperand o[] = {{.id = arrayType}};
			blockType = cg2c_spvDefsAppend(&shader->types, SpvOpTypeStruct, cg2c_spvNoTypeId, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
		}

		{
			SpvOperand o[] = {{.id = blockType}, {.lit = SpvDecorationBlock}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
		}
		{
			SpvOperand o[] = {{.id = blockType}, {.lit = 0}, {.lit = SpvDecorationOffset}, {.lit = 0}};
			cg2c_spvDefsAppend(&shader->annot, SpvOpMemberDecorate, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
		}

		shader->pushConstant = globalVariable(shader, blockType, SpvStorageClassPushConstant);
	}

	ScratchLayout scratchLayout = cg2c_scratchAlloc(shader->ctxt, s);
	if (scratchLayout.size > 0) {
		SpvDef *type = spvTypeArray(shader, spvTypeInt(shader, 32), ((uint32_t)scratchLayout.size + 3) / 4, 4);
		shader->scratch = globalVariable(shader, type, SpvStorageClassPrivate);
	}

	SpvDef *main;
	{
		SpvOperand o[] = {
			{.lit = 0}, // control
			{.id = spvTypeFunction(shader, spvTypeVoid(shader), NULL_SLICE)},
		};
		main = cg2c_spvDefsAppend(&shader->code, SpvOpFunction, spvTypeVoid(shader), cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
	}

	SpvDef *entry = cg2c_newSpvDef(SpvOpLabel, cg2c_spvNoTypeId, NULL_SLICE, gc);
	visitRegion(shader, &scratchLayout, entry, s->def);

	{
		cg2c_spvDefsAppend(&shader->code, SpvOpReturn, NULL, NULL_SLICE, gc);
		cg2c_spvDefsAppend(&shader->code, SpvOpFunctionEnd, NULL, NULL_SLICE, gc);
	}

	{
		Slice o = NULL_SLICE;
		o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.lit = shaderStageToSpvExecModel(compileInfo->stage)}, gc);
		o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = main}, gc);
		o = spvAppends(o, cg2c_stringnocopy("main"), gc);
		for (size_t i = 0; i < cg2c_sllen(shader->interface); i++) {
			SpvDef *v = cg2c_slindex(SpvDef*, shader->interface, i);
			o = cg2c_slappend(sizeof(SpvOperand), o, &(SpvOperand) {.id = v}, gc);
		}
		cg2c_spvDefsAppend(&shader->entryPoints, SpvOpEntryPoint, NULL, o, gc);

		if (compileInfo->stage == VK_SHADER_STAGE_FRAGMENT_BIT) {
			SpvOperand o[] = {{.id = main}, {.lit = SpvExecutionModeOriginUpperLeft}};
			cg2c_spvDefsAppend(&shader->execModes, SpvOpExecutionMode, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
		}

		for (size_t i = 0; i < compileInfo->optionCount; i++) {
			const Cg2cOptionInfo *opt = &compileInfo->options[i];

			switch (opt->option) {
			case CG2C_OPTION_EARLY_FRAGMENT_TESTS: {
				SpvOperand o[] = {{.id = main}, {.lit = SpvExecutionModeEarlyFragmentTests}};
				cg2c_spvDefsAppend(&shader->execModes, SpvOpExecutionMode, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
				break;
			}

			case CG2C_OPTION_WORKGROUP_SIZE: {
				SpvOperand o[] = {
					{.id = main},
					{.lit = SpvExecutionModeLocalSize},
					{.lit = opt->workgroupSize[0]},
					{.lit = opt->workgroupSize[1]},
					{.lit = opt->workgroupSize[2]},
				};
				cg2c_spvDefsAppend(&shader->execModes, SpvOpExecutionMode, NULL, cg2c_mkslicenocopy(sizeof o[0], o, ARRAY_SIZE(o)), gc);
				break;
			}

			default:
				assert(0);
				break;
			}
		}
	}

	uint32_t header[] = {
		0x07230203, // SPIR-V magic
		compileInfo->spirvVersion,
		0x63326743, // our magic, "Cg2c"
		0,          // bound (will be written at the end)
		0,          // reserved
	};

	Slice out = cg2c_mkslice(sizeof header[0], header, ARRAY_SIZE(header), gc);

	Map *idMap = cg2c_mkmap(cg2c_ptr_32_map, gc);

	// See https://registry.khronos.org/SPIR-V/specs/unified1/SPIRV.html#LogicalLayout

	out = cg2c_spvAssemble(out, idMap, shader->caps, gc);
	out = cg2c_spvAssemble(out, idMap, shader->extInstImports, gc);
	out = cg2c_spvAssemble(out, idMap, shader->memoryModel, gc);
	out = cg2c_spvAssemble(out, idMap, shader->entryPoints, gc);
	out = cg2c_spvAssemble(out, idMap, shader->execModes, gc);
	out = cg2c_spvAssemble(out, idMap, shader->annot, gc);
	out = cg2c_spvAssemble(out, idMap, shader->types, gc);
	out = cg2c_spvAssemble(out, idMap, shader->code, gc);

	cg2c_slindex(uint32_t, out, 3) = (uint32_t)(cg2c_maplen(idMap) + 1);

	return out;
}
