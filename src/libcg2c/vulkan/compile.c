#include "compile.h"

#include <limits.h>
#include <string.h>

// TODO: transition to int8 scratch

static Value*
newValue1(Builder b, Op op, Type *type, Value *x)
{
	Value *args[] = {x};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *x, Value *y, Value *z)
{
	Value *args[] = {x, y, z};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, ARRAY_SIZE(args)));
}

static struct {
	Kind   elem;
	size_t n;
} builtinTypes[BuiltinLast] = {
	[BuiltinVertexID]             = {KindInt32, 1},
	[BuiltinFragCoord]            = {KindInt32, 4},
	[BuiltinFrontFacing]          = {KindBool, 1},
	[BuiltinGlobalInvocationID]   = {KindInt32, 3},
	[BuiltinLocalInvocationID]    = {KindInt32, 3},
	[BuiltinSubgroupInvocationID] = {KindInt32, 1},
};

static Value*
buildGetBuiltin(Builder b, Builtin builtin)
{
	assert(builtinTypes[builtin].elem > 0);

	Type *type = cg2c_types[builtinTypes[builtin].elem];
	if (builtinTypes[builtin].n > 1)
		type = cg2c_tupleN(b.ctxt, type, builtinTypes[builtin].n);

	return newValue1(b, OpGetBuiltin, type, cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)builtin));
}

static uint32_t
load32(const unsigned char *p)
{
	return (uint32_t)p[0] | (uint32_t)p[1]<<8 | (uint32_t)p[2]<<16 | (uint32_t)p[3]<<24;
}

static Value*
buildLoadInput(Builder b, const Cg2cInput *input)
{
	Compile *ctxt = b.ctxt;

	switch (*(const Cg2cInputType*)input) {
	case CG2C_IN_CONSTANT: {
		assert(input->size % 4 == 0);

		Slice vals = NULL_SLICE;
		for (uint32_t i = 0; i < input->size; i += 4) {
			assert(!b.ctxt->bigEndian);
			Value *val = cg2c_buildConst(b, cg2c_types[KindInt32], load32((const unsigned char*)input->constantData + i));
			vals = cg2c_slappend(sizeof(Value*), vals, &val, ctxt->gc);
		}
		return cg2c_buildMakeTuple(b, vals);
	}

	case CG2C_IN_INDIRECT_CONSTANT:
		assert(0); // not implemented
		return NULL;

	case CG2C_IN_PUSH_CONSTANT: {
		assert(input->size % 4 == 0);

		Slice vals = NULL_SLICE;
		for (uint32_t off = 0; off < input->size; off += 4) {
			Value *val = newValue1(b, OpGetPushConstant, cg2c_types[KindInt32], cg2c_buildConst(b, cg2c_types[KindInt32], input->pushConstantSrcOffset + off));
			vals = cg2c_slappend(sizeof(Value*), vals, &val, ctxt->gc);
		}
		return cg2c_buildMakeTuple(b, vals);
	}

	case CG2C_IN_VERTEX_ID: {
		assert(input->size == 4);
		return buildGetBuiltin(b, BuiltinVertexID);
	}

	case CG2C_IN_INTERPOLATED: {
		assert(input->size % 16 == 0);
		Type *vec4 = cg2c_tupleN(b.ctxt, cg2c_types[KindInt32], 4);
		Slice hmm = NULL_SLICE;
		for (uint32_t off = 0; off < input->size; off += 16) {
			Value *location = cg2c_buildConst(b, cg2c_types[KindInt32], input->interpolated.location + off/16);
			Value *x = newValue1(b, OpGetInput, vec4, location);
			hmm = cg2c_slappend(sizeof(Value*), hmm, &x, b.ctxt->gc);
		}
		return cg2c_buildMakeTuple(b, hmm);
	}

	case CG2C_IN_FRAG_COORD: {
		assert(input->size == 16);
		return buildGetBuiltin(b, BuiltinFragCoord);
	}

	case CG2C_IN_FRONT_FACING: {
		assert(input->size == 4);
		Value *frontFacing = buildGetBuiltin(b, BuiltinFrontFacing);
		Type *type = cg2c_types[KindInt32];
		Value *one = cg2c_buildConst(b, type, 1);
		Value *zero = cg2c_buildConst(b, type, 0);
		return newValue3(b, OpCondSelect, type, frontFacing, one, zero);
	}

	case CG2C_IN_GLOBAL_INVOCATION_ID: {
		Value *g = buildGetBuiltin(b, BuiltinGlobalInvocationID);

		switch (input->size) {
		case 4:
			return cg2c_buildExtract(b, g, 0);
		case 8:
			return cg2c_buildMakeTuple2(b, cg2c_buildExtract(b, g, 0), cg2c_buildExtract(b, g, 1));
		case 12:
			return g;
		default:
			assert(0);
			break;
		}

		break;
	}

	default:
		assert(0);
		return NULL;
	}
}

static Value*
buildPrologue(Builder b, const Cg2cInput *inputs, size_t inputCount, Value *mem)
{
	uint32_t interfaceSize = 0;
	for (uint32_t i = 0; i < inputCount; i++) {
		const Cg2cInput *input = &inputs[i];

		uint32_t off = input->offset + input->size;
		if (interfaceSize < off)
			interfaceSize = off;
	}

	Value *base = cg2c_buildVar(b, interfaceSize, 8);

	for (uint32_t i = 0; i < inputCount; i++) {
		const Cg2cInput *input = &inputs[i];

		Value *ptr = cg2c_buildAddPtrConst(b, base, input->offset);
		Value *val = buildLoadInput(b, input);
		mem = cg2c_buildStore(b, 1, mem, ptr, val);
	}

	return cg2c_buildMakeTuple2(b, mem, base);
}

static Value*
buildEpilogue(Builder b, const Cg2cOutput *outputs, size_t outputCount, Value *mem, Value *val)
{
	Type *vec4 = cg2c_tupleN(b.ctxt, cg2c_types[KindInt32], 4);

	Value *var = cg2c_buildVar(b, cg2c_typeSize(b.ctxt, val->type), 1);

	mem = cg2c_buildStore(b, 1, mem, var, val);

	for (uint32_t i = 0; i < outputCount; i++) {
		const Cg2cOutput *output = &outputs[i];

		switch (output->type) {
		case CG2C_OUT_VERTEX_POSITION: {
			assert(output->size == 16);

			Value *builtin = cg2c_buildConst(b, cg2c_types[KindInt32], BuiltinPosition);
			Value *xyzw = cg2c_buildLoad(b, vec4, 1, mem, cg2c_buildAddPtrConst(b, var, output->offset));
			mem = newValue3(b, OpStoreBuiltin, cg2c_types[KindMem], mem, builtin, xyzw);

			break;
		}

		case CG2C_OUT_VERTEX_ATTRIBUTE:
		case CG2C_OUT_FRAG_COLOR: {
			assert(output->size % 16 == 0);

			// TODO: think about fp16 attributes and color outputs
			for (uint32_t off = 0; off < output->size; off += 16) {
				Value *location = cg2c_buildConst(b, cg2c_types[KindInt32], output->vertexAttribute.location + off/16);
				Value *xyzw = cg2c_buildLoad(b, vec4, 1, mem, cg2c_buildAddPtrConst(b, var, output->offset+off));
				mem = newValue3(b, OpStoreOutput, cg2c_types[KindMem], mem, location, xyzw);
			}

			break;
		}

		default: {
			assert(0); // not implemented
		}
		}
	}

	return mem;
}

static Type*
tuple1n(Compile *ctxt, Type *e0, Type *e, size_t n)
{
	Slice elems = cg2c_mkslicezeroed(sizeof(Type*), 1+n, ctxt->gc);
	cg2c_slindex(Type*, elems, 0) = e0;
	for (size_t i = 0; i < n; i++)
		cg2c_slindex(Type*, elems, 1+i) = e;
	return cg2c_tuple(ctxt, elems);
}

typedef struct MathBuiltin {
	Op op;
	Kind kind;
	size_t n;
} MathBuiltin;

// TODO: have 1, 2, 3 -arg variants

static Region*
mathBuiltin(Builder b, void *etc)
{
	MathBuiltin *nice = etc;
	Compile *ctxt = b.ctxt;
	GC *gc = ctxt->gc;

	Type *type = cg2c_types[nice->kind];

	Type *fun = cg2c_func(ctxt,
		tuple1n(ctxt, cg2c_types[KindMem], type, nice->n),
		cg2c_tuple2(ctxt, cg2c_types[KindMem], type));

	Region *body = cg2c_newRegion(ctxt, fun);
	{
		Value *param = cg2c_buildParam(b, body);
		Value *mem = cg2c_buildExtract(b, param, 0);
		Slice x = NULL_SLICE;
		for (size_t i = 0; i < nice->n; i++) {
			Value *e = cg2c_buildExtract(b, param, 1+i);
			x = cg2c_slappend(sizeof(Value*), x, &e, gc);
		}
		Value *y = cg2c_buildValue(b, nice->op, type, NULL, x);
		cg2c_regionTerminate(body, cg2c_buildMakeTuple2(b, mem, y));
	}

	return body;
}

static Symbol*
shaderMain(Compile *ctxt, const Cg2cInput *inputs, size_t inputCount, const Cg2cOutput *outputs, size_t outputCount, Symbol *realMain)
{
	GC *gc = ctxt->gc;

	Builder b = {
		.ctxt = ctxt,
		.rewriter = cg2c_newRewriter(cg2c_commonRules, gc),
		.cse = cg2c_newCSE(gc),
	};

	b = cg2c_builderWithPos(b, cg2c_newPos(cg2c_newPosBase(cg2c_stringnocopy("<prologue>"), gc), 0, 0, gc));

	Region *main = cg2c_newRegion(ctxt, cg2c_func(ctxt,
		cg2c_tuple1(b.ctxt, cg2c_types[KindMem]),
		cg2c_tuple1(b.ctxt, cg2c_types[KindMem])));

	// TODO: we could inline both buildPrologue and buildEpilogue (after we
	// outline the switch into buildStoreOutput) here

	Value *arg = cg2c_buildExtract(b, cg2c_buildParam(b, main), 0);
	Value *argRewritten = buildPrologue(b, inputs, inputCount, arg);

	Value *call = cg2c_buildCall(b, cg2c_buildSymbol(b, realMain), argRewritten);

	b = cg2c_builderWithPos(b, cg2c_newPos(cg2c_newPosBase(cg2c_stringnocopy("<epilogue>"), gc), 0, 0, gc));

	Value *mem2 = cg2c_buildExtract(b, call, 0);
	Value *results = cg2c_buildExtract(b, call, 1);
	mem2 = buildEpilogue(b, outputs, outputCount, mem2, results);
	cg2c_regionTerminate(main, cg2c_buildMakeTuple1(b, mem2));

	Symbol *s = cg2c_malloc(sizeof *s, gc);
	s->name = cg2c_stringnocopy("main");
	s->type = main->type;
	s->binding = SymbolBindingLocal;
	s->def = main;
	s->cse = b.cse;
	return s;
}

static bool
hassuffix(const char *s, const char *suffix)
{
	size_t m = strlen(s);
	size_t n = strlen(suffix);
	return m >= n && memcmp(s+m-n, suffix, n) == 0;
}

static struct {
	const char *name;
	Region* (*f)(Builder b, void*);
	void *etc;
} builtins[] = {
	{"fmaf", mathBuiltin, &(MathBuiltin) {OpFMA, KindInt32, 3}},
	{"sqrtf", mathBuiltin, &(MathBuiltin) {OpFSqrt, KindInt32, 1}},
	// TODO: maybe we should also open-code fminf, fmaxf and fabsf?
	// Devising good rewrite rules for fminf and fmaxf is tricky
	// though...
	{"fminf", mathBuiltin, &(MathBuiltin) {OpFMin, KindInt32, 2}},
	{"fmaxf", mathBuiltin, &(MathBuiltin) {OpFMax, KindInt32, 2}},
	{"fabsf", mathBuiltin, &(MathBuiltin) {OpFAbs, KindInt32, 1}},
	{"floorf", mathBuiltin, &(MathBuiltin) {OpFFloor, KindInt32, 1}},
	{"ceilf", mathBuiltin, &(MathBuiltin) {OpFCeil, KindInt32, 1}},
	{"truncf", mathBuiltin, &(MathBuiltin) {OpFTrunc, KindInt32, 1}},
	{"roundevenf", mathBuiltin, &(MathBuiltin) {OpFRoundEven, KindInt32, 1}},
	// TODO: open-code ldexp and frexp and add rewrite rules to match them into ops
	{"__sinf", mathBuiltin, &(MathBuiltin) {OpSin, KindInt32, 1}},
	{"__cosf", mathBuiltin, &(MathBuiltin) {OpCos, KindInt32, 1}},
	{"__tanhf", mathBuiltin, &(MathBuiltin) {OpTanh, KindInt32, 1}},
	{"__exp2f", mathBuiltin, &(MathBuiltin) {OpExp2, KindInt32, 1}},
	{"__log2f", mathBuiltin, &(MathBuiltin) {OpLog2, KindInt32, 1}},
	{"__powrf", mathBuiltin, &(MathBuiltin) {OpPowr, KindInt32, 2}},
};

static Symbol*
findSymbolLazy(Compile *ctxt, String name, void *etc)
{
	Map *m = etc;
	GC *gc = ctxt->gc;

	Symbol *s = *(Symbol* const*)cg2c_mapaccess1(cg2c_str_ptr_map, m, &name);
	if (s != NULL)
		return s;

	// This is slightly terrible: ideally we'd use a mix of manual code (for
	// very combinatorial things like image functions) and some table for
	// boring stuff like __sinf, __cosf, etc.
	for (size_t i = 0; i < ARRAY_SIZE(builtins); i++) {
		if (cg2c_strcmp(cg2c_stringnocopy(builtins[i].name), name) != 0)
			continue;

		Builder b = {
			.ctxt = ctxt,
			.rewriter = cg2c_newRewriter(NULL_SLICE, gc),
			.cse = cg2c_newCSE(gc),
		};

		Region *body = builtins[i].f(b, builtins[i].etc);

		s = cg2c_malloc(sizeof *s, gc);
		s->name = cg2c_stringnocopy(builtins[i].name);
		s->type = body->type;
		s->binding = SymbolBindingWeak;
		s->def = body;
		s->cse = b.cse;

		*(Symbol**)cg2c_mapassign(cg2c_str_ptr_map, m, &name, gc) = s;

		break;
	}

	return s;
}

static int
compile(const Cg2cCompileInfoVk *compileInfo, const Cg2cAllocFuncs *_unused, Cg2cBlob **aOut, GC *gc)
{
	assert(_unused == NULL);

	Compile *compile = &(Compile) {
		.unsignedChar = CHAR_MIN == 0,
		.longSize     = sizeof(long),
		.ptrSize      = sizeof(void*),
		.funcSize     = 8,
		.bigEndian    = false,

		.funcs  = cg2c_mkmap(cg2c_ptrSlice_ptr_map, gc),
		.tuples = cg2c_mkmap(cg2c_ptrSlice_ptr_map, gc),

		.diag         = compileInfo->base.diag,
		.diagUserData = compileInfo->base.diagUserData,

		.gc = gc,
	};

	DeviceFeatures enabledDeviceFeatures;
	cg2c_gatherDeviceFeaturesVk(compileInfo->enabledDeviceFeatures, &enabledDeviceFeatures);

	// Validate shader IO and parse

	Slice /* of Symbol* */ symbols = NULL_SLICE;
	for (const char *const *cfile = compileInfo->base.files;
		cfile != NULL && cfile < compileInfo->base.files+compileInfo->base.fileCount;
		cfile++) {
		String file = cg2c_stringnocopy(*cfile);
		if (hassuffix(*cfile, ".cg2") || hassuffix(*cfile, ".c")) {
			const char *ccode = compileInfo->base.readFile(*cfile, compileInfo->base.readFileUserData);
			String code = cg2c_stringnocopy(ccode);
			Slice o = cg2c_parseC(compile, file, code); // of Symbol*
			symbols = cg2c_slcat(sizeof(Symbol*), symbols, o, compile->gc);
		} else {
			cg2c_errorf(compile, "%s does not end in .cg2 or .c", *cfile);
		}
	}

	if (compile->errored)
		return -1;

	// TODO: rework this. We intentionally made each stage info be unique so
	// as to allow for hypothetical multi entrypoint stages.

	Symbol *realMain = cg2c_findSymbol(symbols, cg2c_stringnocopy(compileInfo->entryPoint));
	if (realMain == NULL) {
		cg2c_errorf(compile, "undefined: %s", compileInfo->entryPoint);
		return -1;
	}

	Symbol *mainMain = shaderMain(compile, compileInfo->inputs, compileInfo->inputCount, compileInfo->outputs, compileInfo->outputCount, realMain);

	symbols = cg2c_slappend(sizeof(Symbol*), symbols, &mainMain, gc);

	for (size_t i = 0; i < cg2c_sllen(symbols); i++)
		cg2c_dump(compile, "gen", cg2c_slindex(Symbol*, symbols, i));

	symbols = cg2c_link(compile, &(LinkOptions) {0}, findSymbolLazy, cg2c_symbolMap(compile, symbols), mainMain);

	for (size_t i = 0; i < cg2c_sllen(symbols); i++)
		cg2c_dump(compile, "link", cg2c_slindex(Symbol*, symbols, i));

	// Optimize

	{
		Rewriter *rewriter = cg2c_newRewriter(cg2c_commonRules, compile->gc);

		for (;;) {
			Symbol *s = mainMain;

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			bool changed = false;

			// TODO: would be nice if we had a pass to just inline
			// everything recursively
			for (;;) {
				bool changed2 = false;

				s->def = cg2c_inlineCalls(b, s->def, &changed2);

				cg2c_dump(compile, "inline calls", s);

				if (changed2)
					changed = true;

				if (!changed2)
					break;
			}

			if (cg2c_loadsToSSA(b, &s->def))
				changed = true;

			cg2c_dump(compile, "loads to SSA", s);

			if (changed) {
				s->def = cg2c_simplifyControlFlow(b, s->def, &changed);

				cg2c_dump(compile, "simplify control flow", s);
			}

			if (!changed)
				break;
		}

		// Get rid of unused symbols.
		symbols = cg2c_liveSymbols(compile, mainMain);
	}

	// Produce performance diagnostics and check that things can actually be
	// lowered
	for (size_t i = 0; i < cg2c_sllen(symbols); i++)
		cg2c_checkVk(compile, cg2c_slindex(Symbol*, symbols, i));
	if (compile->errored)
		return -1;

	// Lower generic addressing

	if (true) {
		Slice rules = NULL_SLICE;
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_commonRules, gc);
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_vulkanRules, gc);
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_genericAddressingToTags, gc);
		Rewriter *rewriter = cg2c_newRewriter(rules, compile->gc);

		for (size_t i = 0; i < cg2c_sllen(symbols); i++) {
			Symbol *s = cg2c_slindex(Symbol*, symbols, i);

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			s->def = cg2c_rewritePlain(b, s->def);

			cg2c_dump(compile, "generic addressing to tags", s);

			// TODO: run additional passes here
		}
	}

	{
		Slice rules = NULL_SLICE;
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_commonRules, gc);
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_vulkanRules, gc);
		if (true)
			rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_miscGenericAddressingRulesVk, gc);
		Rewriter *rewriter = cg2c_newRewriter(rules, compile->gc);

		for (size_t i = 0; i < cg2c_sllen(symbols); i++) {
			Symbol *s = cg2c_slindex(Symbol*, symbols, i);

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			bool changed = false;

			s->def = cg2c_simplifyControlFlow(b, s->def, &changed);

			cg2c_dump(compile, "simplify control flow", s);
		}
	}

	if (compile->errored) {
		// Lowering error
		return -1;
	}

	// Schedule

	for (size_t i = 0; i < cg2c_sllen(symbols); i++) {
		Symbol *s = cg2c_slindex(Symbol*, symbols, i);

		cg2c_schedule(compile, s->def);

		cg2c_dump(compile, "schedule", s);
	}

	Slice out = cg2c_toSpvVk(compile, compileInfo, mainMain);

	size_t len = cg2c_sllen(out);

	uint8_t *out2 = cg2c_malloc(len*4 * sizeof *out2, gc); // TODO: recover
	for (size_t i = 0; i < len; i++) {
		uint32_t x = cg2c_slindex(uint32_t, out, i);
		out2[4*i+0] = (uint8_t)x;
		out2[4*i+1] = (uint8_t)(x >> 8);
		out2[4*i+2] = (uint8_t)(x >> 16);
		out2[4*i+3] = (uint8_t)(x >> 24);
	}

	Cg2cBlob *blob = cg2c_export(out2, len*4, _unused);
	// TODO: return error if we failed to allocate
	*aOut = blob;

	return 0;
}

typedef struct A A;
struct A {
	const Cg2cCompileInfoVk *compileInfo;
	const Cg2cAllocFuncs    *allocFuncs;
	Cg2cBlob                **aOut;
	int                     result;
};

static void
gcshim(void *_a, GC *gc)
{
	A *a = _a;
	a->result = compile(a->compileInfo, a->allocFuncs, a->aOut, gc);
}

int
cg2cCompileVk(const Cg2cCompileInfoVk *compileInfo, const Cg2cAllocFuncs *allocFuncs, Cg2cBlob **aOut)
{
	A a = {
		.compileInfo = compileInfo,
		.allocFuncs  = allocFuncs,
		.aOut        = aOut,
	};

	int r;
	if ((r = cg2c_gccall(gcshim, &a, allocFuncs)) < 0)
		return r;

	return a.result;
}
