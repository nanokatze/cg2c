from argparse import ArgumentParser
import sys
import typing
import xml.etree.ElementTree as et

import mako
from mako.template import Template

INTERESTING_FEATURES = [
    'vertexPipelineStoresAndAtomics',
    'fragmentStoresAndAtomics',
    'shaderFloat64',
    'shaderInt64',
    'shaderInt16',

    'storageBuffer16BitAccess',
    'uniformAndStorageBuffer16BitAccess',
    'storagePushConstant16',
    'storageInputOutput16',

    'storageBuffer8BitAccess',
    'uniformAndStorageBuffer8BitAccess',
    'storagePushConstant8',
    'shaderBufferInt64Atomics',
    'shaderSharedInt64Atomics',
    'shaderFloat16',
    'shaderInt8',
    'uniformBufferStandardLayout',
    'shaderSubgroupExtendedTypes',
    'bufferDeviceAddress',
    'vulkanMemoryModel',
    'vulkanMemoryModelDeviceScope',
    'vulkanMemoryModelAvailabilityVisibilityChains',

    'shaderDemoteToHelperInvocation',
    'shaderTerminateInvocation',
    'shaderZeroInitializeWorkgroupMemory',
    'shaderIntegerDotProduct',
]
INTERESTING_FEATURE_SET = set(INTERESTING_FEATURES)

RENAMED_FEATURES = {}

def get_renamed_feature(c_type, feature):
    return RENAMED_FEATURES.get((c_type.removeprefix('VkPhysicalDevice'), feature), feature)

# Python's XPath implementation doesn't seem to support the contains predicate,
# do the checks in Python.

def gather_things(xml, thing):
    things = set()

    for core in xml.iterfind(".feature"):
        if 'vulkan' not in core.attrib['api'].split(','):
            continue
        for t in core.iterfind(f'.require/{thing}'):
            things.add(t.attrib['name'])

    for extension in xml.iterfind(".extensions/extension"):
        if extension.attrib.get('provisional', False):
            continue
        if 'vulkan' not in extension.attrib['supported'].split(','):
            continue
        for t in extension.iterfind(f'.require/{thing}'):
            things.add(t.attrib['name'])

    return things

def struct_type_enum(_type):
    return _type.find(".member/name[.='sType']/..").attrib['values']

def struct_extends(_type):
    return _type.attrib.get('structextends', '').split(',')

class FeatureStruct(typing.NamedTuple):
    c_type: str
    s_type: str
    features: list[str]
    member_prefix: str = ''

def collect_interesting_features(_type):
    c_type = _type.attrib['name']
    feature_names = (feature.find('name').text for feature in _type.iterfind('member'))
    return [x for x in feature_names if get_renamed_feature(c_type, x) in INTERESTING_FEATURE_SET]

def main():
    parser = ArgumentParser()
    parser.add_argument('--out-h', required=True)
    parser.add_argument('--out-c', required=True)
    parser.add_argument('xml')
    args = parser.parse_args()

    xml = et.parse(args.xml)

    all_types = gather_things(xml, 'type')

    feature_structs = []

    if True:
        _type = xml.find(".types/type[@category='struct'][@name='VkPhysicalDeviceFeatures']")
        feature_structs.append(FeatureStruct(
            c_type='VkPhysicalDeviceFeatures2',
            s_type='VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2',
            features=collect_interesting_features(_type),
            member_prefix='features.',
        ))

    for _type in xml.iterfind(".types/type[@category='struct']"):
        if _type.attrib['name'] not in all_types:
            continue
        if not {'VkDeviceCreateInfo', 'VkPhysicalDeviceFeatures2'}.issubset(struct_extends(_type)):
            continue

        interesting_features = collect_interesting_features(_type)
        if len(interesting_features) == 0:
           continue

        feature_structs.append(FeatureStruct(
            c_type=_type.attrib['name'],
            s_type=struct_type_enum(_type),
            features=interesting_features,
        ))

    env = {
        'interesting_features': INTERESTING_FEATURES,
        'feature_structs': feature_structs,
        'get_renamed_feature': get_renamed_feature,
    }

    try:
        with open(args.out_h, 'wb') as f:
            f.write(device_features_h.render(**env))
        with open(args.out_c, 'wb') as f:
            f.write(device_features_c.render(**env))
    except Exception:
        print(mako.exceptions.text_error_template().render(), file=sys.stderr)
        sys.exit(1)

device_features_h = Template(r"""#pragma once

#include <stdbool.h>

typedef struct DeviceFeatures DeviceFeatures;
struct DeviceFeatures {
% for flag in interesting_features:
    bool ${flag};
% endfor
};

void cg2c_gatherDeviceFeaturesVk(const void *vkFeatures, DeviceFeatures *allFeatures);
""", output_encoding='utf-8')

device_features_c = Template(r"""#include <vulkan/vulkan_core.h>

#include "device_features.h"

void
cg2c_gatherDeviceFeaturesVk(const void *vkFeatures, DeviceFeatures *allFeatures)
{
    for (const VkBaseInStructure *pNext = vkFeatures; pNext != NULL; pNext = pNext->pNext) {
        switch (pNext->sType) {
% for f in feature_structs:
        case ${f.s_type}: {
            const ${f.c_type} *features = (const void*)pNext;
% for flag in f.features:
            if (features->${f.member_prefix}${flag})
                allFeatures->${get_renamed_feature(f.c_type, flag)} = true;
% endfor
            break;
        }

% endfor
        default:
			break;
        }
    }
}
""", output_encoding='utf-8')

if __name__ == '__main__':
    main()
