from argparse import ArgumentParser
import sys
import typing

import mako.template

import ops


def main():
    parser = ArgumentParser()
    parser.add_argument('--out-h', required=True)
    parser.add_argument('--out-c', required=True)
    args = parser.parse_args()

    with open(args.out_h, 'wb') as f:
        f.write(ops_h.render(ops=ops.ALL))

    with open(args.out_c, 'wb') as f:
        f.write(ops_c.render(ops=ops.ALL))


ops_h = mako.template.Template(r"""typedef enum Op {
	OpXxx,
% for op in ops.values():

% if op.description:
% for l in op.description.split('\n'):
	// ${l.strip()}
% endfor
% endif
	Op${op.name},
% endfor

	OpLast,
} Op;
""", output_encoding='utf-8')


ops_c = mako.template.Template(r"""#include "all.h"

const OpInfo cg2c_opInfo[OpLast] = {
% for op in ops.values():
	[Op${op.name}] = {
		.name = "${op.name}",
% if op.aux_type in ["int", "uint32_t", "int64_t", "uint64_t", "size_t"]:
		.smallaux = true,
% endif
% if op.commutative:
		.commutative = true,
% endif
	},
% endfor
};
""", output_encoding='utf-8')


if __name__ == "__main__":
    main()
