// SPDX-License-Identifier: MIT

#pragma once

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <cg2c/cg2c.h>

#ifndef __GNUC__
#define __attribute__(x)
#endif

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

#include "gc.h"

#include "string_.h"
#include "slice.h"
#include "map.h"

int64_t cg2c_gcd(int64_t a, int64_t b);

typedef struct Pos     Pos;
typedef struct PosBase PosBase;

struct PosBase {
	String filename;
};

struct Pos {
	PosBase *base;
	uint32_t line, col;
};

PosBase* cg2c_newPosBase(String filename, GC *gc);

Pos* cg2c_newPos(PosBase *base, uint32_t line, uint32_t col, GC *gc);

bool cg2c_posEqual(Pos a, Pos b);

typedef struct Builder  Builder;
typedef struct CSE      CSE;
typedef struct Compile  Compile;
typedef struct Region   Region;
typedef struct Rewriter Rewriter;
typedef struct Symbol   Symbol;
typedef struct Type     Type;
typedef struct Value    Value;

// TODO: move language-specific options into separate struct

struct Compile {
	bool unsignedChar;

	// longSize must be either 4 or 8.
	int64_t longSize;

	// ptrSize must be 8.
	int64_t ptrSize;

	int64_t funcSize;

	// bigEndian must be false.
	bool bigEndian;

	// Interned types. TODO: these should be weak maps once we get real GC
	// and be renamed to something clearer

	Map *funcs;  // Type*[2] → Type*
	Map *tuples; // Slice of Type* → Type*

	int64_t idctr;

	// TODO: rename to uidCtr?
	int64_t varId;

	bool errored;

	void *dump;

	void (*diag)(const char*, int, void*);
	void *diagUserData;

	GC *gc;
};

void cg2c_warnf(Compile*, const char*, ...) __attribute__((format(printf, 2, 3), no_split_stack));
void cg2c_warnfAt(Compile*, Pos*, const char*, ...) __attribute__((format(printf, 3, 4), no_split_stack));
void cg2c_errorfAt(Compile*, Pos*, const char*, ...) __attribute__((format(printf, 3, 4), no_split_stack));
void cg2c_errorf(Compile*, const char*, ...) __attribute__((format(printf, 2, 3), no_split_stack));
void cg2c_verrorf(Compile*, const char*, va_list);
// TODO: cg2c_fatalf(Compile*, const char*, ...);

typedef enum Kind {
	KindXxx,
	KindBool,
	KindInt8,
	KindInt16,
	KindInt32,
	KindInt64,
	KindPtr,
	KindFunc,
	KindMem,
	KindRMem,
	KindNoret,
	KindTuple,
	KindLast,
} Kind;

extern const char* cg2c_kindName[KindLast];

// Basic types
extern Type* cg2c_types[KindLast];

Type* cg2c_intptr(Compile *ctxt);

// Param and result must always be tuples.
Type* cg2c_func(Compile *ctxt, Type *param, Type *result);

Type* cg2c_tuple(Compile *ctxt, Slice elems);

Type* cg2c_tuple1(Compile *ctxt, Type *e);

Type* cg2c_tuple2(Compile *ctxt, Type *e0, Type *e1);

Type* cg2c_tuple3(Compile *ctxt, Type *e0, Type *e1, Type *e2);

Type* cg2c_tuple4(Compile *ctxt, Type *e0, Type *e1, Type *e2, Type *e3);

Type* cg2c_tupleN(Compile *ctxt, Type *elem, size_t n);

Kind cg2c_typeKind(Type *t);

// TODO: remove? we consistently mistype it as cg2c_typeKind(thing) == kind
bool cg2c_typeIs(Type *t, Kind kind);

Type* cg2c_funcParam(Type *t);

Type* cg2c_funcResult(Type *t);

size_t cg2c_tupleNElem(Type *t);

Type* cg2c_tupleElem(Type *t, size_t i);

int64_t cg2c_typeSize(Compile *ctxt, Type *t);

#include "ops.h"

typedef struct OpInfo OpInfo;
struct OpInfo {
	const char *name;
	bool smallaux; // deprecated; TODO: remove
	bool commutative;
};

extern const OpInfo cg2c_opInfo[OpLast];

// Subgroup reduction and scans need to be parametrized by a reduction op.
//
// Some subgroup voting needs to be parametrized by a comparison op.
//
// Most atomic accesses only need scope and order. In addition to these, compare
// exchange needs fail order and RMW needs op.
//
// Image ops need to specify image dimensionality, component type (float, int,
// uint, int64, uint64) and whether it's multisampled or not.
//
// Image atomics are same as plain atomics but also have all the image bits (see
// ImageTexelPointer in SPIR-V, which is used to implement image atomics.)
//
// Texture ops are hard, TODO

// TODO: split these in input and output builtins
//
// TODO: split input builtins into individual components
typedef enum Builtin {
	BuiltinXxx,

	BuiltinVertexID,

	BuiltinPosition, // int32 × 3

	BuiltinFragCoord,   // int32 × 3
	BuiltinFrontFacing, // bool

	BuiltinGlobalInvocationID,   // int32 × 3
	BuiltinLocalInvocationID,    // int32 × 3
	BuiltinSubgroupInvocationID, // int32

	BuiltinLast,
} Builtin;

// TODO: rename to ImageDim
typedef enum Dim {
	DimXxx,
	Dim1D,
	Dim2D,
	Dim3D,
	DimCube,
	DimLast,
} Dim;

typedef struct AuxImage AuxImage;
struct AuxImage {
	int  comp; // 1=float, 2=int32, 3=uint32, 4=int64, 5=int64
	Dim  dim;
	bool arrayed;
	bool ms;
};

// Value definition
struct Value {
	Op    op;
	Type  *type;
	void  *aux; // TODO: minimize use of aux
	Slice args; // of Value*

	Pos *pos;

	int64_t id;

	// TRACING ONLY: Value* or some other object this Value* is related to.
	// When tracing is enabled, all objects should be kept alive throughout
	// the entire compilation process to avoid
	//
	// TODO: maybe change this to int64_t
	// const void *trace;

	uint64_t auxstorage; // storage for small aux; do not access directly
};

struct Builder {
	Compile  *ctxt;
	Rewriter *rewriter;
	CSE      *cse;
	Pos      *pos; // source location that the built instructions will be assigned
	//const void *trace; // see Value.trace
};

Builder cg2c_builderWithPos(Builder b, Pos *pos);

Value* cg2c_buildValue(Builder b, Op op, Type *type, void *aux, Slice args);

Value* cg2c_buildArith2(Builder b, Op op, Value *x, Value *y);

Value* cg2c_buildShift(Builder b, Op op, Value *x, Value *off);

Value* cg2c_buildShiftConst(Builder b, Op op, Value *x, int kOff);

Value* cg2c_buildCmp(Builder b, Op op, Value *x, Value *y);

Value* cg2c_buildParam(Builder b, Region *r);

Value* cg2c_buildConst(Builder b, Type *type, uint64_t k);

// TODO: rename to just Func
Value* cg2c_buildRegionValue(Builder b, Region *r);

Value* cg2c_buildSymbol(Builder b, Symbol *s);

// TODO: generalized conversion builder

Value* cg2c_buildPtrToInt(Builder b, Value *v);

Value* cg2c_buildIntToPtr(Builder b, Value *v);

Value* cg2c_buildMakeTuple(Builder b, Slice elems);

Value* cg2c_buildMakeTuple1(Builder b, Value *e);

Value* cg2c_buildMakeTuple2(Builder b, Value *e0, Value *e1);

Value* cg2c_buildMakeTuple4(Builder b, Value *e0, Value *e1, Value *e2, Value *e3);

Value* cg2c_buildExtract(Builder b, Value *tuple, size_t i);

Value* cg2c_buildAddPtr(Builder b, Value *base, Value *off);

Value* cg2c_buildAddPtrConst(Builder b, Value *base, int64_t off);

Value* cg2c_buildVar(Builder b, int64_t size, int64_t align);

Value* cg2c_buildLoad(Builder b, Type *type, int64_t align, Value *mem, Value *ptr);

Value* cg2c_buildStore(Builder b, int64_t align, Value *mem, Value *ptr, Value *val);

Value* cg2c_buildIf(Builder b, Value *cond, Region *then, Region *else_, Value *arg);

Value* cg2c_buildLoop(Builder b, Value *arg, Region *body);

Value* cg2c_buildCall(Builder b, Value *target, Value *arg);

// or Const and ConstU

int64_t  cg2c_valueAuxInt(Value *v);
uint64_t cg2c_valueAuxUint(Value *v);

Region* cg2c_valueAuxRegion(Value *v);

Symbol* cg2c_valueAuxSymbol(Value *v);

size_t cg2c_valueNArg(Value *v);
Value* cg2c_valueArg(Value *v, size_t i);

// Same as cg2c_valueAuxRegion(cg2c_valueArg(v, i))
Region* cg2c_valueArgRegion(Value *v, size_t i);

// ...

typedef void (*VisitorFunc)(Compile *ctxt, Value *v, void *etc);

void cg2c_foreach(Compile *ctxt, VisitorFunc visitor, void *etc, Region *r);

// Rewriting

// u is the instruction being rewritten. Do not return u as-is or use u as
// argument to Value constructors, always construct new Value object instead.
typedef Value* (*RewriterFunc)(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc);

Region* cg2c_rewrite(Builder b, RewriterFunc rewriter, void *etc, Region *r);

Region* cg2c_rewritePlain(Builder b, Region *r);

typedef Value* (*RewriterFunc2)(Builder b, Map *re, Value *u, void *etc);

Region* cg2c_rewriteRegion(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Region *r);
Value*  cg2c_rewriteValue(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Value *v);
Value*  cg2c_rewriteValueDefault(Builder b, Map *re, RewriterFunc2 rewriter, void *etc, Value *v);

struct Region {
	Type  *type;
	Value *terminator; // or rename to something else

	// Scheduled instructions
	Slice values;

	int64_t id;
};

static Region*
cg2c_newRegion(Compile *ctxt, Type *type)
{
	assert(cg2c_typeIs(type, KindFunc));
	Region *r = cg2c_malloc(sizeof *r, ctxt->gc);
	r->type = type;
	r->id = ++ctxt->idctr;
	return r;
}

// TODO: move implementations out of the header

static Value*
cg2c_regionTerminator(Region *r)
{
	assert(r->terminator != NULL);
	return r->terminator;
}

static void
cg2c_regionTerminate(Region *r, Value *t)
{
	assert(cg2c_funcResult(r->type) == t->type);
	assert(r->terminator == NULL);
	r->terminator = t;
}

// Rewrite rules

typedef struct Pattern Pattern;
struct Pattern {
	int     op;
	Pattern **args;
	size_t  narg;
	bool    ddd;
};

typedef struct RewriteRule RewriteRule;
struct RewriteRule {
	Pattern *pattern;
	Value   *(*replace)(Builder, Value*);
};

extern Slice cg2c_commonRules;
extern Slice cg2c_genericAddressingToTags;
extern Slice cg2c_idioms;
extern Slice cg2c_int64ToInt32;
// extern Slice cg2c_int128ToInt64;

Rewriter* cg2c_newRewriter(Slice rules, GC *gc);

CSE* cg2c_newCSE(GC *gc);

// Common passes

// TODO: reassociation (rules, pass or implicit as part of buildValue)

// TODO: actually identify live stores and not just vars
typedef struct LiveStores LiveStores;
struct LiveStores {
	Map *vars;
	Map *live;
};

LiveStores cg2c_liveStores(Compile *ctxt, Region *r);

// cg2c_loadsToSSA tries to replace loads with uses of previously stored values
// and eliminates dead stores.
//
// TODO: if necessary, introduce a variant of loadsToSSANoDSE
bool cg2c_loadsToSSA(Builder b, Region **r);

Region* cg2c_elimDeadStores(Builder b, Region *r, LiveStores liveIn, LiveStores *liveOut, bool *changed);

// TODO: array copy elimination pass

Value* cg2c_inline(Builder b, Region *r, Value *a);

Region* cg2c_inlineCalls(Builder b, Region *r, bool *changed);

// cg2c_simplifyControlFlow simplifies control flow in a way that's not possible
// to do with rewrite rules. Notably, it takes care of ifs and loops with
// constant conditions.
Region* cg2c_simplifyControlFlow(Builder b, Region *r, bool *changed);

// Scheduling

void cg2c_schedule(Compile *ctxt, Region *r);

typedef enum SymbolBinding {
	SymbolBindingUndef,
	SymbolBindingLocal,
	SymbolBindingGlobal,
	SymbolBindingWeak,
} SymbolBinding;

struct Symbol {
	SymbolBinding binding;
	String        name;
	Type          *type;
	Region        *def; // (binding != SymbolBindingUndef) == (def != NULL); TODO: rename to def
	CSE           *cse;

	Pos *pos;
};

Symbol* cg2c_findSymbol(Slice symbols, String name);

// cg2c_symbolMap builds a symbol map out of a list of symbols.
Map* cg2c_symbolMap(Compile *ctxt, Slice symbols);

typedef struct LinkOptions LinkOptions;
struct LinkOptions {
	int dummy;
};

// cg2c_link rewrites references to undefined and weak symbols with references
// to symbols with the same name returned by lookup.
//
// cg2c_link only rewrites symbols that are reachable and returns the list of
// all visited symbols. This list is always a subset of the set of symbols
// returned by lookup.
//
// TODO: root should be a slice
Slice cg2c_link(Compile *ctxt, const LinkOptions *opts, Symbol* (*lookup)(Compile *ctxt, String name, void *etc), void *etc, Symbol *root);

// cg2c_liveSymbols returns the list of reachable symbols.
//
// TODO: root should be a slice
Slice cg2c_liveSymbols(Compile *ctxt, Symbol *root);

Slice /* of Symbol* */ cg2c_parseC(Compile *ctxt, String filename, String src);

// Export

Cg2cBlob* cg2c_export(void *data, size_t len, const Cg2cAllocFuncs *_unused);

// Compiler debugging
void cg2c_validateSchedule(Region *r, GC *gc);

void cg2c_dumptype(Type *type);

// TODO: ast dump

// TODO: specify how things are supposed to be dumped, i.e. whether things are
// scheduled, etc

void cg2c_dump(Compile *ctxt, const char *pass, Symbol *s);
