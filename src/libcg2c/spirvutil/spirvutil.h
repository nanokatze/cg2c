#pragma once

#include <stdint.h>

#include <spirv/unified1/spirv.h>
#include <spirv/unified1/GLSL.std.450.h>

#include "string_.h"
#include "map.h"
#include "slice.h"

typedef struct GC GC;

typedef struct SpvDef     SpvDef;
typedef struct SpvOperand SpvOperand;

struct SpvOperand {
	SpvDef *id;
	uint32_t lit;
};

struct SpvDef {
	SpvOp op;
	SpvDef *type;
	Slice operands; // of SpvOperand
	// TODO: have some storage so that most instructions keep their operands in-line

	// TODO: plop a slice of annotation instructions here?
};

// A sentinel type for instructions that define an id but do not have result
// type.
extern SpvDef *cg2c_spvNoTypeId;

SpvDef* cg2c_newSpvDef(SpvOp op, SpvDef *type, Slice operands, GC *gc);

SpvDef* cg2c_spvDefsAppend(Slice *instrs, SpvOp op, SpvDef *type, Slice operands, GC *gc);

// TODO: add a Slice decorations parameter
SpvDef* cg2c_spvDefsAppendHashed(Slice *instrs, Map *m, SpvOp op, SpvDef *type, Slice operands, GC *gc);

// TODO: better name
Map *cg2c_newHashedSpvInstrs(GC *gc);

Slice cg2c_spvAssemble(Slice buf, Map *idMap, Slice instrs, GC *gc);
