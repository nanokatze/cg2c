#include "spirvutil.h"

#include <assert.h>

#include "map_type.h"
#include "gc.h"

static void validateOperand(SpvOperand operand) { assert(operand.id == NULL || operand.lit == 0); }

// TODO: rename
typedef struct CSEKey CSEKey;
struct CSEKey {
	SpvOp op;
	SpvDef *type;
	Slice operands; // of SpvOperand
};

static const MapType* cseKey_ptr_map;

SpvDef *cg2c_spvNoTypeId = &(SpvDef) {0};

SpvDef*
cg2c_newSpvDef(SpvOp op, SpvDef *type, Slice operands, GC *gc)
{
	for (size_t i = 0; i < cg2c_sllen(operands); i++)
		validateOperand(cg2c_slindex(SpvOperand, operands, i));

	SpvDef *instr = cg2c_malloc(sizeof *instr, gc);
	instr->op = op;
	instr->type = type;
	instr->operands = cg2c_slclone(sizeof(SpvOperand), operands, gc);
	return instr;
}

SpvDef*
cg2c_spvDefsAppend(Slice *instrs, SpvOp op, SpvDef *type, Slice operands, GC *gc)
{
	SpvDef *instr = cg2c_newSpvDef(op, type, operands, gc);
	*instrs = cg2c_slappend(sizeof(SpvDef*), *instrs, &instr, gc);
	return instr;
}

SpvDef*
cg2c_spvDefsAppendHashed(Slice *instrs, Map *m, SpvOp op, SpvDef *type, Slice operands, GC *gc)
{
	for (size_t i = 0; i < cg2c_sllen(operands); i++)
		validateOperand(cg2c_slindex(SpvOperand, operands, i));

	SpvDef *instr = *(SpvDef* const*)cg2c_mapaccess1(cseKey_ptr_map, m, &(CSEKey) {op, type, operands});
	if (instr == NULL) {
		instr = cg2c_spvDefsAppend(instrs, op, type, operands, gc);
		*(SpvDef**)cg2c_mapassign(cseKey_ptr_map, m, &(CSEKey) {op, type, instr->operands}, gc) = instr;
	}
	return instr;
}

// TODO: for extra debuggability, only allow forward declaration with certain
// ops

static uint32_t
getId(Map *idMap, SpvDef *instr, GC *gc)
{
	uint32_t id = *(const uint32_t*)cg2c_mapaccess1(cg2c_ptr_32_map, idMap, &instr);
	if (id == 0) {
		id = (uint32_t)cg2c_maplen(idMap) + 1;
		*(uint32_t*)cg2c_mapassign(cg2c_ptr_32_map, idMap, &instr, gc) = id;
	}
	return id;
}

static uint32_t
operandWord(Map *idMap, SpvOperand operand, GC *gc)
{
	return operand.id != NULL ? getId(idMap, operand.id, gc) : operand.lit;
}

static Slice
encode(Slice buf, Map *idMap, SpvDef *instr, GC *gc)
{
	assert(instr->op <= UINT16_MAX);

	size_t off0 = cg2c_sllen(buf);

	buf = cg2c_slappend(sizeof(uint32_t), buf, &(uint32_t) {instr->op}, gc);

	if (instr->type != NULL) {
		if (instr->type != cg2c_spvNoTypeId)
			buf = cg2c_slappend(sizeof(uint32_t), buf, &(uint32_t) {getId(idMap, instr->type, gc)}, gc);
		buf = cg2c_slappend(sizeof(uint32_t), buf, &(uint32_t) {getId(idMap, instr, gc)}, gc);
	}

	for (size_t i = 0; i < cg2c_sllen(instr->operands); i++) {
		uint32_t word = operandWord(idMap, cg2c_slindex(SpvOperand, instr->operands, i), gc);
		buf = cg2c_slappend(sizeof(uint32_t), buf, &word, gc);
	}

	cg2c_slindex(uint32_t, buf, off0) |= (uint32_t)(cg2c_sllen(buf) - off0) << 16;

	return buf;
}

Slice
cg2c_spvAssemble(Slice buf, Map *idMap, Slice instrs, GC *gc)
{
	for (size_t i = 0; i < cg2c_sllen(instrs); i++)
		buf = encode(buf, idMap, cg2c_slindex(SpvDef*, instrs, i), gc);
	return buf;
}

Map* cg2c_newHashedSpvInstrs(GC *gc) { return cg2c_mkmap(cseKey_ptr_map, gc); }

static bool operandsEqual(SpvOperand o1, SpvOperand o2) { return o1.id == o2.id && o1.lit == o2.lit; }

static bool
cseKeyEqual(const void *p, const void *q)
{
	const CSEKey *v = p;
	const CSEKey *u = q;

	if (v->op != u->op)
		return false;

	if (v->type != u->type)
		return false;

	if (cg2c_sllen(v->operands) != cg2c_sllen(u->operands))
		return false;
	for (size_t i = 0; i < cg2c_sllen(v->operands); i++) {
		if (!operandsEqual(
			cg2c_slindex(SpvOperand, v->operands, i),
			cg2c_slindex(SpvOperand, u->operands, i)))
			return false;
	}

	return true;
}

static size_t
cseKeyHash(const void *p, size_t h)
{
	const CSEKey *v = p;

	h = cg2c_memhash(&v->op, sizeof v->op, h);

	h = cg2c_memhash(&v->type, sizeof v->type, h);

	for (size_t i = 0; i < cg2c_sllen(v->operands); i++) {
		SpvOperand operand = cg2c_slindex(SpvOperand, v->operands, i);
		h = cg2c_memhash(&operand.id, sizeof operand.id, h);
		h = cg2c_memhash(&operand.lit, sizeof operand.lit, h);
	}

	return h;
}

static const MapType* cseKey_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = cseKeyEqual,
	.hasher   = cseKeyHash,
	.keysize  = sizeof(CSEKey),
	.elemsize = sizeof(void*),
};
