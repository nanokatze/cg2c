import typing


# Consult Adding new ops section in README.md, adjacent to this file, before
# proceeding


class Op(typing.NamedTuple):
    name: str
    aux_type: str = None
    commutative: bool = False  # commutative in the first two arguments
    description: str = None


_OPS = [
    Op(name="Add", commutative=True),
    Op(name="Addc", commutative=True),
    Op(name="Sub"),
    Op(name="Subb"),
    Op(name="Mul", commutative=True),
    Op(name="MulHi", commutative=True),
    Op(name="UMulHi", commutative=True, description="UMulHi returns the high bits of the product of its arguments"),

    # BUG: for division and remainder ops we need to introduce scheduling
    # barriers so that they don't get scheduled too early.

    Op(name="Div"),
    Op(name="UDiv"),
    Op(name="Rem"),
    Op(name="URem"),

    Op(name="FAdd", commutative=True),
    Op(name="FSub"),
    Op(name="FMul", commutative=True),
    Op(name="FMA", commutative=True),
    Op(name="FDiv"),

    Op(name="FSqrt"),

    Op(name="And", commutative=True),
    Op(name="Or", commutative=True),
    Op(name="Xor", commutative=True, description="Note: Xor is not defined for boolean operands. NotEqual should be used instead."),

    Op(name="Lsh"),
    Op(name="Rsh"),
    Op(name="URsh"),

    Op(name="Not"),

    # TODO: integer dot product ops

    Op(name="Equal", commutative=True),
    Op(name="FEqual", commutative=True),
    Op(name="NotEqual", commutative=True),
    Op(name="FNotEqual", commutative=True),
    Op(name="Less"),
    Op(name="ULess"),
    Op(name="FLess"),
    Op(name="GreaterEqual"),
    Op(name="UGreaterEqual"),
    Op(name="FGreaterEqual"),

    Op(name="CondSelect", description="Deprecated in favor of region If"),

    Op(name="FMin"),
    Op(name="FMax"),

    Op(name="FAbs"),
    Op(name="FNeg"),
    Op(name="FFloor"),
    Op(name="FCeil"),
    Op(name="FTrunc"),
    Op(name="FRoundEven"),

    # Ldexp, FrexpExponent and FrexpSignificand

    Op(name="Param"),

    Op(name="Phi", description="Phi is a helper op used by loads to SSA"),

    Op(
        description="""
            Const values should not carry source location info

            TODO: store constants sign-extended?
        """,
        name="Const",
        aux_type="uint64_t",
    ),

    Op(name="Region", aux_type="Region*", description="Note: Region values should not carry source location info"),

    Op(name="Symbol", aux_type="Symbol*"),

    Op(name="Trunc"),
    Op(name="SignExt"),
    Op(name="ZeroExt"),

    Op(name="IToF"),
    Op(name="UToF"),
    Op(name="FToI"),
    Op(name="FToU"),
    Op(name="FToF"),

    Op(name="RoundF16"),
    Op(name="RoundF32"),

    Op(name="PtrToInt"),
    Op(name="IntToPtr"),

    Op(name="MakeTuple"),
    Op(name="Extract", aux_type="size_t"),

    # TODO: maybe encode alignment, constraints (alignment offset and
    # multiplier) of the resulting, pointer. On the other hand, attaching
    # alignment constraints to AddPtr itself will probably impede CSE, unless we
    # add special cases to CSE. On the other hand, perhaps CSE of AddPtrs is not
    # that important
    Op(name="AddPtr", description="AddPtr offsets a pointer by a signed offset"),

    # TODO: maybe add AddPtrU

    Op(name="MakeInt64", description="MakeInt64 helps lowering int64 arithmetic."),

    # TODO: subgroup voting, quad ops
    Op(name="SubgroupShuffle"),
    Op(name="SubgroupShuffleXor"),
    Op(name="SubgroupReduce"),
    Op(name="SubgroupInclusiveScan"),
    Op(name="SubgroupExclusiveScan"),

    # TODO: ops to merge multiple mems into one and produce rmem out of mem

    Op(name="Var", aux_type="int64_t", description="Var returns a pointer to some location, uniquely corresponding to\nan instance of this instruction within a dynamic instance of the\nfunction this instruction belongs in called by Call."),

    Op(name="Load", aux_type="int64_t", description="Load loads a value"),
    Op(name="Store", aux_type="int64_t", description="Store stores a value"),

    Op(name="AtomicLoad"),
    Op(name="AtomicStore"),
    Op(name="AtomicExchange"),
    Op(name="AtomicCompareAndSwap"),
    Op(name="AtomicRMW"),

    Op(
        description="""
            Implicit LOD sample
        """,
        name="Tex",
    ),

    Op(name="ImageLoad"),
    Op(name="ImageStore"),

    Op(name="RayQueryInit"),

    # TODO: more texture, storage image and ray query ops ...

    # TODO: upgrade this to a switch
    #
    # A switch would look like: If cond arg default_region (value, region)..
    Op(name="If"),
    Op(name="Loop"),

    Op(name="Call"),

    # Underspecified ops intended to closely represent what the hardware does

    Op(name="Sin"),
    Op(name="Cos"),

    Op(
        description="""
            Found in Turing+ Nvidia hardware:
            https://gitlab.freedesktop.org/karolherbst/mesa/-/blob/68e18cb8471014997ec71afd9cc744162acc2c69/src/nouveau/compiler/nvir.xml#L602
        """,
        name="Tanh",
    ),

    Op(name="Exp2"),
    Op(name="Log2"),

    Op(
        description="""
            Found in Intel hardware:
            https://gitlab.freedesktop.org/mesa/mesa/-/blob/e68adf19bc3b55a76a12a58a5b32d2a341fd78dd/src/intel/compiler/brw_eu_defines.h#L1604

            Results might differ from exp2(log2(a) * b). NIR rewrite
            rules apply only if exp2 is not marked exact:
            https://gitlab.freedesktop.org/mesa/mesa/-/blob/e68adf19bc3b55a76a12a58a5b32d2a341fd78dd/src/compiler/nir/nir_opt_algebraic.py#L1276
        """,
        name="Powr",
    ),

    # Shader IO

    Op(name="GetIndirectConstant"),
    Op(name="GetPushConstant"),

    Op(name="GetInput", description="Get data from a user-defined shader input"),  # TODO: specify whether we're storing int/uint/float

    Op(name="GetBuiltin"),

    Op(name="StoreOutput"),  # TODO: specify whether we're storing int/uint/float
    Op(name="StoreBuiltin"),

    # TODO: replace this family of ops with one op returning the address
    # space of a pointer
    Op(
        description="""
            It's up to backends to decide which address space a
            pointer defined by certain operations (such as Var)
            points to, and optimize appropriately.
        """,
        name="PtrIsGlobal",
    ),
    Op(
        name="PtrIsShared",
    ),
    Op(
        name="PtrIsScratch",
    ),

    Op(name="LoadGlobal", aux_type="int64_t"),
    Op(name="StoreGlobal", aux_type="int64_t"),

    Op(name="AtomicLoadGlobal"),
    Op(name="AtomicStoreGlobal"),
    Op(name="AtomicExchangeGlobal"),
    Op(name="AtomicCompareAndSwapGlobal"),
    Op(name="AtomicRMWGlobal"),

    Op(name="LoadShared", aux_type="int64_t"),
    Op(name="StoreShared", aux_type="int64_t"),

    Op(name="AtomicLoadShared"),
    Op(name="AtomicStoreShared"),
    Op(name="AtomicExchangeShared"),
    Op(name="AtomicCompareAndSwapShared"),
    Op(name="AtomicRMWShared"),

    Op(name="LoadScratch", aux_type="int64_t"),
    Op(name="StoreScratch", aux_type="int64_t"),
]


ALL = {}
for op in _OPS:
    ALL[op.name] = op
