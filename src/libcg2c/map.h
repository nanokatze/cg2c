#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct GC GC;

size_t cg2c_memhash(const void *p, size_t n, size_t seed);

typedef struct Map     Map;
typedef struct MapType MapType;

Map*        cg2c_mkmap(const MapType *t, GC *gc);
size_t      cg2c_maplen(const Map *h);
const void* cg2c_mapaccess1(const MapType *t, const Map *h, const void *key);
const void* cg2c_mapaccess2(const MapType *t, const Map *h, const void *key, bool *pres);
bool        cg2c_mapcontains(const MapType *t, const Map *h, const void *key);
void*       cg2c_mapassign(const MapType *t, Map *h, const void *key, GC *gc);
void        cg2c_mapdelete(const MapType *t, Map *h, const void *key, GC *gc);

// Common map types

extern const MapType* cg2c_32_32_map;
extern const MapType* cg2c_64_32_map;
extern const MapType* cg2c_str_32_map;
extern const MapType* cg2c_str_ptr_map;
extern const MapType* cg2c_str_set;
extern const MapType* cg2c_ptr_32_map;
extern const MapType* cg2c_ptr_64_map;
extern const MapType* cg2c_ptr_ptr_map;
extern const MapType* cg2c_ptr_slice_map;
extern const MapType* cg2c_ptr_set;
extern const MapType* cg2c_ptrSlice_ptr_map; // careful: slices used as keys must not be modified
