#include "all.h"

static Value*
ptrchase(Value *v)
{
	while (v->op == OpAddPtr)
		v = cg2c_valueArg(v, 0);
	assert(cg2c_typeIs(v->type, KindPtr));
	return v;
}

// TODO: export this
static void
liveStores(Compile *ctxt, Value *v, void *etc)
{
	LiveStores *live = etc;

	switch (v->op) {
	case OpAddPtr:
		break;

	case OpVar:
		cg2c_mapassign(cg2c_ptr_set, live->vars, &v, ctxt->gc);
		break;

	default: {
		for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
			if (v->op == OpStore && i == 1)
				continue;

			Value *a = cg2c_valueArg(v, i);
			if (cg2c_typeIs(a->type, KindPtr)) {
				Value *base = ptrchase(a);
				if (base->op == OpVar) {
					cg2c_mapassign(cg2c_ptr_set, live->live, &base, ctxt->gc);
				}
			}
		}
		break;
	}
	}
}

LiveStores
cg2c_liveStores(Compile *ctxt, Region *r)
{
	LiveStores *live = &(LiveStores) {
		.vars = cg2c_mkmap(cg2c_ptr_set, ctxt->gc),
		.live = cg2c_mkmap(cg2c_ptr_set, ctxt->gc),
	};
	cg2c_foreach(ctxt, liveStores, live, r);

	return *live;
}

typedef struct DeadStoreElim DeadStoreElim;
struct DeadStoreElim {
	LiveStores in;
	LiveStores out;
	bool changed;
};

static Value*
elimDeadStores(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	DeadStoreElim *dse = etc;

	switch (op) {
	case OpStore: {
		Value *mem = cg2c_slindex(Value*, args, 0);
		Value *p = cg2c_slindex(Value*, args, 1);
		Value *base = ptrchase(p);

		if (base->op == OpVar && !cg2c_mapcontains(cg2c_ptr_set, dse->in.live, &base)) {
			assert(cg2c_mapcontains(cg2c_ptr_set, dse->in.vars, &base));
			dse->changed = true;
			return mem;
		}

		break;
	}

	default:
		break;
	}

	Value *v = cg2c_buildValue(b, op, type, aux, args);

	// cg2c_elimDeadStores is usually run several times in a row. Avoid
	// redundant cg2c_findLiveStores runs by looking for remaining live
	// stores during dead store elimination.
	liveStores(b.ctxt, v, &dse->out);

	return v;
}

Region*
cg2c_elimDeadStores(Builder b, Region *r, LiveStores liveIn, LiveStores *liveOut, bool *changed)
{
	DeadStoreElim *dse = &(DeadStoreElim) {
		.in = liveIn,
		.out = {
			.vars = cg2c_mkmap(cg2c_ptr_set, b.ctxt->gc),
			.live = cg2c_mkmap(cg2c_ptr_set, b.ctxt->gc),
		},
	};
	r = cg2c_rewrite(b, elimDeadStores, dse, r);
	if (dse->changed) {
		*liveOut = dse->out;
		*changed = true;
	}
	return r;
}
