// SPDX-License-Identifier: MIT

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __GNUC__
#define CG2C_PUBLIC __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Cg2cBlob Cg2cBlob;

typedef enum Cg2cOption {
	CG2C_OPTION_EARLY_FRAGMENT_TESTS = 1,
	CG2C_OPTION_WORKGROUP_SIZE,
} Cg2cOption;

typedef struct Cg2cOptionInfo Cg2cOptionInfo;
struct Cg2cOptionInfo {
	Cg2cOption option;
	union {
		uint32_t workgroupSize[3];
	};
};

typedef enum Cg2cInputType {
	CG2C_IN_CONSTANT = 1,
	CG2C_IN_INDIRECT_CONSTANT,
	CG2C_IN_PUSH_CONSTANT,

	// CG2C_IN_SHARED_PTR,

	CG2C_IN_DRAW_ID,

	// CG2C_IN_PRIMITIVE_INDICES_PTR,
	// CG2C_IN_MESH_VERTICES_PTR,
	// CG2C_IN_MESH_PRIMITIVES_PTR,
	// CG2C_IN_VERTEX_ATTRIBUTES_PTR, // can occur multiple times, uses location

	CG2C_IN_BASE_INSTANCE,
	CG2C_IN_BASE_VERTEX,
	CG2C_IN_INSTANCE_ID,
	CG2C_IN_VERTEX_ID,

	// CG2C_IN_PER_VERTEX,
	CG2C_IN_INTERPOLATED,
	// CG2C_IN_BARY_COORD,
	CG2C_IN_FRAG_COORD,
	CG2C_IN_FRONT_FACING,
	CG2C_IN_PRIMITIVE_ID,

	CG2C_IN_GLOBAL_INVOCATION_ID,
	CG2C_IN_LOCAL_INVOCATION_ID,
} Cg2cInputType;

typedef struct Cg2cInput Cg2cInput;
struct Cg2cInput {
	Cg2cInputType type;
	uint32_t offset;
	uint32_t size;
	union {
		const void *constantData;
		uint32_t pushConstantSrcOffset;
		struct {
			uint32_t location;
		} interpolated;
	};
};

typedef enum Cg2cOutputType {
	CG2C_OUT_VERTEX_POSITION = 1,
	CG2C_OUT_VERTEX_ATTRIBUTE,

	CG2C_OUT_FRAG_COLOR,
	CG2C_OUT_FRAG_DEPTH,
	CG2C_OUT_FRAG_STENCIL,
	CG2C_OUT_DISCARD, // size can be 1 (C _Bool) or 4 (legacy GLSL/HLSL bool)
} Cg2cOutputType;

typedef struct Cg2cOutput Cg2cOutput;
struct Cg2cOutput {
	Cg2cOutputType type;
	uint32_t offset;
	uint32_t size;
	union {
		struct {
			uint32_t location;
		} vertexAttribute;
		struct {
			uint32_t location;
		} fragColor;
	};
};

typedef struct Cg2cCompileInfo Cg2cCompileInfo;
struct Cg2cCompileInfo {
	const char* const* files;
	size_t             fileCount;

	// The pointer returned by readFile must remain valid until
	// cg2cCompile returns.
	const char* (*readFile)(const char *filename, void *userData);
	void *readFileUserData;

	void (*diag)(const char *message, int verbosity, void *userData);
	void *diagUserData;
};

typedef struct Cg2cAllocFuncs Cg2cAllocFuncs;
struct Cg2cAllocFuncs {
	void* (*callocFunc)(size_t, size_t, void*);
	void  (*freeFunc)(void*, void*);
	void* userData;
};

CG2C_PUBLIC const void* cg2cBlobData(const Cg2cBlob*);
CG2C_PUBLIC size_t      cg2cBlobLen(const Cg2cBlob*);
CG2C_PUBLIC void        cg2cFreeBlob(Cg2cBlob*, const Cg2cAllocFuncs*);

CG2C_PUBLIC int cg2cCompile(const Cg2cCompileInfo *compileInfo, const Cg2cAllocFuncs *allocFuncs, Cg2cBlob **aOut);

#ifdef __cplusplus
}
#endif
