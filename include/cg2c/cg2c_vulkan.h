// SPDX-License-Identifier: MIT

#pragma once

#include "cg2c.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Cg2cBindlessVk Cg2cBindlessVk;
struct Cg2cBindlessVk {
	uint32_t set;
	uint32_t binding;
};

typedef struct Cg2cCompileInfoVk Cg2cCompileInfoVk;
struct Cg2cCompileInfoVk {
	Cg2cCompileInfo base;

	// deviceProperties specifies Vulkan device properties the compiler can
	// use.
	const void* deviceProperties;

	uint32_t spirvVersion;

	// enabledDeviceFeatures specifies Vulkan features the compiler can use.
	//
	// Programs that use Cg2c at runtime should reuse the pNext chain that
	// was used to create Vulkan device.
	const void* enabledDeviceFeatures;

	VkShaderStageFlagBits stage;
	const char*           entryPoint;
	const Cg2cInput*      inputs;
	size_t                inputCount;
	const Cg2cOutput*     outputs;
	size_t                outputCount;
	const Cg2cOptionInfo* options;
	size_t                optionCount;

	// TODO: outline the following into their own structure

	const Cg2cBindlessVk* samplers;
	const Cg2cBindlessVk* sampledImages;
	const Cg2cBindlessVk* storageImages;
};

CG2C_PUBLIC int cg2cCompileVk(const Cg2cCompileInfoVk *compileInfo, const Cg2cAllocFuncs *allocFuncs, Cg2cBlob **aOut);

#ifdef __cplusplus
}
#endif
